import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("bookmark");
  if (!hasTable) {
    await knex.schema.createTable("bookmark", (table) => {
      table.increments();
      table.integer("user_id");
      table.integer("attractions_id");
      table.string("attractions_name");
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("bookmark");
}
