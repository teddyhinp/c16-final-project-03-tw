import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable("plans", (t) => {
    t.dropColumn("name");
    t.string("plan_name");
    t.string("selected_date");
    t.string("json_plan", 4000);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable("plans", (t) => {
    t.string("name").notNullable();
    t.dropColumn("plan_name");
    t.dropColumn("selected_date");
    t.dropColumn("json_plan");
  });
}
