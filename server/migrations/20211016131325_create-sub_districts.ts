import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('sub_districts');
    if(!hasTable){
        await knex.schema.createTable('sub_districts',(table)=>{
            table.increments();
            table.string("name").notNullable().unique();
            table.float("latitude").notNullable();
            table.float("longitude").notNullable();
            table.integer("district_id").notNullable().unsigned();
            table.foreign("district_id").references("districts.id");
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('sub_districts');
}

