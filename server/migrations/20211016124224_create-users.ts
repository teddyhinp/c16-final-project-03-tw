import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("users");
  if (!hasTable) {
    await knex.schema.createTable("users", (table) => {
      table.increments();
      table.string("username").notNullable().unique();
      table.string("password").notNullable();
      table.string("ageGroup").notNullable();
      table.string("gender").notNullable();
      table.string("preference1").notNullable();
      table.string("preference2").notNullable();
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("users");
}
