import { Knex } from "knex";
import {
    HK,
    HK_港島,
    HK_九龍,
    HK_新界,
    HK_港島_中西區,
    HK_港島_灣仔,
    HK_港島_東區,
    HK_港島_南區,
    HK_九龍_油尖旺,
    HK_九龍_深水埗,
    HK_九龍_九龍城,
    HK_九龍_黃大仙,
    HK_九龍_觀塘,
    HK_新界_葵青,
    HK_新界_荃灣,
    HK_新界_屯門,
    HK_新界_元朗,
    HK_新界_北區,
    HK_新界_大埔,
    HK_新界_沙田,
    HK_新界_西貢,
    HK_新界_離島,
} from "../resources/areas_and_districts_model";
import { sub_district_coordinates } from "../resources/sub_district_model";


export async function up(knex: Knex): Promise<void> {
    await knex("routes").del();
    await knex("malls").del();
    await knex("restaurants").del();
    await knex("attractions").del();
    await knex("facilities").del();
    await knex("hotels").del();
    await knex("sub_districts").del();
    await knex("districts").del();
    await knex("areas").del();
    
    const districts = [HK_港島, HK_九龍, HK_新界];
    const sub_districts = [
        HK_港島_中西區,
        HK_港島_灣仔,
        HK_港島_東區,
        HK_港島_南區,
        HK_九龍_油尖旺,
        HK_九龍_深水埗,
        HK_九龍_九龍城,
        HK_九龍_黃大仙,
        HK_九龍_觀塘,
        HK_新界_葵青,
        HK_新界_荃灣,
        HK_新界_屯門,
        HK_新界_元朗,
        HK_新界_北區,
        HK_新界_大埔,
        HK_新界_沙田,
        HK_新界_西貢,
        HK_新界_離島,
    ];
    const areas_id = [];
    const districts_id = [];

    // Inserts areas seed entries
    for (const area of HK) {
        const area_id = await knex("areas")
            .insert({
                name: area,
            })
            .into("areas")
            .returning("id");
        areas_id.push(area_id);
    }

    // Inserts districts seed entries
    for (const index in districts) {
        for (const district of districts[index]) {
            const district_id = await knex("districts")
                .insert({
                    name: district,
                    area_id: Number(areas_id[index]),
                })
                .into("districts")
                .returning("id");
            districts_id.push(district_id);
        }
    }

    // Inserts sub_districts seed entries
    for (const index in sub_districts) {
        for (const sub_district of sub_districts[index]) {
            await knex("sub_districts")
                .insert({
                    name: sub_district,
                    latitude: sub_district_coordinates[sub_district][0],
                    longitude: sub_district_coordinates[sub_district][1],
                    district_id: Number(districts_id[index]),
                })
                .into("sub_districts");
        }
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex("sub_districts").del();
    await knex("districts").del();
    await knex("areas").del();
}

