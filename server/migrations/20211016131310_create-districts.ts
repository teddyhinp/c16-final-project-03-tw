import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('districts');
    if(!hasTable){
        await knex.schema.createTable('districts',(table)=>{
            table.increments();
            table.string("name").notNullable().unique();
            table.integer("area_id").notNullable().unsigned();
            table.foreign("area_id").references('areas.id');
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('districts');
}

