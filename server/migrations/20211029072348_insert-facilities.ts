import { Knex } from "knex";
import { Client } from "pg";
import dotenv from "dotenv";

dotenv.config();

export async function up(knex: Knex): Promise<void> {
  // if(process.env.NODE_ENV === 'production') {return}
  const client = new Client({
    host: process.env.RDS_HOST,
    port: 5432,
    database: process.env.RDS_NAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
  });

  await client.connect();

  // Inserts seed entries
  const result = await client.query(
    "SELECT * FROM place WHERE categories1 = $1",
    ["facilities"]
  );
  const facilities = result.rows;
  for (const facility of facilities) {
    const sub_district_id = await knex
      .select("id")
      .from("sub_districts")
      .where("name", `${facility.sub_district}`)
      .first();
    await knex("facilities")
      .insert({
        name: facility.name,
        categories: facility.categories2,
        address: facility.address,
        phone_number: facility.phone_number,
        website: facility.website,
        latitude: facility.latitude,
        longitude: facility.longitude,
        sub_district_id: sub_district_id.id,
      })
      .into("facilities");
    //   .returning("id");
  }

  await client.end();
}

export async function down(knex: Knex): Promise<void> {
  await knex("facilities").del();
}
