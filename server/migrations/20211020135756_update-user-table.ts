import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable("users", (t) => {
    t.string("username").nullable().alter();
    t.string("email").nullable();
    t.string("picture").nullable();
    t.string("name").nullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable("users", (t) => {
    t.string("username").notNullable().alter();
    t.dropColumn("email");
    t.dropColumn("picture");
    t.dropColumn("name");
  });
}
