import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable("planItems", (t) => {
    t.dropColumn("plan_id");
    t.dropColumn("stay_time");
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable("planItems", (t) => {
    t.integer("stay_time").unsigned;
    t.integer("plan_id").notNullable().unsigned;
    t.foreign("plan_id").references("plans.id");
  });
}
