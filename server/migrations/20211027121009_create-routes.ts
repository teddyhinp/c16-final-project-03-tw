import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable("routes");
    if (!hasTable) {
        await knex.schema.createTable("routes", (table) => {
            table.increments();
            table.integer("distance").notNullable();
            table.integer("duration").notNullable();
            table.integer("origin_id").notNullable();
            table.integer("destination_id").notNullable();
            table.foreign("origin_id").references("attractions.id");
            table.foreign("destination_id").references("attractions.id");
        });
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("routes");
}

