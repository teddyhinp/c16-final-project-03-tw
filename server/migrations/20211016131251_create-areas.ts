import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('areas');
    if(!hasTable){
        await knex.schema.createTable('areas',(table)=>{
            table.increments();
            table.string("name").notNullable().unique();
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('areas');
}

