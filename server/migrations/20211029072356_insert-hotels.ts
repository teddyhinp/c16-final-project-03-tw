import { Knex } from "knex";
import { Client } from "pg";
import dotenv from "dotenv";

dotenv.config();

export async function up(knex: Knex): Promise<void> {
  // if(process.env.NODE_ENV === 'production') {return}
  const client = new Client({
    host: process.env.RDS_HOST,
    port: 5432,
    database: process.env.RDS_NAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
  });

  await client.connect();

  // Inserts seed entries
  const result = await client.query(
    "SELECT * FROM place WHERE categories1 = $1",
    ["hotels"]
  );
  const hotels = result.rows;
  for (const hotel of hotels) {
    const sub_district_id = await knex
      .select("id")
      .from("sub_districts")
      .where("name", `${hotel.sub_district}`)
      .first();
    await knex("hotels")
      .insert({
        name: hotel.name,
        categories: hotel.categories2,
        address: hotel.address,
        phone_number: hotel.phone_number,
        latitude: hotel.latitude,
        longitude: hotel.longitude,
        sub_district_id: sub_district_id.id,
      })
      .into("hotels");
    //   .returning("id");
  }

  await client.end();
}

export async function down(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex("hotels").del();
}
