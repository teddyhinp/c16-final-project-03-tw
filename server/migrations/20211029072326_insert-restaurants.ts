import { Knex } from "knex";
import { Client } from "pg";
import dotenv from "dotenv";

dotenv.config();

export async function up(knex: Knex): Promise<void> {
  // if(process.env.NODE_ENV === 'production') {return}
  const client = new Client({
    host: process.env.RDS_HOST,
    port: 5432,
    database: process.env.RDS_NAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
  });

  await client.connect();

  // Inserts seed entries
  const result = await client.query(
    "SELECT * FROM place WHERE categories1 = $1",
    ["restaurants"]
  );
  const restaurants = result.rows;
  for (const restaurant of restaurants) {
    const sub_district_id = await knex
      .select("id")
      .from("sub_districts")
      .where("name", `${restaurant.sub_district}`)
      .first();
    await knex("restaurants")
      .insert({
        name: restaurant.name,
        categories: restaurant.categories2,
        address: restaurant.address,
        latitude: restaurant.latitude,
        longitude: restaurant.longitude,
        sub_district_id: sub_district_id.id,
      })
      .into("restaurants");
    //   .returning("id");
  }

  const enName = [
    "American Restaurant",
    "Chinese Restaurant",
    "Hotpot Restaurant",
    "Japanese Restaurant",
    "Korean Restaurant",
    "Noodle House",
    "Thai Restaurant",
    "Vietnamese Restaurant",
    "Buffet",
    "Coffee Shop",
    "Dessert Shop",
    "French Restaurant",
    "Indian Restaurant",
    "Italian Restaurant",
    "Vegan Restaurant",
  ];
  const cnName = [
    "美式餐廳",
    "中菜",
    "火鍋",
    "日本菜",
    "韓國菜",
    "麵館",
    "泰國菜",
    "越南菜",
    "自助餐",
    "咖啡店",
    "甜品/糖水鋪",
    "法國菜",
    "印度菜",
    "意大利菜",
    "素食",
  ];

  for (let i = 0; i < enName.length; i++) {
    await knex("restaurants")
      .update({ categories: cnName[i] })
      .where({ categories: enName[i] });
  }

  await client.end();
}

export async function down(knex: Knex): Promise<void> {
  await knex("restaurants").del();
}
