import { Knex } from "knex";
import { Client } from "pg";
import dotenv from "dotenv";
import jsonfile from "jsonfile";

dotenv.config();

export async function up(knex: Knex): Promise<void> {
  // if(process.env.NODE_ENV === 'production') {return}
  const client = new Client({
    host: process.env.RDS_HOST,
    port: 5432,
    database: process.env.RDS_NAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
  });

  const catList = {
    文化藝術: [
      "Amphitheater",
      "Art Gallery",
      "Art Museum",
      "Arts & Entertainment",
      "Comedy Club",
      "History Museum",
      "indie Theater",
      "Movie Theater",
      "Multiplex",
      "Museum",
      "Performing Arts Venue",
      "Planetarium",
      "Public Art",
      "Science Museum",
      "Theater",
    ],
    演藝娛樂: [
      "Concert Hall",
      "Country Dance Club",
      "Dance Studio",
      "Exhibit",
      "Go Kart Track",
      "Laser Tag",
      "Music Venue",
      "Opera House",
      "Reacecourse",
    ],
    戶外探索: ["Mini Golf", "Outdoor Sculpture", "Racecourse", "Memorial Site"],
    朋友相聚: ["Rock Club", "Rugby Stadium"],
    主題樂園: ["Theme Park", "VR Cafe", "Zoo Exhibit"],
  };

  await client.connect();

  // Inserts seed entries
  const result = await client.query(
    "SELECT * FROM place WHERE categories1 = $1",
    ["attractions"]
  );
  const attractions = result.rows;
  for (const attraction of attractions) {
    const sub_district_id = await knex
      .select("id")
      .from("sub_districts")
      .where("name", `${attraction.sub_district}`)
      .first();
    await knex("attractions")
      .insert({
        name: attraction.name,
        categories: attraction.categories2,
        address: attraction.address,
        description: attraction.description,
        latitude: attraction.latitude,
        longitude: attraction.longitude,
        img_url: attraction.image_url,
        sub_district_id: sub_district_id.id,
      })
      .into("attractions");
    //   .returning("id");
  }

  //update my_cat
  for (const key in catList) {
    await knex("attractions")
      .returning("id")
      .whereIn("categories", catList[key])
      .update({
        my_category: key,
      });
  }

  //update descriptions
  const descriptions = jsonfile.readFileSync(
    "../data/wiki_description/resources/filteredDescription2.json"
  );
  for (const description of descriptions) {
    if (description.description.length >= 255) {
      continue;
    }

    await knex("attractions")
      //   .returning("id")
      .where("name", description.name)
      .update({
        description: description.description,
      });
  }

  await client.end();
}

export async function down(knex: Knex): Promise<void> {
  await knex("attractions").del();
}
