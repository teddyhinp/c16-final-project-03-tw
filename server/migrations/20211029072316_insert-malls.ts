import { Knex } from "knex";
import { Client } from "pg";
import dotenv from "dotenv";

dotenv.config();

export async function up(knex: Knex): Promise<void> {
  // if(process.env.NODE_ENV === 'production') {return}
  const client = new Client({
    host: process.env.RDS_HOST,
    port: 5432,
    database: process.env.RDS_NAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
  });

  await client.connect();

  // Inserts seed entries
  const result = await client.query(
    "SELECT * FROM place WHERE categories1 = $1",
    ["malls"]
  );
  const malls = result.rows;
  for (const mall of malls) {
    const sub_district_id = await knex
      .select("id")
      .from("sub_districts")
      .where("name", `${mall.sub_district}`)
      .first();
    await knex("malls")
      .insert({
        name: mall.name,
        address: mall.address,
        latitude: mall.latitude,
        longitude: mall.longitude,
        img_url: mall.image_url,
        sub_district_id: sub_district_id.id,
      })
      .into("malls");
    //   .returning("id");
  }

  await client.end();
}

export async function down(knex: Knex): Promise<void> {
  await knex("malls").del();
}
