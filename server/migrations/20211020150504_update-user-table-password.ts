import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable("users", (t) => {
    t.string("password").nullable().alter();
    t.string("ageGroup").nullable().alter();
    t.string("gender").nullable().alter();
    t.string("preference1").nullable().alter();
    t.string("preference2").nullable().alter();
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable("users", (t) => {
    t.string("password").notNullable().alter();
    t.string("ageGroup").notNullable().alter();
    t.string("gender").notNullable().alter();
    t.string("preference1").notNullable().alter();
    t.string("preference2").notNullable().alter();
  });
}
