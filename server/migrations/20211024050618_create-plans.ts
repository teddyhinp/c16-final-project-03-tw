import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("plans");
  if (!hasTable) {
    await knex.schema.createTable("plans", (table) => {
      table.increments();
      table.string("name").notNullable();
      table.integer("user_id").notNullable();
      table.foreign("user_id").references("users.id");
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("plans");
}
