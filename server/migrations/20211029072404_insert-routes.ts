import { Knex } from "knex";
import jsonfile from "jsonfile";

export async function up(knex: Knex): Promise<void> {
  // if(process.env.NODE_ENV === 'production') {return}
  const routes1 = jsonfile.readFileSync(
    "./../data/coordinate/resources/route.json"
  );
  const routes2 = jsonfile.readFileSync(
    "./../data/coordinate/resources/route2.json"
  );

  // Inserts seed entries
  for (const route1 of routes1) {
    const origin_id = await knex
      .select("id")
      .from("attractions")
      .where("name", route1.origin)
      .first();

    const destination_id = await knex
      .select("id")
      .from("attractions")
      .where("name", route1.destination)
      .first();

    await knex("routes")
      .insert({
        distance: route1.distance,
        duration: route1.time,
        origin_id: origin_id.id,
        destination_id: destination_id.id,
      })
      .into("routes");
    //   .returning("id");
  }

  for (const route2 of routes2) {
    const origin_id = await knex
      .select("id")
      .from("attractions")
      .where("name", route2.origin)
      .first();

    const destination_id = await knex
      .select("id")
      .from("attractions")
      .where("name", route2.destination)
      .first();

    await knex("routes")
      .insert({
        distance: route2.distance,
        duration: route2.time,
        origin_id: origin_id.id,
        destination_id: destination_id.id,
      })
      .into("routes");
    //   .returning("id");
  }
}

export async function down(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex("routes").del();
}
