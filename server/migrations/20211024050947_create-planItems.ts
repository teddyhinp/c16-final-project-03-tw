import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  const hasTable = await knex.schema.hasTable("planItems");
  if (!hasTable) {
    await knex.schema.createTable("planItems", (table) => {
      table.increments();
      table.string("attractions_name").notNullable();
      table.integer("attractions_id").notNullable();
      table.integer("user_id").notNullable();
      table.integer("stay_time").unsigned;
      table.integer("plan_id").notNullable().unsigned;
      table.foreign("plan_id").references("plans.id");
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("planItems");
}
