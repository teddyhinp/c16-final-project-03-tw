import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('restaurants');
    if(!hasTable){
        await knex.schema.createTable('restaurants',(table)=>{
            table.increments();
            table.string("name").notNullable();
            table.string("categories").notNullable();
            table.string("address");
            table.float("latitude").notNullable();
            table.float("longitude").notNullable();
            table.integer("sub_district_id").notNullable().unsigned();
            table.foreign("sub_district_id").references("sub_districts.id");
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('restaurants');
}

