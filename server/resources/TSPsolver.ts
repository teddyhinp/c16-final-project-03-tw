import TSPSolver from "@nikbelikov/tsp-solver";

export function TSP(maximumTime: number, points: any, transportValues: any) {
  if (points.length > 0 && points[0].time > maximumTime) {
    return [];
  }

  const values = [];
  for (const transportValue of transportValues) {
    values.push({
      set: transportValue.set,
      value: transportValue.value,
      distance: transportValue.distance,
    });
  }

  const solved = TSPSolver(points, values, { generations: 10000 });

  let trafficTime = 0;
  let distance = 0;
  let timetaken = points[0].time;
  let currentLocation = 0;
  let attractionRoute = [
    {
      atraction_id: points[0].attraction_id,
      trafficDistance: 0,
      trafficTime: 0,
      stayTime: points[0].time,
      timetaken: points[0].time,
    },
  ];

  if (!solved.latestPopulation) {
    for (let i = 0; i < solved.result.length - 1; i++) {
      for (const value of values) {
        if (
          (value.set[0] == currentLocation &&
            value.set[1] == solved.result[i + 1].id) ||
          (value.set[1] == currentLocation &&
            value.set[0] == solved.result[i + 1].id)
        ) {
          trafficTime = value.value;
          distance = value.distance;
          break;
        }
      }
      currentLocation = solved.result[i + 1].id;
      timetaken += trafficTime + points[currentLocation].time;

      if (timetaken > maximumTime) {
        break;
      }

      attractionRoute.push({
        atraction_id: points[currentLocation].attraction_id,
        trafficDistance: distance,
        trafficTime: trafficTime,
        stayTime: points[currentLocation].time,
        timetaken: timetaken,
      });
    }
    return attractionRoute;
  }

  const routeTemp = solved.latestPopulation[0].chromosome;

  for (let i = 1; i < points.length; i++) {
    for (const value of values) {
      if (
        (value.set[0] == currentLocation && value.set[1] == routeTemp[i]) ||
        (value.set[1] == currentLocation && value.set[0] == routeTemp[i])
      ) {
        trafficTime = value.value;
        distance = value.distance;
        break;
      }
    }
    timetaken += timetaken + trafficTime + points[currentLocation].time;
    currentLocation = routeTemp[i];

    if (timetaken > maximumTime) {
      break;
    }

    attractionRoute.push({
      atraction_id: points[routeTemp[i]].attraction_id,
      trafficDistance: distance,
      trafficTime: trafficTime,
      stayTime: points[routeTemp[i]].time,
      timetaken: timetaken,
    });
  }

  return attractionRoute;
}
