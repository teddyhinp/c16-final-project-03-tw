

export function objectToCSVRow(recordObject:Object) {
    var recordArray = new Array;
    for (let key in recordObject) {
        const innerValue = recordObject[key]===null?'':recordObject[key].toString();
        let data = innerValue.replace(/"/g, '""');
        data = '"' + data + '"';
        recordArray.push(data);
    }
    return recordArray.join(' ') + '\r\n';
}