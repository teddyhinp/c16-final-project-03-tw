export const sub_district_coordinates = {
    "堅尼地城": [22.2822235, 114.1292619],
    "石塘咀": [22.2863914, 114.1338924],
    "西營盤": [22.2875634, 114.1417059],
    "上環": [22.2863943, 114.1491375],
    "中環": [22.2799907, 114.1587983],
    "金鐘": [22.2796357, 114.1654865],
    "半山區": [22.2816223, 114.147865],
    "山頂": [22.2776827, 114.1591909],
    "灣仔": [22.276022, 114.1751471],
    "銅鑼灣": [22.2859787, 114.1914919],
    "跑馬地": [22.268352, 114.1865135],
    "大坑": [22.2751881, 114.1937244],
    "掃桿埔": [22.2731007, 114.1892659],
    "渣甸山": [22.2666667, 114.2],
    "天后": [22.2823972, 114.1922382],
    "寶馬山": [22.2847861, 114.1998006],
    "北角": [22.2884683, 114.192803],
    "鰂魚涌": [22.2827206, 114.2123009],
    "西灣河": [22.2824738, 114.2227035],
    "筲箕灣": [22.278746, 114.2286476],
    "柴灣": [22.2678103, 114.2360778],
    "小西灣": [22.2615148, 114.2494516],
    "薄扶林": [22.2679201, 114.1290719],
    "香港仔": [22.2483068, 114.1524398],
    "鴨脷洲": [22.2418581, 114.1528532],
    "黃竹坑": [22.2459198, 114.1664957],
    "壽臣山": [22.2471845, 114.1810919],
    "淺水灣": [22.2345043, 114.1952039],
    "舂磡角": [22.2183333, 114.2048704],
    "赤柱": [22.220422, 114.2126685],
    "大潭": [22.2567774, 114.2106427],
    "石澳": [22.2334016, 114.2420218],
    "尖沙咀": [22.2988123, 114.1721746],
    "油麻地": [22.3068537, 114.1714423],
    "西九龍填海區": [22.3034757, 114.1602846],
    "京士柏": [22.3095048, 114.1751471],
    "旺角": [22.3203648, 114.169773],
    "大角咀": [22.3198474, 114.1620857],
    "美孚": [22.3366356, 114.1402195],
    "荔枝角": [22.3377817, 114.1359272],
    "長沙灣": [22.3358055, 114.1494892],
    "深水埗": [22.3285899, 114.1602846],
    "石硤尾": [22.3337786, 114.1674019],
    "又一村": [22.3312462, 114.1744039],
    "大窩坪": [22.341024, 114.1654865],
    "昂船洲": [22.3228031, 114.1350173],
    "紅磡": [22.3055658, 114.1887233],
    "土瓜灣": [22.3173373, 114.1879691],
    "馬頭角": [22.3206419, 114.1929813],
    "馬頭圍": [22.3223949, 114.1862935],
    "啟德": [22.3246084, 114.1990482],
    "九龍城": [22.3232097, 114.1855505],
    "何文田": [22.318507, 114.1755339],
    "九龍塘": [22.3369319, 114.1763367],
    "筆架山": [22.3495063, 114.1692021],
    "新蒲崗": [22.3364426, 114.1989259],
    "黃大仙": [22.3429615, 114.1929813],
    "東頭": [22.3330898, 114.1933481],
    "橫頭磡": [22.3405302, 114.1862935],
    "樂富": [22.3377402, 114.1862935],
    "鑽石山": [22.35, 114.2],
    "慈雲山": [22.3515518, 114.200412],
    "牛池灣": [22.3356369, 114.2084317],
    "坪石": [22.3323748,114.2119894],
    "九龍灣": [22.3080749, 114.2018982],
    "牛頭角": [22.322466, 114.2167592],
    "佐敦谷": [22.3244408, 114.2175022],
    "觀塘": [22.3103686, 114.2227035],
    "秀茂坪": [22.3186108, 114.2348969],
    "藍田": [22.3080199, 114.2375638],
    "油塘": [22.2940736, 114.2375638],
    "鯉魚門": [22.2848099, 114.2377354],
    "葵涌": [22.3668502, 114.1302591],
    "青衣": [22.3474872, 114.1023164],
    "荃灣": [22.3699122, 114.1144306],
    "梨木樹": [22.3792004, 114.1357605],
    "汀九": [22.3768203, 114.0755593],
    "深井": [22.367102, 114.058809],
    "青龍頭": [22.3631111, 114.0473141],
    "馬灣": [22.3500753, 114.059207],
    "欣澳": [22.331731, 114.0289607],
    "大欖涌": [22.3641206, 114.0227101],
    "掃管笏": [22.3754639, 113.9987128],
    "屯門": [22.3908295, 113.9725126],
    "藍地": [22.4216464, 113.981979],
    "洪水橋": [22.4346478, 113.9927005],
    "廈村": [22.4464878, 113.9860245],
    "流浮山": [22.468172, 113.984489],
    "天水圍": [22.4606419, 114.0041996],
    "元朗": [22.4445376, 114.0222076],
    "新田": [22.4950121, 114.0696131],
    "落馬洲": [22.5098835, 114.0815055],
    "錦田": [22.440058, 114.0650077],
    "石崗": [22.4249178, 114.0753753],
    "八鄉": [22.4465916, 114.0933976],
    "粉嶺": [22.4916829, 114.1414685],
    "聯和墟": [22.5006876, 114.1398274],
    "上水": [22.5112008, 114.1260991],
    "石湖墟": [22.502011, 114.130402],
    "沙頭角": [22.531577, 114.20324],
    "鹿頸": [22.520629, 114.2138563],
    "烏蛟騰": [22.5058269, 114.2441439],
    "大埔墟": [22.4445559, 114.1704893],
    "大埔": [22.4423218, 114.1655064],
    "大埔滘": [22.4370545, 114.1828429],
    "大尾篤": [22.4728367, 114.232961],
    "船灣": [22.5081084, 114.2509376],
    "樟木頭": [22.428573, 114.248978],
    "企嶺下": [22.4107, 114.27302],
    "大圍": [22.375941, 114.178626],
    "沙田": [22.3771304, 114.1974398],
    "火炭": [22.3985709, 114.1929428],
    "馬料水": [22.4179845, 114.2029277],
    "烏溪沙": [22.4291767, 114.2438963],
    "馬鞍山": [22.4276756, 114.2402751],
    "清水灣": [22.2851406, 114.2940282],
    "西貢": [22.383689, 114.2707867],
    "大網仔": [22.3859795, 114.3080781],
    "將軍澳": [22.3119357, 114.2568776],
    "坑口": [22.3172623, 114.2672828],
    "調景嶺": [22.3038, 114.252098],
    "馬游塘": [22.321765, 114.244353],
    "長洲": [22.2016179, 114.0265007],
    "坪洲": [22.2830679, 114.0413675],
    "大嶼山": [22.2664984, 113.941751],
    "東涌": [22.2873743, 113.9425086],
    "南丫島": [22.2000056, 114.1350173]
}

export const sub_districts = [
        "堅尼地城",
        "石塘咀",
        "西營盤",
        "上環",
        "中環",
        "金鐘",
        "半山區",
        "山頂",  
        "銅鑼灣",
        "跑馬地",
        "大坑",
        "掃桿埔",
        "渣甸山",
        "天后",
        "寶馬山",
        "北角",
        "鰂魚涌",
        "西灣河",
        "筲箕灣",
        "柴灣",
        "小西灣",
        "薄扶林",
        "香港仔",
        "鴨脷洲",
        "黃竹坑",
        "壽臣山",
        "淺水灣",
        "舂磡角",
        "赤柱",
        "大潭",
        "石澳",
        "尖沙咀",
        "油麻地",
        "西九龍填海區",
        "京士柏",
        "旺角",
        "大角咀",
        "美孚",
        "荔枝角",
        "長沙灣",
        "石硤尾",
        "又一村",
        "大窩坪",
        "昂船洲",
        "紅磡",
        "土瓜灣",
        "馬頭角",
        "馬頭圍",
        "啟德",
        "九龍城",
        "何文田",
        "九龍塘",
        "筆架山",
        "新蒲崗",
        "東頭",
        "橫頭磡",
        "樂富",
        "鑽石山",
        "慈雲山",
        "牛池灣",
        "坪石",
        "九龍灣",
        "牛頭角",
        "佐敦谷",
        "秀茂坪",
        "藍田",
        "油塘",
        "鯉魚門",
        "葵涌",
        "青衣",
        "梨木樹",
        "汀九",
        "深井",
        "青龍頭",
        "馬灣",
        "欣澳",
        "大欖涌",
        "掃管笏",
        "藍地",
        "洪水橋",
        "廈村",
        "流浮山",
        "天水圍",
        "新田",
        "落馬洲",
        "錦田",
        "石崗",
        "八鄉",
        "粉嶺",
        "聯和墟",
        "上水",
        "石湖墟",
        "沙頭角",
        "鹿頸",
        "烏蛟騰",
        "大埔墟",
        "大埔滘",
        "大尾篤",
        "船灣",
        "樟木頭",
        "企嶺下",
        "大圍",
        "火炭",
        "馬料水",
        "烏溪沙",
        "馬鞍山",
        "清水灣",
        "大網仔",
        "將軍澳",
        "坑口",
        "調景嶺",
        "馬游塘",
        "長洲",
        "坪洲",
        "東涌",
        "南丫島"
]