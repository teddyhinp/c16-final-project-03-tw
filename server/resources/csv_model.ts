
export interface Record {
user_id:number;
gender:string;
ageGroup:string;
preference1:string;
preference2:string;
attraction_id:number;
attraction_category:string;
area_id: number;
area_name: string
district_id: number;
district_name: string;
sub_district_id: number;
sub_district_name: string;
action: string;
record_time: Date;
}

