import { sub_district_coordinates, sub_districts } from './sub_district_model';

export function getSubDistrictFromCoordinates(coordinates: number[]) { //[22.2183333, 114.2048704]
    let sub_district = Object.keys(sub_district_coordinates)[0];
    let shortest_distance = getDistance(coordinates, sub_district_coordinates[Object.keys(sub_district_coordinates)[0]]);
    for (let i = 1; i < Object.keys(sub_district_coordinates).length; i++) {
        const distance = getDistance(coordinates, sub_district_coordinates[Object.keys(sub_district_coordinates)[i]]);
        if (Number(distance) < Number(shortest_distance)) {
            shortest_distance = distance;
            sub_district = Object.keys(sub_district_coordinates)[i];
        }
    }
    return sub_district;
}

export function getDistance(coordinates1: number[], coordinates2: number[]) {
    return (coordinates1[0] - coordinates2[0]) ** 2 + (coordinates1[1] - coordinates2[1]) ** 2;
}

export function getSubDistrictFromAddress(address: string){
    for(const elem of sub_districts){
        if(address.includes(elem)){
            return elem;
        }
    }
    return null;
}