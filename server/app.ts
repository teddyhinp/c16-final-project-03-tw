import dotenv from "dotenv";
dotenv.config();

import Knex from "knex";
import express from "express";

import { UserController } from "./Controller/UserController";
import { UserService } from "./Service/UserService";
import { LocationService } from "./Service/LocationService";
import { LocationController } from "./Controller/LocationController";
import { CsvController } from "./Controller/recordController";
import cors from "cors";
const knexConfigs = require("./knexfile");
const environment = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[environment];
const knex = Knex(knexConfig);

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use((req, res, next) => {
  let time = new Date().toLocaleString();
  console.log(time, req.method, req.url);
  next();
});

export const userService = new UserService(knex);
export const userController = new UserController(userService);
export const locationService = new LocationService(knex);
export const locationController = new LocationController(locationService);
export const csvController = new CsvController(locationService, userService);

import { routes } from "./routes";
app.use("/", routes);
let PORT = process.env.PORT;
if (!PORT) {
  throw new Error("missing PORT in env");
}
let port = +PORT;
app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${port}`);
});
