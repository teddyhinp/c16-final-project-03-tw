import { locationController, userController, csvController } from "./app";
import express from "express";
import { optionalJWT } from "./auth/guard";
export const routes = express.Router();

routes.use(optionalJWT);

//user
routes.post("/register", userController.createUser);
routes.post("/editProfile", userController.editProfile);
routes.post("/login/password", userController.loginWithPassword);
routes.post("/login/facebook", userController.loginWithFacebook);
routes.get("/getPreference/:user_id", userController.getPreference);

//Tab2
routes.get("/attractions", locationController.getDistrict);
routes.get(
  "/attractions/category/:category_name",
  locationController.getItemByCategory
);
routes.get("/facilities", locationController.getFacilities);
routes.get("/district", locationController.getDistrict);
routes.get("/district/:id", locationController.getSubDistrict);
routes.get("/categories", locationController.getCategories);
routes.get(
  "/categories/:selectedSubDistrict_id/:selectedCategories",
  locationController.getRestaurantList
);
routes.get("/malls", locationController.getDistrict);

//LoadUnitAttractions
routes.get("/attractions/:district", locationController.getUnitAttractions);
routes.get("/facilities/:facility", locationController.getUnitFacilities);
routes.get(
  "/getUnitAttractionsDetail/:location_id",
  locationController.getUnitAttractionsDetail
);

//getOneMall
routes.get("/malls/:district", locationController.loadOneMall);
routes.get(
  "/malls/:district/:location_id",
  locationController.loadOneMallDetail
);

//bookmark
routes.post("/addbookmark", locationController.addBookmark); //
routes.get("/loadbookmark/:user_id", locationController.loadBookmark);
routes.delete(
  "/delbookmark/:user_id/:attractions_id",
  locationController.deleteBookmark
);

//plan
routes.post("/addPlan/:user_id", locationController.addPlan);
routes.get("/getPlanItems/:user_id", locationController.getPlanItems);
routes.get(
  "/loadPreferenceItems0/:preference0",
  locationController.loadPreferenceItems0
);
routes.get(
  "/loadPreferenceItems1/:preference1",
  locationController.loadPreferenceItems1
);
routes.delete(
  "/delplanItem/:user_id/:attraction_id",
  locationController.delplanItem
);

routes.post("/submitPlan/:user_id", locationController.submitPlan);

//hotel
routes.get(
  "/loadHotelList/:selectedSubDistrict_id/:selectedCategories",
  locationController.loadHotelList
);

//route
routes.get("/route/:origin_id/:destination_id", locationController.getRoute);
//TSP Solver
routes.post("/loadRoutes", locationController.loadRoutes);

//use to record action, route = action name
routes.get("/search/:user_id/:attractions_id", csvController.recordAction);

//resumePlan
routes.get("/resumePlan/:user_id", locationController.resumePlan);
routes.delete("/resumePlan/delplan/:plan_id", locationController.delPlan);
