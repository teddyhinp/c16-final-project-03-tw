import { Response, Request, NextFunction } from "express";
import { Bearer } from "permit";
import jwtSimple from "jwt-simple";
import { jwtConfig } from "./jwt";
import { JWTPayload } from "./types";

const permit = new Bearer({
  query: "access_token",
});

export function requireJWT(req: Request, res: Response, next: NextFunction) {
  let token: string;
  try {
    token = permit.check(req);
    if (!token) {
      res.json({ error: "missing Bearer token in req" });
      return;
    }
  } catch (error) {
    res.json({ error: "failed to decode Bearer token in req" });
    return;
  }

  let payload: JWTPayload;
  try {
    payload = jwtSimple.decode(token, jwtConfig.SECRET);
  } catch (error) {
    res.json({ error: "failed to decode JWT token in req" });
    return;
  }

  // TODO validate the user permission with database using UserService

  req.jwtPayload = payload;
  next();
}

export function optionalJWT(req: Request, res: Response, next: NextFunction) {
  try {
    let token = permit.check(req);
    let payload: JWTPayload = jwtSimple.decode(token, jwtConfig.SECRET);
    if (payload && payload.id) {
      req.jwtPayload = payload;
      req.params.user_id = req.params.user_id || String(payload.id);
    }
  } catch (error) {}
  next();
}
