import { JWTPayload } from 'shared'
export { JWTPayload } from 'shared'
export * from 'shared'

declare global {
  namespace Express {
    interface Request {
      jwtPayload: JWTPayload
    }
  }
}
