#!/usr/bin/bash
set -e
set -o pipefail
set -x

for f in $(ls migrations); do
  npx knex migrate:up
done
