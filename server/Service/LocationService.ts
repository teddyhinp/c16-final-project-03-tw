import { Knex } from "knex";

export class LocationService {
  constructor(public knex: Knex) {}
  //Tab 2 getDistrict,getFacilities
  async getDistrict() {
    return await this.knex.select("*").from("districts");
  }

  async getSubDistrict(district_id: number) {
    return await this.knex
      .select("*")
      .from("sub_districts")
      .where({ district_id });
  }

  async getCategories() {
    return await this.knex.raw("select DISTINCT categories from restaurants");
  }

  async getRestaurantList(sub_district_id: number, categories: string) {
    return await this.knex
      .select("*")
      .from("restaurants")
      .where({ sub_district_id })
      .where({ categories });
  }

  async getFacilities() {
    return await this.knex.raw("select DISTINCT categories from facilities");
  }

  //LoadUnitAttractions
  async getUnitAttractions(district: string) {
    let district_idStr = await this.knex
      .select("id")
      .from("districts")
      .where("name", district);

    console.log("district_idStr", district_idStr);
    let district_id = district_idStr[0].id;
    let sub_districts_ids = await this.knex
      .select("id")
      .from("sub_districts")
      .where("district_id", district_id);

    console.log("sub_districts_ids", sub_districts_ids);

    let attractionsArray = [];
    for (let sub_districts_id of sub_districts_ids) {
      let unitDetail = await this.knex
        .select("*")
        .from("attractions")
        .where("sub_district_id", sub_districts_id.id);

      attractionsArray.push(unitDetail);
    }
    // console.log(attractionsArray, "attractionsArray");
    return attractionsArray;
  }

  async getUnitFacilities(facility: string) {
    return await this.knex
      .select("*")
      .from("facilities")
      .where("categories", facility);
  }

  async getUnitAttractionsDetail(location_id: number) {
    return await this.knex
      .select("*")
      .from("attractions")
      .where("id", location_id);
  }

  async getItemByCategory(category_name: string) {
    return await this.knex
      .select("*")
      .from("attractions")
      .where("my_category", category_name);
  }

  //bookmark
  async addBookmark(
    user_id: number,
    attractions_id: number,
    attractions_name: string
  ) {
    let row = await this.knex
      .select("attractions_id")
      .from("bookmark")
      .where({ attractions_id })
      .where({ user_id })
      .first();

    if (!row) {
      console.log("not duplicate");
      return this.knex
        .insert({
          user_id,
          attractions_id,
          attractions_name,
        })
        .into("bookmark");
    } else {
      console.log("duplicate bookmark");
    }
    return;
  }

  async loadBookmark(user_id: number) {
    return await this.knex
      .from("bookmark")
      .innerJoin("attractions", "attractions.id", "bookmark.attractions_id")
      .where({ user_id });
  }

  async deleteBookmark(attractions_id: number, user_id: number) {
    await this.knex("bookmark")
      .where("attractions_id", attractions_id)
      .where({ user_id })
      .del();
  }

  //plan
  async addPlan(
    user_id: number,
    attractions_id: number,
    attractions_name: string
  ) {
    let row = await this.knex
      .select("attractions_id")
      .from("planItems")
      .where({ attractions_id })
      .where({ user_id })
      .first();

    console.log(row);
    if (!row) {
      console.log("not duplicate");
      return this.knex
        .insert({
          user_id,
          attractions_id,
          attractions_name,
        })
        .into("planItems");
    } else {
      console.log("duplicate addplan");
    }
    return;
  }

  async getPlanItems(user_id: number) {
    return await this.knex.select("*").from("planItems").where({ user_id });
  }

  async submitPlan(
    plan_name: string,
    selected_date: string,
    json_plan: string,
    user_id: number
  ) {
    let row = await this.knex
      .select("plan_name")
      .from("plans")
      .where({ plan_name })
      .where({ user_id })
      .first();

    console.log(row);
    if (!row) {
      console.log("not duplicate");
      return this.knex
        .insert({
          plan_name,
          selected_date,
          json_plan,
          user_id,
        })
        .into("plans");
    } else {
      console.log("duplicate addplan");
    }
    return;
  }

  //LoadOneMall
  async LoadOneMall(district: string) {
    let district_idStr = await this.knex
      .select("id")
      .from("districts")
      .where("name", district);

    console.log("district_idStr", district_idStr);
    let district_id = district_idStr[0].id;
    let sub_districts_ids = await this.knex
      .select("id")
      .from("sub_districts")
      .where("district_id", district_id);

    console.log("sub_districts_ids", sub_districts_ids);

    let mallsArray = [];
    for (let sub_districts_id of sub_districts_ids) {
      let unitDetail = await this.knex
        .select("*")
        .from("malls")
        .where("sub_district_id", sub_districts_id.id);

      mallsArray.push(unitDetail);
    }
    // console.log(attractionsArray, "attractionsArray");
    return mallsArray;
  }
  async loadOneMallDetail(location_id: number) {
    return await this.knex.select("*").from("malls").where("id", location_id);
  }

  async loadPreferenceItems0(preference0: string) {
    return await this.knex
      .select("*")
      .from("attractions")
      .where("my_category", preference0);
  }

  async loadPreferenceItems1(preference1: string) {
    return await this.knex
      .select("*")
      .from("attractions")
      .where("my_category", preference1);
  }
  async delplanItem(attraction_id: number, user_id: number) {
    console.log("del", attraction_id);
    await this.knex("planItems")
      .where("attractions_id", attraction_id)
      .where({ user_id })
      .del();
  }
  //hotel

  async loadHotelList(sub_district_id: number, categories: string) {
    return await this.knex
      .select("*")
      .from("hotels")
      .where({ sub_district_id })
      .where({ categories });
  }

  async getRoute(origin_id: number, destination_id: number) {
    return await this.knex
      .select(["distance", "duration"])
      .from("routes")
      .where("origin_id", origin_id)
      .where("destination_id", destination_id);
  }

  async getEventArea(attraction_id: number) {
    return await this.knex
      .select("id", "name", "my_catergory", "sub_district_id")
      .from("attractions")
      .where("id", attraction_id);
  }

  async getSubDistrictInfoById(sub_district_id: number) {
    return await this.knex
      .select("id", "name", "district_id")
      .from("sub_districts")
      .where("id", sub_district_id);
  }

  async getDistrictInfoById(district_id: number) {
    return await this.knex
      .select("id", "name", "area_id")
      .from("districts")
      .where("id", district_id);
  }

  async getAreaInfoById(area_id: number) {
    return await this.knex
      .select("id", "name", "area_id")
      .from("areas")
      .where("id", area_id);
  }

  async resumePlan(user_id: number) {
    return await this.knex.select("*").from("plans").where("user_id", user_id);
  }
  async delplan(plan_id: number) {
    await this.knex("plans").where("id", plan_id).del();
  }
}
