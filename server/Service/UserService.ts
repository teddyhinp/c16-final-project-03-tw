import { Knex } from "knex";
import { comparePassword, hashPassword } from "../auth/hash";
import jwtSimple from "jwt-simple";
import { JWTPayload } from 'shared'
import { jwtConfig } from "../auth/jwt";
import fetch from "node-fetch";
import type { ReactFacebookLoginInfo } from "react-facebook-login";

export class UserService {
  constructor(public knex: Knex) {}

  async loginWithPassword(username: string, password: string): Promise<string> {
    let row = await this.knex
      .select("id", "password")
      .from("users")
      .where({ username })
      .first();
    if (!row) {
      throw new Error("Wrong username");
    }
    if (!(await comparePassword(password, row.password))) {
      throw new Error("Wrong username or password");
    }
    let payload: JWTPayload = {
      id: row.id,
      username,
    };
    let token = jwtSimple.encode(payload, jwtConfig.SECRET);
    return token;
  }

  async createUser(
    username: string,
    password: string,
    gender: string,
    ageGroup: string,
    preference1: string,
    preference2: string
  ) {
    return this.knex
      .insert({
        username: username,
        password: await hashPassword(password),
        gender: gender,
        ageGroup: ageGroup,
        preference1: preference1,
        preference2: preference2,
      })
      .into("users");
  }

  async editProfile(
    id: number,
    password: string,
    preference1: string,
    preference2: string
  ) {
    return this.knex("users")
      .update({
        password: await hashPassword(password),
        preference1: preference1,
        preference2: preference2,
      })
      .where("id", id);
  }

  async getPreference(user_id: number) {
    return await this.knex
      .select("preference1", "preference2")
      .from("users")
      .where("id", user_id);
  }

  async loginWithFacebook(accessToken: string): Promise<string> {
    const fetchResponse = await fetch(
      `https://graph.facebook.com/me?access_token=${accessToken}&fields=name,email,picture`
    );
    let userInfo: ReactFacebookLoginInfo = await fetchResponse.json();
    console.log("facebook login user info", userInfo);

    let payload: JWTPayload = await this.knex.transaction(
      async (knex): Promise<JWTPayload> => {
        let user = await knex
          .select("id", "username")
          .from("users")
          .where({ email: userInfo.email })
          .first();
        if (user) {
          return user;
        }
        let picture: string | undefined = userInfo.picture?.data.url;
        let name: string | undefined = userInfo.name;
        let [id] = await knex
          .insert({
            email: userInfo.email,
            picture,
            name: userInfo.name,
          })
          .into("users")
          .returning("id");
        return {
          id: id as number,
          name,
          picture,
        };
      }
    );
    let token = jwtSimple.encode(payload, jwtConfig.SECRET);
    return token;
  }

  async reportIssue(email: string, issue: string) {
    return this.knex
      .insert({
        email: email,
        issue: issue,
      })
      .into("users");
  }

  async getUserInfoById(user_id: number){
    const [result] = await this.knex.
    select("id","ageGroup","gender","preference1", "preference2").from("users")
    .where("id",user_id);
    return result;
}
}
