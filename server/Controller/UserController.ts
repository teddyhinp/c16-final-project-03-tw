import { Request, Response } from "express";
import { UserService } from "../Service/UserService";

export class UserController {
  constructor(private userService: UserService) {}

  createUser = async (req: Request, res: Response) => {
    const { username, password, gender, ageGroup, preference1, preference2 } =
      req.body;

    // 實際應要check 埋user 個email 有無重覆
    await this.userService.createUser(
      username,
      password,
      gender,
      ageGroup,
      preference1,
      preference2
    );
    res.json({ success: true });
  };
  editProfile = async (req: Request, res: Response) => {
    const { id, password, preference1, preference2 } = req.body;

    await this.userService.editProfile(id, password, preference1, preference2);
    res.json({ success: true });
  };

  getPreference = async (req: Request, res: Response) => {
    let request = req.params;
    let user_id = parseInt(request.user_id);
    let preference = await this.userService.getPreference(user_id);
    res.json(preference);
  };

  loginWithPassword = async (req: Request, res: Response) => {
    let { username, password } = req.body;

    if (!username) {
      res.json({ error: "missing username in req.body" });
      return;
    }
    if (!password) {
      res.json({ error: "missing password in req.body" });
      return;
    }
    try {
      let token = await this.userService.loginWithPassword(username, password);
      res.json({ token });
    } catch (error) {
      console.log(error.message);
      res.json({
        error: (error as Error).toString(),
      });
    }
  };

  loginWithFacebook = async (req: Request, res: Response) => {
    let { accessToken } = req.body;
    if (!accessToken) {
      res.json({ error: "missing accessToken in req.body" });
      return;
    }

    try {
      let token = await this.userService.loginWithFacebook(accessToken);
      res.json({ token });
    } catch (error) {
      console.log(error.message);
      res.json({
        error: (error as Error).toString(),
      });
    }
  };
  
}
