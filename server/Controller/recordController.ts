import { Request, Response } from "express";
import { LocationService } from "../Service/LocationService";
import { UserService } from "../Service/UserService";
import { Record } from "../resources/csv_model";
import { objectToCSVRow } from "../resources/csvWriter";
import fs from 'fs';


export class CsvController {
    constructor(private LocationService: LocationService,
        private userService: UserService
    ) { }
    //Tab 2 getDistrict,getFacilities

    recordAction = async (req: Request, res: Response) => {
        const attraction_Id = +req.params.attraction_id;  //or body
        const user_id = +req.params.user_id;
        const action = req.route;
        let eventInfo = await this.LocationService.getEventArea(attraction_Id);
        let userInfo = await this.userService.getUserInfoById(user_id);
        let subId = eventInfo[0]["sub_district_id"];
        const subInfo = await this.LocationService.getSubDistrictInfoById(subId);
        let districtId: number = subInfo[0]["district_id"]
        const districtInfo = await this.LocationService.getDistrictInfoById(districtId);
        let areaId: number = districtInfo[0]["area_id"]
        let areaInfo = await this.LocationService.getAreaInfoById(areaId)

        let record_batch:Record[] = [];
        const record: Record = {
            user_id: userInfo[0]["id"],
            gender: userInfo[0]["gender"],
            ageGroup: userInfo[0]["ageGroup"],
            preference1: userInfo[0]["preference1"],
            preference2: userInfo[0]["preference2"],
            attraction_id: attraction_Id,
            attraction_category: eventInfo[0]["my_category"],
            area_id: areaId,
            area_name: areaInfo[0]["name"],
            district_id: districtId,
            district_name: districtInfo[0]["name"],
            sub_district_id: subId,
            sub_district_name: subInfo[0]["name"],
            action: action,
            record_time: new Date(),
        }
        const dataRow = objectToCSVRow(record)
        // const csvFile = fs.createWriteStream('../resources.csv',{flags:'a'});
        if (!fs.existsSync(`../resources+${action}.csv`)) {
            fs.writeFileSync(`../resources+${action}.csv`, dataRow)
            console.log(`new action record created: ${action}`)
        }else{
        fs.appendFileSync(`../resources+${action}.csv`, dataRow)
        console.log(`${action} :recording + 1`)}
        
        record_batch.push(record)
        
    }
}

