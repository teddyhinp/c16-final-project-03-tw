import { Request, Response } from "express";
import { LocationService } from "../Service/LocationService";
import { TSP } from "../resources/TSPsolver";

export class LocationController {
  constructor(private LocationService: LocationService) {}
  //Tab 2 getDistrict,getFacilities
  getDistrict = async (req: Request, res: Response) => {
    let location = await this.LocationService.getDistrict();
    res.json(location);
  };

  getSubDistrict = async (req: Request, res: Response) => {
    let id = +req.params.id;
    let location = await this.LocationService.getSubDistrict(id);
    res.json(location);
  };

  getCategories = async (req: Request, res: Response) => {
    let location = await this.LocationService.getCategories();
    res.json(location);
  };

  getRestaurantList = async (req: Request, res: Response) => {
    const request = req.params;
    let subDistrict_id = parseInt(request.selectedSubDistrict_id);
    let category = request.selectedCategories;
    console.log(request);
    // console.log(subDistrict_id, "subDistrict")
    // console.log(selectedCategories, "category")
    let location = await this.LocationService.getRestaurantList(
      subDistrict_id,
      category
    );
    res.json(location);
  };

  getFacilities = async (req: Request, res: Response) => {
    let location = await this.LocationService.getFacilities();
    res.json(location);
  };

  //LoadUnitAttractions
  getUnitAttractions = async (req: Request, res: Response) => {
    const request = req.params;
    let district = request.district;
    // console.log(district);
    let location = await this.LocationService.getUnitAttractions(district);
    res.json(location);
  };

  getUnitFacilities = async (req: Request, res: Response) => {
    const request = req.params;
    let facility = request.facility;
    // console.log(facility);
    let location = await this.LocationService.getUnitFacilities(facility);
    res.json(location);
  };

  getUnitAttractionsDetail = async (req: Request, res: Response) => {
    const request = req.params;
    console.log(request, "request");
    let location_id = parseInt(request.location_id);
    console.log(location_id, "location_id");

    let location = await this.LocationService.getUnitAttractionsDetail(
      location_id
    );
    res.json(location);
  };

  getItemByCategory = async (req: Request, res: Response) => {
    const request = req.params;
    console.log(request, "request");
    let category_name = request.category_name;
    let location = await this.LocationService.getItemByCategory(category_name);
    res.json(location);
  };

  addBookmark = async (req: Request, res: Response) => {
    try {
      const { user_id, attractions_id, attractions_name } = req.body;
      await this.LocationService.addBookmark(
        user_id,
        attractions_id,
        attractions_name
      );
      res.json({ success: true });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };

  loadBookmark = async (req: Request, res: Response) => {
    try {
      const request = req.params;
      const user_id = parseInt(request.user_id);
      let bookmark = await this.LocationService.loadBookmark(user_id);
      res.json(bookmark);
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };

  deleteBookmark = async (req: Request, res: Response) => {
    try {
      const request = req.params;
      const delBM_id = parseInt(request.attractions_id);
      const user_id = parseInt(request.user_id);
      console.log("request", request);
      await this.LocationService.deleteBookmark(delBM_id, user_id);
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };

  //plan
  addPlan = async (req: Request, res: Response) => {
    try {
      const attractions_name = req.body.name;
      const attractions_id = req.body.id;
      const user_id = parseInt(req.params.user_id);
      await this.LocationService.addPlan(
        user_id,
        attractions_id,
        attractions_name
      );
      res.json({ success: true });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };

  getPlanItems = async (req: Request, res: Response) => {
    const user_id = parseInt(req.params.user_id);
    if (!user_id) {
      res.status(400).json({ error: "missing user_id in req.params" });
      return;
    }
    let plans = await this.LocationService.getPlanItems(user_id);
    res.json(plans);
  };

  loadPreferenceItems0 = async (req: Request, res: Response) => {
    const preference0 = req.params.preference0;
    console.log(preference0);
    let plans = await this.LocationService.loadPreferenceItems0(preference0);
    res.json(plans);
  };
  loadPreferenceItems1 = async (req: Request, res: Response) => {
    const preference1 = req.params.preference1;
    console.log(preference1);
    let plans = await this.LocationService.loadPreferenceItems0(preference1);
    res.json(plans);
  };

  delplanItem = async (req: Request, res: Response) => {
    const delItemId = +req.params.attraction_id;
    const user_id = +req.params.user_id;
    await this.LocationService.delplanItem(delItemId, user_id);
  };

  submitPlan = async (req: Request, res: Response) => {
    try {
      const plan_name = req.body.plan_name;
      const selected_date = req.body.selected_date;
      const json_plan = req.body.json_plan;
      const user_id = parseInt(req.params.user_id);
      await this.LocationService.submitPlan(
        plan_name,
        selected_date,
        json_plan,
        user_id
      );
      res.json({ success: true });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };

  //LoadOneMall
  loadOneMall = async (req: Request, res: Response) => {
    const request = req.params;
    let district = request.district;
    // console.log(district);
    let location = await this.LocationService.LoadOneMall(district);
    res.json(location);
  };

  loadOneMallDetail = async (req: Request, res: Response) => {
    const request = req.params;
    console.log(request, "request");
    let location_id = parseInt(request.location_id);
    console.log(location_id, "location_id");

    let location = await this.LocationService.loadOneMallDetail(location_id);
    res.json(location);
  };

  //hotel

  loadHotelList = async (req: Request, res: Response) => {
    const sub_district_id = parseInt(req.params.selectedSubDistrict_id);
    const categories = req.params.selectedCategories;
    console.log(categories, "categories");
    console.log(sub_district_id, "sub_district_id");

    let hotels = await this.LocationService.loadHotelList(
      sub_district_id,
      categories
    );
    res.json(hotels);
  };

  getRoute = async (req: Request, res: Response) => {
    const origin_id = parseInt(req.params.origin_id);
    const destination_id = parseInt(req.params.destination_id);

    const result = await this.LocationService.getRoute(
      origin_id,
      destination_id
    );

    res.json({
      distance: await result[0]["distance"],
      duration: await result[0]["duration"],
    });
  };

  loadRoutes = async (req: Request, res: Response) => {
    const request = req.body;
    const maximumTime = request.maximumTime;
    let points = [];
    let transportValues = [];

    for (let i = 0; i < request.attractions.length; i++) {
      points.push({
        id: i,
        attraction_id: request.attractions[i].id,
        time: request.attractions[i].time,
      });
    }

    for (let i = 0; i < points.length; i++) {
      for (let y = i + 1; y < points.length; y++) {
        const result = await this.LocationService.getRoute(
          request.attractions[i].id,
          request.attractions[y].id
        );
        transportValues.push({
          set: [i, y],
          value: await result[0]["duration"],
          distance: await result[0]["distance"],
        });
      }
    }

    const routes = TSP(maximumTime, points, transportValues);

    res.status(200).json(routes);
  };

  resumePlan = async (req: Request, res: Response) => {
    const user_id = parseInt(req.params.user_id);
    let plans = await this.LocationService.resumePlan(user_id);
    res.json(plans);
  };

  delPlan = async (req: Request, res: Response) => {
    const plan_id = parseInt(req.params.plan_id);
    await this.LocationService.delplan(plan_id);
    res.json({ success: true });
  };
}
