import "./RegisterPage.css";
import React, { useState, FormEvent } from "react";
import {
  IonLabel,
  IonContent,
  IonHeader,
  IonPage,
  IonToolbar,
  IonButton,
  IonInput,
  IonSelect,
  IonSelectOption,
  IonItem,
  IonCheckbox,
  IonImg,
  IonButtons,
  IonTitle,
  IonBackButton,
} from "@ionic/react";
import { Form } from "react-bootstrap";
import { getAPIServer } from "../helpers/api";

export const RegisterPage: React.FC = () => {
  const [username, setUsername] = useState<string>();
  const [password, setPassword] = useState<string>();
  const [gender, setGender] = useState<string>();
  const [ageGroup, setAgeGroup] = useState<string>();
  const [preference1, setPreference1] = useState<string>();
  const [preference2, setPreference2] = useState<string>();
  const [checked, setChecked] = useState(false);

  function submit(event: FormEvent) {
    event.preventDefault();
    console.log("submit form data:", {
      username,
      password,
      gender,
      ageGroup,
      preference1,
      preference2,
      checked,
    });
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/tab/settings" />
          </IonButtons>

          <IonTitle>註冊</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonImg
          className="ion-img-center"
          src="/logo.gif"
          id="img1"
          hidden
        ></IonImg>
        <div
          style={{
            backgroundImage: "url(/logo.gif)",
            backgroundColor: "var(--ion-color-primary)",
            height: "40%",
            backgroundSize: "contain",
            width: "100%",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
          }}
        ></div>

        <Form onSubmit={submit}>
          <IonItem>
            <IonInput
              className="usernameInput ion-padding"
              value={username}
              placeholder="輸入用戶名"
              onIonChange={(e) => setUsername(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonInput
              className="passwordInput ion-padding"
              type="password"
              value={password}
              placeholder="輸入密碼"
              onIonChange={(e) => setPassword(e.detail.value!)}
            ></IonInput>
          </IonItem>
          <IonItem>
            <IonLabel>性別</IonLabel>
            <IonSelect
              className="genderInput ion-padding"
              value={gender}
              placeholder="請選擇性別"
              onIonChange={(e) => setGender(e.detail.value)}
            >
              <IonSelectOption value="male">男性</IonSelectOption>
              <IonSelectOption value="female">女性</IonSelectOption>
            </IonSelect>
          </IonItem>
          <IonItem>
            <IonLabel>年齡組別</IonLabel>
            <IonSelect
              className="ageGroupInput ion-padding"
              value={ageGroup}
              placeholder="年齡組別"
              onIonChange={(e) => setAgeGroup(e.detail.value)}
            >
              <IonSelectOption value="below12">12歲以下</IonSelectOption>
              <IonSelectOption value="12to17">12-17歲</IonSelectOption>
              <IonSelectOption value="18to30">18-30歲</IonSelectOption>
              <IonSelectOption value="31to49">31-49歲</IonSelectOption>
              <IonSelectOption value="above50">50歲以上</IonSelectOption>
            </IonSelect>
          </IonItem>

          <IonItem>
            <IonLabel>景點偏好</IonLabel>
            <IonSelect
              className="favColourInput ion-padding"
              value={preference1}
              placeholder="第一偏好"
              okText="OK"
              cancelText="Cancel"
              onIonChange={(e) => setPreference1(e.detail.value)}
            >
              <IonSelectOption value="文化藝術">文化藝術</IonSelectOption>
              <IonSelectOption value="演藝娛樂">演藝娛樂</IonSelectOption>
              <IonSelectOption value="戶外探索">戶外探索</IonSelectOption>
              <IonSelectOption value="朋友相聚">朋友相聚</IonSelectOption>
              <IonSelectOption value="主題樂園">主題樂園</IonSelectOption>
            </IonSelect>
          </IonItem>

          <IonItem>
            <IonLabel>景點偏好</IonLabel>
            <IonSelect
              className="favColourInput ion-padding"
              value={preference2}
              placeholder="第二偏好"
              okText="OK"
              cancelText="Cancel"
              onIonChange={(e) => setPreference2(e.detail.value)}
            >
              <IonSelectOption value="文化藝術">文化藝術</IonSelectOption>
              <IonSelectOption value="演藝娛樂">演藝娛樂</IonSelectOption>
              <IonSelectOption value="戶外探索">戶外探索</IonSelectOption>
              <IonSelectOption value="朋友相聚">朋友相聚</IonSelectOption>
              <IonSelectOption value="主題樂園">主題樂園</IonSelectOption>
            </IonSelect>
          </IonItem>

          <IonItem>
            <IonCheckbox
              checked={checked}
              onIonChange={(e) => setChecked(e.detail.checked)}
            />
            <IonLabel className="sign ion-padding">
              我接受所有條款和條件
            </IonLabel>
          </IonItem>

          <IonButton
            type="submit"
            className="button"
            color="warning"
            onClick={async () => {
              const formObj = {
                username,
                password,
                gender,
                ageGroup,
                preference1,
                preference2,
              };
              await fetch(getAPIServer() + `/register`, {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify(formObj),
              });
              alert("你已成功註冊");
            }}
          >
            註冊
          </IonButton>
        </Form>
      </IonContent>
    </IonPage>
  );
};
