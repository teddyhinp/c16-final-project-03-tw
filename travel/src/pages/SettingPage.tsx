import React, { useState } from "react";
import "./SettingPage.css";
import { Route, Redirect, Link } from "react-router-dom";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonToolbar,
  IonButton,
  IonImg,
  IonText,
  IonCard,
  IonBadge,
} from "@ionic/react";
import { RootState } from "../redux/state";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../redux/auth/action";
import { getAPIServer } from "../helpers/api";

export const SettingPage: React.FC = () => {
  const dispatch = useDispatch();
  const isLoggedIn = useSelector((state: RootState) => state.auth.isLoggedIn);
  const name = useSelector(
    (state: RootState) => state.auth.user?.name || state.auth.user?.username,
  )
  const user_id = useSelector(
    (state: RootState) => state.auth.user?.id
  )

  function doLogout() {
    dispatch(logout());
  }

  const [displayPreference, setDisplayPreference] = useState(false)
  const [preferences, setPreferences] = useState({ preference1: "1", preference2: "2" })
  const [msg, setMsg] = useState("顯示你的景點偏好")
  function onDisplay() {
    if (displayPreference === false) {
      setDisplayPreference(true)
      let loadPreference = async () => {
        const res = await fetch(getAPIServer() + `/getPreference/${user_id}`)
        const json = await res.json()
        console.log(json[0], "preference")
        setPreferences(json[0])
        setMsg('隱藏你的景點偏好')
      }
      loadPreference()
    } else {
      setDisplayPreference(false)
      setMsg('顯示你的景點偏好')
    }
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonImg className="ion-img-center" src="/logo.gif" id="img1"></IonImg>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        {!isLoggedIn ? (
          <>
            <IonText className="ion-text-center">
              <h4> 您好，立即登入您的帳戶</h4>
            </IonText>
            <div id="loginRegButtons">
              <Link to="/login">
                <IonButton className="pwLoginButton" color="warning">
                  登入
                </IonButton>
              </Link>

              <IonText className="ion-text-center">
                <h4> 沒有帳戶？ 立即創建！</h4>
              </IonText>
              <Link to="/register">
                <IonButton className="regAccount" color="warning">
                  註冊
                </IonButton>
              </Link>
            </div>

            <div id="termsAndConditions">
              <h3>一起讓旅行星球變得更好</h3>
              <Link to="/about">
                <IonButton expand="full" color="tertiary" id="aboutUs">
                  關於旅行星球
                </IonButton>
              </Link>

              <Link to="/terms">
                <IonButton expand="full" color="tertiary" id="terms">
                  使用者條款
                </IonButton>
              </Link>

              <Link to="/privacy">
                <IonButton expand="full" color="tertiary" id="privacy">
                  私隱政策
                </IonButton>
              </Link>

            </div>
          </>
        ) :
          <>

            <h4>歡迎回來，{name}
              {displayPreference ?
                <div>
                  <IonBadge color="tertiary" className='tertiary'>{preferences.preference1}</IonBadge>
                  <IonBadge color="secondary" className='secondary'>{preferences.preference2}</IonBadge>
                </div> : null}
            </h4>
            <IonButton onClick={onDisplay}>{msg}</IonButton>

            <div id="functionButtons">
              <IonButton className="logOut" color="warning" onClick={doLogout}>
                登出
              </IonButton>
              <Link to='editProfile'>
                <IonButton className="edit" color="warning" >
                  更改您的資料
                </IonButton>
              </Link>
            </div>
            <div id="termsAndConditions">
              <h3>一起讓旅行星球變得更好</h3>
              <Link to="/about">
                <IonButton expand="full" color="tertiary" id="aboutUs">
                  關於旅行星球
                </IonButton>
              </Link>

              <Link to="/terms">
                <IonButton expand="full" color="tertiary" id="terms">
                  使用者條款
                </IonButton>
              </Link>

              <Link to="/privacy">
                <IonButton expand="full" color="tertiary" id="privacy">
                  私隱政策
                </IonButton>
              </Link>
            </div>




          </>}

        <IonText>
          v 1.0.1
        </IonText>
      </IonContent>
    </IonPage>
  );
};
