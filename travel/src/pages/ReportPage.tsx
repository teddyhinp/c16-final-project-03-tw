import React, { useState, FormEvent } from "react";
import "./TermsPage.css";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonToolbar,
  IonImg,
  IonInput,
  IonList,
  IonTextarea,
  IonItemDivider,
  IonButton,
} from "@ionic/react";
import { getAPIServer } from "../helpers/api";

export const ReportPage: React.FC = () => {
  const [email, setEmail] = useState<string>();
  const [issue, setIssue] = useState<string>();

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonImg className="ion-img-center" src="/logo.gif" id="img1"></IonImg>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <h1>回報問題</h1>
        <div className="emailInput">
          <IonList>
            <IonItemDivider color="light">
              <IonInput
                placeholder="若需要我們回覆請填寫您的電子郵件"
                value={email}
                inputmode="email"
                onIonChange={(e) => setEmail(e.detail.value!)}
              ></IonInput>
            </IonItemDivider>

            <IonItemDivider color="light">
              <IonTextarea rows={5} cols={15}>
                <IonInput
                  placeholder="填寫更多相關訊息"
                  value={issue}
                  onIonChange={(e) => setIssue(e.detail.value!)}
                ></IonInput>
              </IonTextarea>
            </IonItemDivider>
          </IonList>
        </div>


        <IonButton
          type="submit"
          className="button"
          color="warning"
          onClick={async () => {
            const formObj = {
              email,
              issue
            };
            await fetch(getAPIServer() + `/report`, {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify(formObj),
            });
          }}
        >
          提交
        </IonButton>

      </IonContent>
    </IonPage>
  );
};
