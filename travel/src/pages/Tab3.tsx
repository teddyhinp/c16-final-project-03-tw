import { IonButton, IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { refresh } from 'ionicons/icons';
import { useState } from 'react';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
// import { Link } from 'react-router-dom';
import BookmarkCard from '../components/BookmarkCard';
import { del, getAPIServer } from '../helpers/api';
import { RootState } from '../redux/state';
import './Tab3.css';


type Bookmark = { id: number, user_id: number, attractions_id: number, attractions_name: string, sub_district_id: number }

const Tab3: React.FC = () => {
  const [bookmarks, setBookmarks] = useState<Bookmark[]>()
  const username = useSelector(
    (state: RootState) => state.auth.user?.name || state.auth.user?.username,
  )
  const user_id = useSelector(
    (state: RootState) => state.auth.user?.id
  )

  useEffect(() => {

    if (username) {
      console.log(username)
      let loadBookmark = async () => {
        const res = await fetch(getAPIServer() + `/loadbookmark/${user_id}`)
        const json = await res.json()
        console.log(json, "bookmarks")
        setBookmarks(json)
      }
      loadBookmark()
    }

  }, [user_id, username])


  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color='tertiary'>
          <IonTitle className='ion-text-center'>書籤</IonTitle>

        </IonToolbar>
      </IonHeader>

      <IonContent>
        {username && bookmarks ?
          bookmarks!.map(bookmark =>
            <BookmarkCard key={bookmark.id} name={bookmark.attractions_name} id={bookmark.attractions_id} sub_district_id={bookmark.sub_district_id} onRemove={async () => {
              setBookmarks(bookmarks => bookmarks!.filter(item => item !== bookmark))
              let attractions_id = bookmark.attractions_id
              console.log('del', bookmark.attractions_name)
              console.log('del', attractions_id)
              del(`/delbookmark/${user_id}/${attractions_id}`)

            }
            } />


          ) : <IonTitle size='small' className='ion-text-center'><h1>請先登入使用書籤功能</h1></IonTitle>}


      </IonContent>
    </IonPage >
  );
};

export default Tab3;
