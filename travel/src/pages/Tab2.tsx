import {
  IonCard,
  IonContent,
  IonHeader,
  IonPage,
  IonSearchbar,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import "./Tab2.css";
import React, { useEffect, useState } from "react";
import { IonModal } from "@ionic/react";
import { Button, ButtonGroup } from "react-bootstrap";
import { IonSegment, IonSegmentButton } from "@ionic/react";
import Attractions from "../components/Attractions";
import Facilities from "../components/Facilities";
import Restaurants from "../components/Restaurants";
import Malls from "../components/Malls";
import { useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { compassSharp } from "ionicons/icons";
import {
  FitBoundsOptions,
  LatLngBoundsExpression,
  Map as LeafletMap,
  MapOptions,
} from "leaflet";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { Link } from "react-router-dom";
import { getAPIServer } from "../helpers/api";

let defaultTab = "attractions";

const Tab2: React.FC = () => {
  type District = { name: string; id: number };
  type Facility = { categories: string };
  type Restaurant = { name: string; address: string; categories: string };

  const [tab, setTab] = useState<string | undefined>(defaultTab);
  const [bigTab, setbigTab] = useState<string | undefined>("district");
  const [showModal, setShowModal] = useState(false);
  const [districts, setDistricts] = useState<District[]>([]);
  const [facilities, setFacilitie] = useState<Facility[]>([]);
  const [restaurants, setRestaurants] = useState<Restaurant[]>([]);
  const categories = [{ name: '文化藝術', img: 'https://tecky-finalproject.s3.ap-southeast-1.amazonaws.com/image/attraction/4eb737da2c5b53141b1a7dab.jpg' },
  { name: '演藝娛樂', img: "https://tecky-finalproject.s3.ap-southeast-1.amazonaws.com/image/attraction/505070d7e4b0145d407ee00a.jpg" },
  { name: '戶外探索', img: "https://tecky-finalproject.s3.ap-southeast-1.amazonaws.com/image/attraction/4bb80cac1261d13a954be898.jpg" },
  { name: '朋友相聚', img: "https://tecky-finalproject.s3.ap-southeast-1.amazonaws.com/image/attraction/4d566e6258856dcb0c6c576d.jpg" },
  { name: '主題樂園', img: "https://tecky-finalproject.s3.ap-southeast-1.amazonaws.com/image/attraction/50bb1dd3e4b0e22561416ebd.jpg" }]

  const name = useSelector(
    (state: RootState) => state.auth.user?.name || state.auth.user?.username
  );

  useEffect(() => {
    try {
      let loadDistrict = async () => {
        const res = await fetch(getAPIServer() + `/${tab}`);
        const json = await res.json();

        if (tab === "attractions") {
          setDistricts(json);
        }
        if (tab === "facilities") {
          setFacilitie(json.rows);
        }
        if (tab === "malls") {
          setDistricts(json);
        }
      };
      loadDistrict();

    } catch (error) {

    }
  }, [tab]);


  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="tertiary">
          <IonTitle className="ion-text-center">探索香港</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>



        {bigTab === "district" ?
          <IonToolbar>
            <IonSegment value={tab} onIonChange={(e) => setTab(e.detail.value)}>
              <IonSegmentButton value="attractions" className="segment-btn">
                景點
              </IonSegmentButton>
              <IonSegmentButton value="facilities" className="segment-btn">
                公共娛樂設施
              </IonSegmentButton>
              <IonSegmentButton value="restaurants" className="segment-btn">
                餐廳
              </IonSegmentButton>

              <IonSegmentButton value="malls" className="segment-btn">
                商場
              </IonSegmentButton>
            </IonSegment>
          </IonToolbar> : null}

        {tab === "attractions" ?
          <IonToolbar>
            <IonSegment value={bigTab} onIonChange={(e) => setbigTab(e.detail.value)}>
              <IonSegmentButton value="district" className="segment-btn">
                根據地區分類
              </IonSegmentButton>
              <IonSegmentButton value="category" className="segment-btn">
                根據種類分類
              </IonSegmentButton>
            </IonSegment>
          </IonToolbar> : null}


        {bigTab === 'district' ? <div>
          {tab === "attractions" && <div className='board'><Attractions districts={districts} /></div>}
          {tab === "facilities" && <div className='board'><Facilities facilities={facilities} /></div>}
          {tab === "restaurants" && <Restaurants />}
          {tab === "malls" && <Malls districts={districts} />}</div> : null}

        {bigTab === 'category' ? <div>{categories.map(category =>
          <Link to={'/category/' + category.name} key={category.name}>
            <IonCard className='categoryCard' >
              <div className='title'>{category.name}</div>
              <img src={category.img} alt={category.name} />
            </IonCard></Link>)}</div>

          : null}

      </IonContent>

      <IonModal isOpen={showModal} cssClass="my-custom-class">
        <IonSearchbar></IonSearchbar>
        <img
          src="https://file.workeroom.com.hk/uploads/7341/1596789477/2012-08-%E8%A7%92%E5%8C%85-56x85-1-scaled.webp"
          alt="adv"
        ></img>
        <ButtonGroup aria-label="Basic example">
          <Button variant="warning" className="modal-button">
            搜尋
          </Button>
          <Button
            onClick={() => setShowModal(false)}
            variant="warning"
            className="modal-button"
          >
            關閉
          </Button>
        </ButtonGroup>
      </IonModal>
    </IonPage >
  );
};

export default Tab2;
