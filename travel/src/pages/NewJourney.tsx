import "./NewJourney.css";
import React, { useState, FormEvent, useEffect } from "react";
import {
  IonLabel,
  IonContent,
  IonHeader,
  IonPage,
  IonToolbar,
  IonButton,
  IonInput,
  IonSelect,
  IonSelectOption,
  IonItem,
  IonCheckbox,
  IonTitle,
  IonRow,
  IonCol,
  IonIcon,
  IonCard,
  IonCardContent,
  IonButtons,
  IonBackButton,
  IonGrid,
  IonModal,
  IonItemDivider,
  IonList,
  IonDatetime,
} from "@ionic/react";
import { useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { Link } from "react-router-dom";
import { arrowDown, pinOutline, trash } from "ionicons/icons";
import { getAPIServer, post } from "../helpers/api";
type PlanItem = {
  id: number;
  attractions_name: string;
  attractions_id: number;
  user_id: number;
  stay_time?: number;
};
type Route = {
  atraction_id: number;
  trafficDistance: number;
  trafficTime: number;
  stayTime: number;
  timetaken: number;
};

type Result = {
  atraction_id: number;
  attractions_name: string;
  trafficDistance: number;
  trafficTime: number;
  stayTime: number;
  timetaken: number;
};

export const NewJourney: React.FC = () => {
  const [planItems, setPlanItems] = useState<PlanItem[]>();
  const [row, setRow] = useState<Rows>({});
  const [hour, setHour] = useState<number>(8);
  const [min, setMin] = useState<number>(0);
  const [start, setStart] = useState<string>();
  const [results, setResults] = useState<Result[]>();
  const [showModal, setShowModal] = useState(false);
  const [plan_name, setplan_name] = useState<string>();
  const [selected_date, setselected_date] = useState<string>();

  const username = useSelector(
    (state: RootState) => state.auth.user?.name || state.auth.user?.username
  );
  const user_id = useSelector((state: RootState) => state.auth.user?.id);

  function convertToHour(min: number) {
    let hours = Math.floor(Math.abs(min) / 60);
    let mins = Math.abs(min) % 60;
    if (Math.abs(min) === 0) {
      return 0 + " 分鐘";
    }
    if (hours < 1) {
      return Math.abs(min) + " 分鐘";
    } else if (Math.abs(min) < 1) {
      return hours + " 小時";
    }
    return hours + "小時 " + mins + " 分鐘";
  }
  type attractionsArrayItem = { id: number; time: number };

  function submit(event: FormEvent) {
    event.preventDefault();
    let attractionsArray: attractionsArrayItem[] = [];
    for (let item in row) {
      if (
        row[item].checked === true &&
        (row[item].hour !== "undefined" || row[item].min !== "undefined")
      ) {
        attractionsArray.push({
          id: +item,
          time: ~~row[item].min + ~~row[item].hour * 60,
        });
      } else if (row[item].checked === true && row[item].hour === "undefined") {
        alert("invalid hour");
      } else if (row[item].checked === true && row[item].min === "undefined") {
        alert("invalid min");
      }
    }

    if (start) {
      // let index =  attractionsArray.indexOf(start)
      let start_id = planItems?.filter(
        (planitem) => planitem.attractions_name === start
      )[0].attractions_id;
      let removed_obj = attractionsArray.filter(
        (item) => item.id === start_id
      )[0];
      attractionsArray = attractionsArray.filter(
        (item) => item.id !== start_id
      );
      attractionsArray.unshift(removed_obj);
    }

    let submitForm = {
      maximumTime: ~~hour! * 60 + ~~min!,
      attractions: attractionsArray,
    };
    console.log(submitForm, "submitForm");
    let getRoute = async () => {
      const routes = await post("/loadRoutes", submitForm);
      let mergeResult = [];
      for (let route of routes) {
        // console.log(route)
        mergeResult.push({
          ...route,
          attractions_name: planItems?.filter(
            (planItems) => planItems.attractions_id === route.atraction_id
          )[0].attractions_name,
        });
      }
      console.log("mergeResult", mergeResult);
      setResults(mergeResult);
    };
    getRoute();
  }

  interface Rows {
    [attractions_id: number]: {
      min: string;
      hour: string;
      checked: boolean;
    };
  }

  useEffect(() => {
    if (username) {
      console.log(username);
      let loadPlanItems = async () => {
        const res = await fetch(getAPIServer() + `/getPlanItems/${user_id}`);
        const json = await res.json();
        setPlanItems(json);
      };
      loadPlanItems();
    } else {
      return;
    }
  }, [user_id, username]);

  async function remove(planitem: PlanItem) {
    await fetch(
      getAPIServer() + `/delplanItem/${user_id}/${planitem.attractions_id}`,
      {
        method: "DELETE",
      }
    );
  }

  // console.log("start", ~~start)

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/tab/journey" />
          </IonButtons>
          <IonTitle className="ion-text-center">行程</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <Link to="/suggestions">
          <IonButton expand='full' color="secondary">
            自選行程
          </IonButton>
        </Link>

        {planItems ? (
          <div>
            {planItems.map((planitem) => (
              <div className="plancard" key={planitem.id}>
                <IonItem>
                  <IonLabel className="ion-text-wrap">
                    <IonIcon size="small" icon={pinOutline}></IonIcon>
                    {planitem.attractions_name}
                  </IonLabel>
                  <IonCheckbox
                    slot="start"
                    checked={row[planitem.attractions_id]?.checked}
                    onIonChange={(e) => {
                      const newRow = { ...row };
                      newRow[planitem.attractions_id] = {
                        ...newRow[planitem.attractions_id],
                        checked: e.detail.checked,
                      };
                      setRow(newRow);
                    }}
                  />
                </IonItem>
                <IonRow>
                  <IonCol>
                    <IonItem>
                      <IonInput
                        className="hourInput ion-padding"
                        min="0"
                        type="number"
                        value={row[planitem.attractions_id]?.hour}
                        onIonChange={(e) => {
                          const newRow = { ...row };
                          newRow[planitem.attractions_id] = {
                            ...newRow[planitem.attractions_id],
                            hour: e.detail.value + "",
                          };
                          setRow(newRow);
                        }}
                      ></IonInput>
                      <IonLabel className="ion-text-wrap">小時</IonLabel>
                    </IonItem>
                  </IonCol>
                  <IonCol>
                    <IonItem>
                      <IonInput
                        className="minInput ion-padding"
                        min="0"
                        type="number"
                        value={row[planitem.attractions_id]?.min}
                        onIonChange={(e) => {
                          const newRow = { ...row };
                          newRow[planitem.attractions_id] = {
                            ...newRow[planitem.attractions_id],
                            min: e.detail.value + "",
                          };
                          setRow(newRow);
                        }}
                      ></IonInput>
                      <IonLabel className="ion-text-wrap">分鐘</IonLabel>
                    </IonItem>
                  </IonCol>
                  <IonCol size="auto">
                    <IonButton
                      color="danger"
                      size="small"
                      onClick={async () => {
                        remove(planitem);
                        setPlanItems(
                          planItems.filter((item) => item !== planitem)
                        );
                      }}
                    >
                      <IonIcon icon={trash}></IonIcon>
                    </IonButton>
                  </IonCol>
                </IonRow>
              </div>
            ))}
            <IonItem>
              <IonLabel className="ion-text-wrap">請調節最長行程時間</IonLabel>
              <IonInput
                min="0"
                value={hour}
                placeholder="hour"
                onIonChange={(e) => setHour(+e.detail.value!)}
              ></IonInput>
              <IonLabel className="ion-text-wrap">小時</IonLabel>
              <IonInput
                min="0"
                value={min}
                placeholder="min"
                onIonChange={(e) => setMin(+e.detail.value!)}
              ></IonInput>
              <IonLabel className="ion-text-wrap">分鐘</IonLabel>
            </IonItem>
            <IonItem>
              <IonLabel className="ion-text-wrap">選擇起點</IonLabel>
              <IonSelect
                interface="action-sheet"
                placeholder="Select One"
                onIonChange={(e) => setStart(e.detail.value)}
                value={start}
              >
                {planItems
                  .filter(
                    (value) =>
                      Object.keys(row).includes(value.attractions_id + "") &&
                      row[value.attractions_id].checked === true
                  )
                  .map((checked) => (
                    <IonSelectOption
                      value={checked.attractions_name}
                      key={checked.attractions_id}
                    >
                      {checked.attractions_name}
                    </IonSelectOption>
                  ))}
              </IonSelect>
            </IonItem>

            <IonButton onClick={submit}>建立行程</IonButton>
          </div>
        ) : null}

        {results ? (
          <IonCard>
            <IonCardContent>
              {results
                ? results.map((result) => (
                  <div key={result.atraction_id}>
                    <IonItem>
                      <IonGrid>
                        <IonRow
                          className="ion-text-wrap attractions_name"
                          key={result.atraction_id}
                        >
                          {result.attractions_name}
                        </IonRow>
                        {result.trafficTime !== 0 ? (
                          <IonRow className="ion-text-wrap">
                            車程: {convertToHour(result.trafficTime)}{" "}
                          </IonRow>
                        ) : (
                          <IonRow>起點</IonRow>
                        )}
                        <IonRow className="ion-text-wrap">
                          逗留時間: {convertToHour(result.stayTime)}
                        </IonRow>
                        {result.timetaken !==
                          results[results.length - 1].timetaken ? (
                          <div className="arrow">
                            <IonIcon size="large" icon={arrowDown}></IonIcon>{" "}
                          </div>
                        ) : null}
                      </IonGrid>
                    </IonItem>
                  </div>
                ))
                : null}
              <IonItem>
                {" "}
                <IonGrid>
                  <IonRow className="totaltime">
                    {" "}
                    總行程時間{" "}
                    {convertToHour(results[results.length - 1].timetaken)}
                  </IonRow>{" "}
                </IonGrid>
              </IonItem>

              <IonButton onClick={() => setShowModal(true)}>
                儲存到我的行程
              </IonButton>
            </IonCardContent>
          </IonCard>
        ) : null}
      </IonContent>

      <IonModal isOpen={showModal} cssClass="my-custom-class">
        <IonPage>
          <IonHeader>
            <IonToolbar>
              <IonTitle>儲存到我的行程</IonTitle>
            </IonToolbar>
          </IonHeader>
          <IonContent>
            <IonList>
              <IonItemDivider>行程名稱</IonItemDivider>
              <IonItem>
                <IonInput
                  value={plan_name}
                  placeholder="輸入行程名稱"
                  onIonChange={(e) => setplan_name(e.detail.value!)}
                ></IonInput>
              </IonItem>

              <IonItemDivider>行程日期</IonItemDivider>
              <IonItem>
                <IonLabel>出發日期</IonLabel>
                <IonDatetime
                  placeholder="輸入出發日期"
                  displayFormat="DD-MM-YYYY"
                  value={selected_date}
                  onIonChange={(e) => setselected_date(e.detail.value!)}
                ></IonDatetime>
              </IonItem>

              <IonItemDivider>行程概覽</IonItemDivider>
              {results
                ? results.map((result) => (
                  <div key={result.atraction_id}>
                    <IonItem>
                      <IonGrid>
                        <IonRow
                          className="ion-text-wrap attractions_name"
                          key={result.atraction_id}
                        >
                          {result.attractions_name}
                        </IonRow>
                        {result.trafficTime !== 0 ? (
                          <IonRow className="ion-text-wrap">
                            車程: {convertToHour(result.trafficTime)}{" "}
                          </IonRow>
                        ) : (
                          <IonRow>起點</IonRow>
                        )}
                        <IonRow className="ion-text-wrap">
                          逗留時間: {convertToHour(result.stayTime)}
                        </IonRow>
                        {result.timetaken !==
                          results[results.length - 1].timetaken ? (
                          <div className="arrow">
                            <IonIcon size="large" icon={arrowDown}></IonIcon>{" "}
                          </div>
                        ) : null}
                      </IonGrid>
                    </IonItem>
                  </div>
                ))
                : null}
              {results ? (
                <IonItem>
                  {" "}
                  <IonGrid>
                    <IonRow className="totaltime">
                      {" "}
                      總行程時間{" "}
                      {convertToHour(results![results!.length - 1].timetaken)}
                    </IonRow>{" "}
                  </IonGrid>
                </IonItem>
              ) : null}
            </IonList>
          </IonContent>
          <IonButton
            color="secondary"
            onClick={async () => {
              const json_plan = JSON.stringify(results);
              const formObj = {
                plan_name,
                selected_date,
                json_plan,
                user_id,
              };
              console.log(formObj);
              await fetch(getAPIServer() + `/submitPlan/${user_id}`, {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify(formObj),
              });
            }}
          >
            提交
          </IonButton>
          <IonButton onClick={() => setShowModal(false)}>關閉 </IonButton>
        </IonPage>
      </IonModal>
    </IonPage>
  );
};
