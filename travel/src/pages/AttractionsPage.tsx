import {
  IonBackButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import AttractionsCard from "../components/AttractionsCard";
import "./Tab2.css";
import React from "react";
import { useParams } from "react-router-dom";
import { useAttractions } from "../hooks/use-api";
import ErrorMessage from "../components/ErrorMessage";

export default function AttractionsPage() {
  const { district } = useParams<{ district: string }>();

  const attractions = useAttractions(district);

  const attraction = "error" in attractions ? undefined : attractions[0];

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="tertiary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/tab/attractions" />
          </IonButtons>
          <IonTitle className="ion-text-center">{district}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="">
        {"error" in attractions ? (
          <ErrorMessage error={attractions.error} />
        ) : (
          attractions.map((attraction) => (
            <AttractionsCard
              key={attraction.id}
              id={attraction.id}
              name={attraction.name}
              categories={attraction.categories}
              address={attraction.address}
              phone_number={attraction.phone_number}
              website={attraction.website}
              latitude={attraction.latitude}
              longitude={attraction.longitude}
              sub_district_id={attraction.sub_district_id}
              img_url={attraction.img_url}
              my_category={attraction.my_category}
              description={attraction.description}
              district={district}
            />
          ))
        )}
      </IonContent>
    </IonPage>
  );
}
