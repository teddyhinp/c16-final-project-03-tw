import React, { useState, FormEvent, useEffect } from "react";
import {
  IonGrid,
  IonButton,
  IonSelect,
  IonLabel,
  IonItem,
  IonSelectOption,
  IonCol,
  IonContent,
  IonPage,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonToast,
} from "@ionic/react";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { Form } from "react-bootstrap";
import HotelCard from "../components/HotelCard";

import { setTimeout } from "timers";
import {
  Map as LeafletMap,
} from "leaflet";
import shuffle from "../helpers/shuffle";
import "../App.css";
import { getAPIServer } from "../helpers/api";
type district = { id: number; name: string };
type subDistrict = { id: number; name: string };
type category = { categories: string };
type hotel = {
  id: number;
  name: string;
  categories: string;
  address: string;
  latitude: number;
  longitude: number;
  sub_district_id: number;
};

export const HotelsPage: React.FC = () => {
  const [districts, setDistricts] = useState<district[]>([]);
  const [selectedDistrict, setSelectedDistrict] = useState<district>({} as any);
  const [subDistricts, setSubDistricts] = useState<subDistrict[]>([]);
  const [selectedSubDistrict, setSelectedSubDistrict] = useState<string>("");

  const categories = ["賓館", "酒店", "度假營", "度假屋"];
  const [showToast1, setShowToast1] = useState(false);
  const [selectedCategories, setSelectedCategories] = useState<category>(
    {} as any
  );
  const [hotelList, sethotelList] = useState<hotel[]>([]);

  const center: [number, number] = [0, 0];
  if (hotelList.length === 0) {
    center[0] = 22.3174732;
    center[1] = 114.1709714;
  } else {
    hotelList.forEach((restaurant) => {
      center[0] += restaurant.latitude;
      center[1] += restaurant.longitude;
    });
    center[0] /= hotelList.length;
    center[1] /= hotelList.length;
  }

  const [renderMapSequence, setRenderMapSequence] = useState([
    true,
    false,
    true,
  ]);
  const [map, setMap] = useState<LeafletMap>();



  function success(pos: any) {
    var crd = pos.coords;
    let { latitude, longitude } = pos.coords;
    // setCenter([latitude, longitude]);
    console.log("Your current position is:");
    console.log(`More or less ${crd.accuracy} meters.`);
  }

  useEffect(() => {
    if (renderMapSequence.length > 1) {
      setTimeout(() => {
        setRenderMapSequence(renderMapSequence.slice(1));
      }, 333);
    }
  }, [center, renderMapSequence]);

  useEffect(() => {
    if (map) {
      map.setView(center);
    }
  }, [center, map]);

  useEffect(() => {
    let districts = async () => {
      const res = await fetch(getAPIServer() + `/district`);
      const json = await res.json();

      setDistricts(json);
    };
    districts();
  }, []);

  useEffect(() => {
    if (selectedDistrict.id) {
      let subDistricts = async () => {
        const res = await fetch(
          getAPIServer() + `/district/${selectedDistrict.id}`
        );
        const json = await res.json();
        console.log("subDistrict", json);
        setSubDistricts(json);
      };
      subDistricts();
    } else {
      return;
    }
  }, [selectedDistrict]);
  console.log(selectedDistrict);

  useEffect(() => {
    if (map) {
      map.setView(center);
    }
  }, [center, map]);

  function loadHotel() {
    let hotelLists = async () => {
      let selectedSubDistrict_id: number = subDistricts.filter(
        (subDistrict) => subDistrict.name === selectedSubDistrict
      )[0].id;

      const res = await fetch(
        getAPIServer() + `/loadHotelList/${selectedSubDistrict_id}/${selectedCategories}`
      );
      const json = await res.json();
      if (json.length === 0) {
        setShowToast1(true)
      }
      console.log("hotelList", json);

      sethotelList(shuffle(json));
    };
    hotelLists();
  }

  if (!renderMapSequence[0]) {
    return (
      <IonContent>
        <IonTitle className="Ion-text-center"> loading...</IonTitle>
      </IonContent>
    );
  }

  function submit(event: FormEvent) {
    event.preventDefault();
    console.log("submit form data:", {
      selectedCategories,
      selectedDistrict,
      selectedSubDistrict,
    });
  }

  if (!renderMapSequence[0]) {
    return (
      <IonContent>
        <IonTitle className="Ion-text-center"> loading...</IonTitle>
      </IonContent>
    );
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="tertiary">
          <IonTitle className="ion-text-center">住宿</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonGrid>
          <MapContainer
            // center={position}
            zoom={15}
            scrollWheelZoom={false}
            whenCreated={(map) => {
              console.log(map);
              let win = window as any;
              win.map = map;
              setMap(map);
            }}
          >
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {hotelList.map((item) => (
              <Marker position={[item.latitude, item.longitude]} key={item.id}>
                <Popup>{item.name}</Popup>
              </Marker>
            ))}
          </MapContainer>

          {/* <IonButton
              type="submit"
              className="allowGetNavigation"
              color="warning"
              onClick={getLocation}
            >
              使用當前位置
            </IonButton> */}

          <Form onSubmit={submit}>
            <IonItem>
              <IonLabel>選擇地區</IonLabel>
              <IonSelect
                className="district"
                value={selectedDistrict}
                okText="OK"
                cancelText="Cancel"
                onIonChange={(e) => setSelectedDistrict(e.detail.value!)}
              >
                {districts.map((district) => (
                  <IonSelectOption key={district.id} value={district}>
                    {district.name}
                  </IonSelectOption>
                ))}
              </IonSelect>
            </IonItem>

            <IonItem>
              <IonLabel>選擇分區</IonLabel>
              <IonSelect
                className="subDistrict"
                value={selectedSubDistrict}
                okText="OK"
                cancelText="Cancel"
                onIonChange={(e) => setSelectedSubDistrict(e.detail.value!)}
              >
                {subDistricts.map((subDistrict) => (
                  <IonSelectOption
                    key={subDistrict.id}
                    value={subDistrict.name}
                  >
                    {subDistrict.name}
                  </IonSelectOption>
                ))}
              </IonSelect>
            </IonItem>

            <IonItem>
              <IonLabel>選擇住宿類型</IonLabel>
              <IonSelect
                className="category"
                value={selectedCategories}
                okText="OK"
                cancelText="Cancel"
                onIonChange={(e) => setSelectedCategories(e.detail.value!)}
              >
                {categories.map((category, i) => (
                  <IonSelectOption key={i} value={category}>
                    {category}
                  </IonSelectOption>
                ))}
              </IonSelect>
            </IonItem>

            <IonButton
              type="submit"
              className="allowGetNavigation"
              color="warning"
              onClick={loadHotel}
            >
              搜尋
            </IonButton>

            {hotelList
              ? hotelList.map((hotel) => (
                <IonCol key={hotel.id}>
                  <HotelCard
                    id={hotel.id}
                    name={hotel.name}
                    categories={hotel.categories}
                    address={hotel.address}
                    latitude={hotel.latitude}
                    longitude={hotel.longitude}
                    sub_district_id={hotel.sub_district_id}
                  />
                </IonCol>
              ))
              : null}
          </Form>
        </IonGrid>
      </IonContent>


      <IonToast
        isOpen={showToast1}
        onDidDismiss={() => setShowToast1(false)}
        message="沒有相關搜尋結果."
        duration={2000}
      />



    </IonPage>
  );
};
export default HotelsPage;
