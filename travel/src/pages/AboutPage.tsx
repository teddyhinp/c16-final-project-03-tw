import React from "react";
import "./TermsPage.css";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonToolbar,
  IonImg,
  IonTitle,
  IonButtons,
  IonBackButton,
} from "@ionic/react";

export const AboutPage: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/tab/settings" />
          </IonButtons>

          <IonTitle>關於旅行星球</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonImg
          className="ion-img-center"
          src="/logo.gif"
          id="img1"
          hidden
        ></IonImg>
        <div
          style={{
            backgroundImage: "url(/logo.gif)",
            backgroundColor: "var(--ion-color-primary)",
            height: "40%",
            backgroundSize: "contain",
            width: "100%",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
          }}
        ></div>

        <div className="ion-padding">
          <p>這是第16屆科啟學院學生的作品，希望透過 Project 形式鞏固所學，並運用 React 編寫一個以旅行為題材的作品，提供香港旅遊資訊及最佳行程編排。</p>
          <p></p>
        </div>
      </IonContent>
    </IonPage>
  );
};
