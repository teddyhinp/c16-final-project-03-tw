import {
  IonBackButton,
  IonButtons,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonPage,
  IonRow,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import "./Tab2.css";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import FacilitiesCard from "../components/FacilitiesCard";
import { getAPIServer } from "../helpers/api";

export default function FacilitiesPage() {
  type Attraction = {
    id: number;
    name: string;
    categories: string;
    address: string;
    phone_number: number;
    website: string;
    latitude: number;
    longitude: number;
    sub_district_id: number;
  };

  const { facility } = useParams<{ facility: string }>();
  const [attractions, setAttractions] = useState<Attraction[]>(
    [] as Attraction[]
  );

  useEffect(() => {
    let loadAttraction = async () => {
      const res = await fetch(getAPIServer() + `/facilities/${facility}`);
      const json = await res.json();
      // console.log('json', json)

      setAttractions((json));
    };
    loadAttraction();
  }, [facility]);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="tertiary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/tab/attractions" />
          </IonButtons>
          <IonTitle className="ion-text-center">{facility}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            {attractions.map((location) => (
              <IonCol key={location.id}>
                <FacilitiesCard
                  id={location.id}
                  name={location.name}
                  categories={location.categories}
                  address={location.address}
                  phone_number={location.phone_number}
                  website={location.website}
                  latitude={location.latitude}
                  longitude={location.longitude}
                  sub_district_id={location.sub_district_id}
                />
              </IonCol>
            ))}
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
}
