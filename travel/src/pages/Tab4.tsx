import './Tab4.css';
import { IonCard, IonCol, IonContent, IonHeader, IonIcon, IonGrid, IonPage, IonRow, IonTitle, IonToolbar } from '@ionic/react';
import { useSelector } from 'react-redux';
import { RootState } from '../redux/state';
import { airplaneOutline, arrowUndoOutline } from "ionicons/icons";
import { Link } from 'react-router-dom';

const Tab4: React.FC = () => {
  const name = useSelector(
    (state: RootState) => state.auth.user?.name || state.auth.user?.username,
  )

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color='tertiary'>
          <IonTitle className='ion-text-center'>行程</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent className='board'>
        {name ? <IonGrid><IonRow>
          <IonCol>
            <Link to='/newJourney'>
              <IonCard className='card'>
                <IonIcon icon={airplaneOutline} />
                新的旅程
              </IonCard>
            </Link>
          </IonCol>
          <IonCol>
            <Link to='/resumeJourney'>
              <IonCard className='card'>
                <IonIcon icon={arrowUndoOutline} />
                找回旅程記錄
              </IonCard>
            </Link>
          </IonCol>
        </IonRow></IonGrid> : <IonTitle size='small' className='ion-text-center'><h1>請先登入使用行程功能</h1></IonTitle>}

      </IonContent>
    </IonPage >
  );
};

export default Tab4;
