import {
  IonBackButton,
  IonButtons,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonPage,
  IonRow,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import AttractionsCard from "../components/AttractionsCard";
import "./Tab2.css";
import { Link } from "react-router-dom";
import React from "react";
import { useParams } from "react-router-dom";

import { Attraction, useGet } from "../hooks/use-api";
import ErrorMessage from "../components/ErrorMessage";

export default function CategoryPage() {
  const { category } = useParams<{ category: string }>();

  const [attractions] = useGet<Attraction[]>(
    "/attractions/category/" + category,
    []
  );

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="tertiary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/tab/attractions" />
          </IonButtons>
          <IonTitle className="ion-text-center">{category}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            {"error" in attractions ? (
              <ErrorMessage error={attractions.error} />
            ) : (
              attractions!.map((attraction) => (
                <Link
                  to={"/attractions/" + category + "/" + attraction.id}
                  key={attraction.id}
                >
                  <IonCol>
                    <AttractionsCard
                      id={attraction.id}
                      name={attraction.name}
                      categories={attraction.categories}
                      address={attraction.address}
                      phone_number={attraction.phone_number}
                      website={attraction.website}
                      latitude={attraction.latitude}
                      longitude={attraction.longitude}
                      sub_district_id={attraction.sub_district_id}
                      img_url={attraction.img_url}
                      my_category={attraction.my_category}
                    />
                  </IonCol>
                </Link>
              ))
            )}
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
}
