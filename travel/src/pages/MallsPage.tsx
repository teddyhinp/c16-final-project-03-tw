import {
  IonBackButton,
  IonButtons,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonModal,
  IonPage,
  IonRow,
  IonSearchbar,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import MallsCard from "../components/MallsCard";
import "./Tab2.css";
import { Link } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useMalls } from "../hooks/use-api";
import ErrorMessage from "../components/ErrorMessage";

export default function MallsPage() {
  const { district } = useParams<{ district: string }>();

  const malls = useMalls(district);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="tertiary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/tab/attractions" />
          </IonButtons>
          <IonTitle className="ion-text-center">{district}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            {"error" in malls ? (
              <ErrorMessage error={malls.error} />
            ) : (
              malls!.map((mall) => (
                <Link to={"/malls/" + district + "/" + mall.id} key={mall.id}>
                  <IonCol>
                    <MallsCard
                      id={mall.id}
                      name={mall.name}
                      address={mall.address}
                      latitude={mall.latitude}
                      longitude={mall.longitude}
                      sub_district_id={mall.sub_district_id}
                      img_url={mall.img_url}
                    />
                  </IonCol>
                </Link>
              ))
            )}
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
}
