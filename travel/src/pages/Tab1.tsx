import "./Tab1.css";
import React, { useEffect, useRef } from "react";
import {
  IonContent,
  IonHeader,
  IonLoading,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";

import { useSelector } from "react-redux";
import { RootState } from "../redux/state";
import Iframe from "react-iframe";
import { useState } from "react";
import "./Tab1.css";

export const Tab1: React.FC = () => {
  const user_name = useSelector(
    (state: RootState) => state.auth.user?.name || state.auth.user?.username
  );
  const user_id = useSelector((state: RootState) => state.auth.user?.id);

  let [width, setWidth] = useState("");
  const elementRef = useRef(null);

  const [showLoading, setShowLoading] = useState(true);

  setTimeout(() => {
    setShowLoading(false);
  }, 1000);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="tertiary">
          <IonTitle className="ion-text-center title">
            <div>熱門旅遊景點</div>
          </IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent ref={elementRef}>
        <Iframe
          className="data_iframe"
          height="2000"
          url="https://datastudio.google.com/embed/reporting/18e545a9-cc0d-4b64-9875-73cd48f0aa5c/page/EV3dC"
        ></Iframe>
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
