import "./RegisterPage.css";
import React, { useState, FormEvent } from "react";
import {
  IonLabel,
  IonContent,
  IonHeader,
  IonPage,
  IonToolbar,
  IonButton,
  IonInput,
  IonSelect,
  IonSelectOption,
  IonBackButton,
  IonItem,
  IonCheckbox,
  IonImg,
  IonButtons
} from "@ionic/react";
import { Form } from "react-bootstrap";
import { useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { getAPIServer } from "../helpers/api";

export const EditUserProfile: React.FC = () => {
  const [password, setPassword] = useState<string>();
  const [passwordConfirm, setPasswordConfirm] = useState<string>();
  const [preference1, setPreference1] = useState<string>();
  const [preference2, setPreference2] = useState<string>();
  const [checked, setChecked] = useState(false);

  function submit(event: FormEvent) {
    event.preventDefault();
    console.log("submit form data:", {
      password,
      passwordConfirm,
      preference1,
      preference2,
      checked,
    });
  }

  const user_id = useSelector(
    (state: RootState) => state.auth.user?.id
  )
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/tab/settings" />
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonHeader>
        <IonToolbar color="primary">
          <IonImg className="ion-img-center" src="/logo.gif" id="img1"></IonImg>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <Form onSubmit={submit}>

          <IonItem>
            <IonInput
              className="passwordInput ion-padding-top"
              type="password"
              value={password}
              placeholder="更改您的密碼"
              onIonChange={(e) => setPassword(e.detail.value!)}
            ></IonInput>
          </IonItem>

          <IonItem>
            <IonInput
              className="passwordInput ion-padding-top"
              type="password"
              value={passwordConfirm}
              placeholder="重新輸入密碼"
              onIonChange={(e) => setPasswordConfirm(e.detail.value!)}
            ></IonInput>
          </IonItem>

          <IonItem>
            <IonLabel>更改景點偏好1</IonLabel>
            <IonSelect
              className="preference1 ion-padding"
              value={preference1}
              placeholder="第一偏好"
              okText="OK"
              cancelText="Cancel"
              onIonChange={(e) => setPreference1(e.detail.value)}
            >
              <IonSelectOption value="文化藝術">文化藝術</IonSelectOption>
              <IonSelectOption value="演藝娛樂">演藝娛樂</IonSelectOption>
              <IonSelectOption value="戶外探索">戶外探索</IonSelectOption>
              <IonSelectOption value="朋友相聚">朋友相聚</IonSelectOption>
              <IonSelectOption value="主題樂園">主題樂園</IonSelectOption>
            </IonSelect>
          </IonItem>

          <IonItem>
            <IonLabel>更改景點偏好2</IonLabel>
            <IonSelect
              className="preference2 ion-padding"
              value={preference2}
              placeholder="第二偏好"
              okText="OK"
              cancelText="Cancel"
              onIonChange={(e) => setPreference2(e.detail.value)}
            >
              <IonSelectOption value="文化藝術">文化藝術</IonSelectOption>
              <IonSelectOption value="演藝娛樂">演藝娛樂</IonSelectOption>
              <IonSelectOption value="戶外探索">戶外探索</IonSelectOption>
              <IonSelectOption value="朋友相聚">朋友相聚</IonSelectOption>
              <IonSelectOption value="主題樂園">主題樂園</IonSelectOption>
            </IonSelect>
          </IonItem>

          <IonItem>
            <IonCheckbox
              checked={checked}
              onIonChange={(e) => setChecked(e.detail.checked)}
            />
            <IonLabel className="sign ion-padding">
              我接受所有條款和條件
            </IonLabel>
          </IonItem>

          <IonButton
            type="submit"
            className="button"
            color="warning"
            onClick={async () => {

              const formObj = {
                id: user_id,
                password,
                preference1,
                preference2,
              };
              await fetch(getAPIServer() + `/editProfile`, {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify(formObj),
              })

              setPassword('')
              setPasswordConfirm('')
              setPreference1('')
              setPreference2('')
              setChecked(false)
            }}
          >
            更改
          </IonButton>

        </Form>
      </IonContent>
    </IonPage>
  );
};
