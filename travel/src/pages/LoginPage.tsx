import "./LoginPage.css";
import React, { useEffect, useState, FormEvent } from "react";
import { Route, Redirect, useHistory, Link } from "react-router-dom";
import {
  IonContent,
  IonHeader,
  IonPage,
  IonToolbar,
  IonButtons,
  IonBackButton,
  IonTitle,
  IonInput,
  IonButton,
  IonImg,
  IonToast,
} from "@ionic/react";
import { } from "ionicons/icons";
import { Form } from "react-bootstrap";
import {
  loginWithPasswordThunk,
  loginWithFacebookThunk,
} from "../redux/auth/thunk";
import { loginFailed } from "../redux/auth/action";
import { RootState } from "../redux/state";
import { RootDispatch } from "../redux/dispatch";
import { useDispatch, useSelector } from "react-redux";
import { LoginInput } from "shared";

// import Setting from "../components/Setting";
import ReactFacebookLogin, {
  ReactFacebookLoginInfo,
} from "react-facebook-login";
import dotenv from "dotenv";
dotenv.config();

export const LoginPage: React.FC = () => {
  const dispatch = useDispatch();
  const isLoggedIn = useSelector((state: RootState) => state.auth.isLoggedIn);
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  // const [showLoading, setShowLoading] = useState(true);
  const history = useHistory();

  function submit(event: FormEvent) {
    event.preventDefault();
    dispatch(loginWithPasswordThunk(username, password, history));
  }
  const fBCallback = (
    userInfo: ReactFacebookLoginInfo & { accessToken: string }
  ) => {
    if (userInfo.accessToken) {
      dispatch(loginWithFacebookThunk(userInfo.accessToken));
    }
    return null;
  };
  const fBOnCLick = () => {
    return null;
  };
  console.log("isLoggedIn: ", isLoggedIn);

  let { REACT_APP_FACEBOOK_APP_ID } = process.env;
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/tab/settings" />
          </IonButtons>

          <IonTitle>登入</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonImg
          className="ion-img-center"
          src="/logo.gif"
          id="img1"
          hidden
        ></IonImg>
        <div
          style={{
            backgroundImage: "url(/logo.gif)",
            backgroundColor: "var(--ion-color-primary)",
            height: "40%",
            backgroundSize: "contain",
            width: "100%",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
          }}
        ></div>

        {!isLoggedIn && (
          <Form onSubmit={submit}>
            <div id="loginInputForm">
              <IonInput
                className="usernameInput ion-padding"
                value={username}
                placeholder="輸入用戶名"
                onIonChange={(e) => setUsername(e.detail.value!)}
              ></IonInput>
              <IonInput
                type="password"
                className="passwordInput ion-padding"
                value={password}
                placeholder="輸入密碼"
                onIonChange={(e) => setPassword(e.detail.value!)}
              ></IonInput>
              <IonButton
                className="pwLoginButton"
                color="warning"
                type="submit"
                onClick={() => {
                  const formObj: LoginInput = {
                    username: username,
                    password: password,
                  };
                }}
              >
                登入
              </IonButton>
            </div>
            <div id="termsAndConditions">
              <h3>一起讓旅行星球變得更好</h3>
              <Link to="/about">
                <IonButton expand="full" color="tertiary" id="aboutUs">
                  關於旅行星球
                </IonButton>
              </Link>

              <Link to="/terms">
                <IonButton expand="full" color="tertiary" id="terms">
                  使用者條款
                </IonButton>
              </Link>

              <Link to="/privacy">
                <IonButton expand="full" color="tertiary" id="privacy">
                  私隱政策
                </IonButton>
              </Link>


            </div>
            {/* 
            <h5>或者，你也可以</h5> */}
            {/* 
            <div id="oAuthButton">
              <div className="fb-button">
              {!REACT_APP_FACEBOOK_APP_ID ? 
                <code>missing REACT_APP_FACEBOOK_APP_ID in env</code>:
                <ReactFacebookLogin
                  appId={process.env.REACT_APP_FACEBOOK_APP_ID || ""}
                  autoLoad={false}
                  fields="name,email,picture"
                  onClick={fBOnCLick}
                  callback={fBCallback}
                />}
              </div>
            </div> */}
          </Form>
        )}
      </IonContent>
    </IonPage>
  );
};
