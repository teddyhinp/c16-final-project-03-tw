import { IonBackButton, IonButton, IonButtons, IonContent, IonGrid, IonHeader, IonIcon, IonItem, IonItemDivider, IonLabel, IonList, IonPage, IonRow, IonTitle, IonToolbar } from '@ionic/react';
import { useSelector } from 'react-redux';
import { RootState } from '../redux/state';
import './Tab4.css';
import { arrowDown, calendarOutline, pinOutline, trash } from "ionicons/icons";
import { useState, useEffect } from 'react';
import { getAPIServer } from '../helpers/api';


type Result = {
    "id": number,
    "plan_name": string,
    "selected_date": string,
    "json_plan": Plan[]
}

type Plan = {
    "atraction_id": number,
    "attractions_name": string,
    "trafficDistance": number,
    "trafficTime": number,
    "stayTime": number,
    "timetaken": number
}

function convertToHour(min: number) {
    let hours = Math.floor(min / 60)
    let mins = min % 60
    if (min === 0) {
        return (0 + ' 分鐘')
    }
    if (hours < 1) {
        return (mins + ' 分鐘')
    } else if (min < 1) {
        return (hours + ' 小時')
    }
    return (hours + '小時 ' + mins + ' 分鐘')
}



const ResumeJourney: React.FC = () => {
    const user_id = useSelector(
        (state: RootState) => state.auth.user?.id
    )

    const [plans, setPlans] = useState<Result[]>();


    useEffect(() => {
        let loadPlanItems = async () => {
            const res = await fetch(getAPIServer() + `/resumePlan/${user_id}`);
            const json = await res.json();
            for (var i = 0; i < json.length; i++) {
                let temp = []
                var counter = json[i].json_plan;
                temp = JSON.parse(counter);
                json[i].json_plan = temp
            }
            setPlans(json)
        };
        loadPlanItems();
    }, [user_id]);

    async function remove(plan_id: number) {
        console.log(plan_id, 'plan_id')
        await fetch(getAPIServer() + `/resumePlan/delplan/${plan_id}`, {
            method: 'DELETE',
        })

    }

    console.log(plans)
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar color='tertiary'>
                    <IonButtons slot="start">
                        <IonBackButton defaultHref="/tab/settings" />
                    </IonButtons>
                    <IonTitle className='ion-text-center'>找回您的行程</IonTitle>

                </IonToolbar>
            </IonHeader>
            <IonContent>
                {plans ? plans!.map(plan =>
                    <IonList key={plan.id}>
                        <IonItemDivider>行程名稱</IonItemDivider>
                        <IonItem>
                            <IonRow>{plan.plan_name}</IonRow>
                            <IonButton slot='end' color='danger' size='small' onClick={
                                async () => {
                                    remove(plan.id)
                                    setPlans(plans.filter(item => item !== plan))
                                }
                            }  >
                                <IonIcon icon={trash}></IonIcon>
                            </IonButton>
                        </IonItem>

                        <IonItemDivider>行程日期</IonItemDivider>
                        <IonItem>
                            <IonLabel>出發日期</IonLabel>
                            <IonRow>
                                <div>
                                    <IonIcon size="small" icon={calendarOutline} ></IonIcon></div>
                                <div className='date'>
                                    {new Date(plan.selected_date).getDate()}-
                                    {new Date(plan.selected_date).getMonth() + 1}-
                                    {new Date(plan.selected_date).getFullYear()}
                                </div>
                            </IonRow>
                        </IonItem>
                        <IonItemDivider>行程概覽</IonItemDivider>

                        {plan.json_plan ? plan.json_plan!.map(planItem =>
                            <IonItem key={plan.id + planItem.attractions_name} >
                                <div className='planDetail'>
                                    <IonGrid>
                                        <IonRow className='ion-text-wrap attractions_name'>
                                            <IonIcon size="small" icon={pinOutline} ></IonIcon>
                                            {planItem.attractions_name}</IonRow>
                                        {planItem.trafficTime !== 0 ? <IonRow className='ion-text-wrap'>車程: {convertToHour(planItem.trafficTime)} </IonRow> : <IonRow>起點</IonRow>}
                                        <IonRow className='ion-text-wrap'>逗留時間: {convertToHour(planItem.stayTime)}</IonRow>
                                        {planItem.timetaken !== plan.json_plan![plan.json_plan.length - 1].timetaken ? <div className='arrow'><IonIcon size="large" icon={arrowDown} ></IonIcon> </div> : null}
                                    </IonGrid>
                                </div>
                            </IonItem>
                        ) : null}
                        {plan.json_plan ? <IonItem>  <IonGrid>
                            <IonRow className='totaltime'> 總行程時間 {convertToHour(plan.json_plan![plan.json_plan!.length - 1].timetaken)}</IonRow>  </IonGrid></IonItem> : null}
                    </IonList>
                ) : null}

            </IonContent>




        </IonPage >
    );
};

export default ResumeJourney;
