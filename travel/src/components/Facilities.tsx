import { IonCol, IonGrid, IonRow } from "@ionic/react"
import { Link } from "react-router-dom"
import FacilitiesTypeCard from "./FacilitiesTypeCard"

type facility = {
    categories: string,
    // id: number
}

interface IFacilitiesProp {
    facilities: facility[]
}


const Facilities: React.FC<IFacilitiesProp> = (props) => {
    return (
        <IonGrid>
            <IonRow>
                {props.facilities.map(facility =>
                    <Link to={"/facilities/" + facility.categories} key={facility.categories}>
                        <IonCol > <FacilitiesTypeCard categories={facility.categories} />
                        </IonCol>
                    </Link>
                )}
            </IonRow>
        </IonGrid>
    )
}

export default Facilities