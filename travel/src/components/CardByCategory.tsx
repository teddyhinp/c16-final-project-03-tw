import "../App.css";
import React, { useEffect, useState } from "react";
import {
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCardContent,
  IonButton,
  IonContent,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonIcon,
  IonPage,
} from "@ionic/react";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { LatLngExpression, Map as LeafletMap } from "leaflet";
import { useParams } from "react-router";
import { informationCircleOutline, location } from "ionicons/icons";
import { del, post } from "../helpers/api";
import { useGet } from "../hooks/use-api";
import SafeMap from "./SafeMap";

type Attraction = {
  id: number;
  name: string;
  categories: string;
  address: string;
  phone_number: number;
  website: string;
  latitude: number;
  longitude: number;
  img_url: string;
  sub_district_id: number;
  description: string;
};

export default function CardByCategory() {
  const user_name = useSelector(
    (state: RootState) => state.auth.user?.name || state.auth.user?.username
  );
  const user_id = useSelector((state: RootState) => state.auth.user?.id);

  const { category, location_id } =
    useParams<{ category: string; location_id: string }>();

  const [attractions, setAttractions] = useGet<Attraction[]>(
    `/attractions/${category}/${location_id}`,
    [{} as Attraction]
  );
  const attraction = "error" in attractions ? undefined : attractions[0];

  const position: undefined | LatLngExpression = attraction
    ? [attraction.latitude, attraction.longitude]
    : undefined;

  const [msg, setMsg] = useState("加入書籤");

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="tertiary">
          <IonTitle className="ion-text-center">景點簡介</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        {!attraction ? (
          "loading attraction"
        ) : (
          <>
            <IonCard className="container">
              <div className="header">
                <IonCardHeader>
                  <IonCardSubtitle>為你推薦</IonCardSubtitle>

                  <IonCardTitle className="log">{attraction.name}</IonCardTitle>
                </IonCardHeader>
                <IonButton
                  className="ion-margin-start"
                  onClick={async () => {
                    if (user_name) {
                      const bookmarkObj = {
                        attractions_name: attraction.name,
                        attractions_id: attraction.id,
                        user_id: user_id,
                      };
                      console.log(bookmarkObj);
                      await post("/addbookmark", bookmarkObj);
                      if (msg === "加入書籤") {
                        setMsg("已加入書籤");
                      } else {
                        const attractions_id = attraction.id;
                        setMsg("加入書籤");
                        console.log("deleted", attraction.name);
                        await del(`/delbookmark/${user_id}/${attractions_id}`);
                      }
                    } else {
                      alert("請登入使用書籤");
                    }
                  }}
                >
                  {msg}
                </IonButton>
              </div>
              <IonCardContent>
                <img src={attraction.img_url} alt={attraction.name}></img>
              </IonCardContent>
            </IonCard>
            <IonCard>
              <IonCard>
                {attraction.description ? (
                  <IonCardContent>
                    <IonIcon icon={informationCircleOutline} />{" "}
                    {attraction.description}
                  </IonCardContent>
                ) : null}
              </IonCard>
              <IonCard>
                {attraction.address ? (
                  <IonCardContent>
                    <IonIcon icon={location} /> {attraction.address}
                  </IonCardContent>
                ) : null}
              </IonCard>
              {/* <IonCardContent> {attractions!.text}</IonCardContent> */}
            </IonCard>
          </>
        )}
        {position ? (
          <IonCard className="container">
            <SafeMap position={position} />
          </IonCard>
        ) : null}
      </IonContent>
    </IonPage>
  );
}
