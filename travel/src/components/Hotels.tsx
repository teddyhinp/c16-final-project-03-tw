import { IonCol, IonGrid, IonRow } from "@ionic/react"
import { Link } from "react-router-dom"
import DistrictCard from "./DistrictCard"

type district = {
    name: string,
    id: number
}

interface IAttractionsProp {
    districts: district[]
}


const Hotels: React.FC<IAttractionsProp> = (props) => {
    return (
        <IonGrid>
            <IonRow>
                {props.districts.map(district =>
                    <Link to={"/hotels/" + district.id + '/' + district.name} key={district.id}>
                        <IonCol> <DistrictCard name={district.name} /></IonCol>
                    </Link>
                )}
            </IonRow>
        </IonGrid>
    )
}

export default Hotels