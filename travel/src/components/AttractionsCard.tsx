import React, { useState } from 'react';
import { IonCard, IonCardContent, IonButton, IonIcon } from '@ionic/react';
import './AttractionsCard.css'
import { useSelector } from 'react-redux';
import { RootState } from '../redux/state';
import { location } from "ionicons/icons";
import { del, post } from '../helpers/api';

type Props = {
    id: number,
    name: string,
    categories: string,
    address: string,
    phone_number: number,
    website: string,
    latitude: number,
    longitude: number,
    img_url: string,
    sub_district_id: number,
    my_category: string
    description?: string
    district?: string
}


export default function AttractionsCard(props: Props) {
    const user_name = useSelector(
        (state: RootState) => state.auth.user?.name || state.auth.user?.username
    );
    const user_id = useSelector((state: RootState) => state.auth.user?.id);

    const [msg, setMsg] = useState("加入書籤");

    return (
        <>
            <IonCard className='LocationCardxxx' routerLink={"/AttractionDetailPage/" + props.id}>
                <IonCardContent className='facility'>
                    <div className='title'>{props.name}</div>
                    <img src={props.img_url} alt={props.name} />
                    <div>景點種類: {props.my_category}</div>

                    {props.address ? <div>
                        <IonIcon icon={location} />

                        地址: {props.address}</div> : null}
                    {props.website ? <IonButton href={props.website}>網址

                    </IonButton> : null}
                    <IonButton
                        className="ion-margin-start"
                        onClick={async (event) => {
                            event.preventDefault()
                            event.stopPropagation()
                            if (user_name) {
                                const bookmarkObj = {
                                    attractions_name: props!.name,
                                    attractions_id: props!.id,
                                    user_id: user_id,
                                };
                                post('/addbookmark', bookmarkObj)

                                if (msg === "加入書籤") {
                                    setMsg("已加入書籤");
                                } else {
                                    const attractions_id = props.id;
                                    setMsg("加入書籤");

                                    del(`/delbookmark'/${user_id}/${attractions_id}`)

                                }
                            } else {
                                alert("請登入使用書籤");
                            }
                        }}
                    >
                        {msg}
                    </IonButton>
                </IonCardContent>
            </IonCard>

        </>
    )
}

function delBookMark(arg0: string, arg1: number, attractions_id: number) {
    throw new Error('Function not implemented.');
}
