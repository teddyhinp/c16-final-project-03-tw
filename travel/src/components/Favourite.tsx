import { IonCol, IonGrid, IonRow } from "@ionic/react"
import SuggestionCard from "./SuggestionCard"

type favourite = {
    attractions_id: number
    attractions_name: string,
    id: number
    img_url: string
}



interface IfavouritesProp {
    favourites: favourite[]
}


const Favourite: React.FC<IfavouritesProp> = (props) => {
    return (
        <IonGrid>
            <IonRow>
                {props.favourites ? props.favourites.map(favourite =>

                    <IonCol key={favourite.id}> <SuggestionCard name={favourite.attractions_name} id={favourite.id} img_url={favourite.img_url} />
                    </IonCol>

                ) : null}
            </IonRow>
        </IonGrid>
    )
}

export default Favourite