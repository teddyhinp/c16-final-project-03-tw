import React from 'react';
import { IonCard, IonCardTitle, IonCardContent } from '@ionic/react';

type Props = {
    categories: string


}


export default function FacilitiesTypeCard(props: Props) {
    let img = './images/facilities/' + props.categories + '.jpg'
    return (
        <>
            <IonCard className='FacilitiesCard'>
                <IonCardContent >
                    <IonCardTitle className='fac_title'>{props.categories}</IonCardTitle>

                    <img src={img} alt={props.categories} ></img>
                </IonCardContent>
            </IonCard>

        </>
    )
}