import React from "react";
import {
  IonCard,
  IonCardContent,
} from "@ionic/react";
import "./MallsCard.css";

type Props = {
  id: number;
  name: string;
  address: string;
  latitude: number;
  longitude: number;
  img_url: string;
  sub_district_id: number;
};

export default function MallsCard(props: Props) {
  return (
    <>
      <IonCard className="LocationCard">
        <IonCardContent className="mall">
          <div className="title">{props.name}</div>
          <img src={props.img_url} alt={props.name} />

          <div>地址: {props.address}</div>
        </IonCardContent>
      </IonCard>
    </>
  );
}
