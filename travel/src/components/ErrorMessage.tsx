import { IonText } from "@ionic/react";

const ErrorMessage = (props: { error?: string }) => {
  if (props.error) {
    return <></>;
  }
  return <IonText className="ion-text-wrap">{props.error}</IonText>;
};

export default ErrorMessage;
