import { IonCol, IonGrid, IonRow } from "@ionic/react"
import SuggestionCard from "./SuggestionCard"

type popular = {
    name: string,
    id: number
}

interface IPopularsProp {
    populars: popular[]
}


const Popular: React.FC<IPopularsProp> = (props) => {
    return (
        <IonGrid>
            <IonRow>
                {props.populars.map(popular =>

                    <IonCol key={popular.id}> <SuggestionCard name={popular.name} id={popular.id} />
                    </IonCol>

                )}
            </IonRow>
        </IonGrid>
    )
}

export default Popular