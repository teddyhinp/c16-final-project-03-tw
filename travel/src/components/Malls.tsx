import { IonCol, IonGrid, IonRow } from "@ionic/react";
import { Link } from "react-router-dom";
import DistrictCard from "./DistrictCard";

type district = {
  name: string;
  id: number;
};

interface IMallsProp {
  districts: district[];
}

const Malls: React.FC<IMallsProp> = (props) => {
  return (
    <IonGrid>
      <IonRow>
        {props.districts.map((district) => (
          <Link to={"/malls/" + district.name} key={district.id}>
            <IonCol>
              <DistrictCard name={district.name} />
            </IonCol>
          </Link>
        ))}
      </IonRow>
    </IonGrid>
  );
};

export default Malls;
