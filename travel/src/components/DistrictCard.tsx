import React from "react";
import { IonCard, IonCardTitle, IonCardContent } from "@ionic/react";

type Props = {
  name: string;
};

//地區+顯示圖
export default function DistrictCard(props: Props) {
  let img = "./images/districts/" + props.name + ".png";

  return (
    <>
      <IonCard className="districtCard">
        <IonCardContent>
          <IonCardTitle>{props.name}</IonCardTitle>

          <img src={img} alt={props.name}></img>
        </IonCardContent>
      </IonCard>
    </>
  );
}
