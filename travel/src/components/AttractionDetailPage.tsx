import "../App.css";
import React, { useEffect, useState } from "react";
import {
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCardContent,
  IonButton,
  IonContent,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonButtons,
  IonBackButton,
  IonIcon,
  IonPage,
} from "@ionic/react";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { Map as LeafletMap } from "leaflet";
import { useParams } from "react-router";
import { location, informationCircleOutline } from "ionicons/icons";
import { del, getAPIServer, post } from "../helpers/api";
import { useGet } from "../hooks/use-api";

type Attraction = {
  id: number;
  name: string;
  categories: string;
  address: string;
  phone_number: number;
  website: string;
  latitude: number;
  longitude: number;
  img_url: string;
  sub_district_id: number;
  description?: string;
};

type Attractions = Attraction[];

export default function AttractionDetailPage() {
  const user_name = useSelector(
    (state: RootState) => state.auth.user?.name || state.auth.user?.username
  );
  const user_id = useSelector((state: RootState) => state.auth.user?.id);

  const { location_id } =
    useParams<{ location_id: string }>();

  const [attractionsArr, setAttractions] = useGet<Attractions>(
    `/getUnitAttractionsDetail/${location_id}`,
    [{} as Attraction]
  );
  const attractions = (attractionsArr as Attractions)[0] || ({} as Attraction);
  const [map] = useState<LeafletMap>();
  const [position] = useState<[number, number]>([
    attractions!.latitude,
    attractions!.longitude,
  ]);
  const [renderMapSequence, setRenderMapSequence] = useState([
    true,
    false,
    true,
  ]);

  const [msg, setMsg] = useState("加入書籤");

  useEffect(() => {
    if (renderMapSequence.length > 1) {
      setTimeout(() => {
        setRenderMapSequence(renderMapSequence.slice(1));
      }, 333);
    }
  }, [position, renderMapSequence]);

  useEffect(() => {
    if (map) {
      map.setView(position);
    }
  }, [position, map]);

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar color="tertiary">
          <IonButtons slot="start">
            <IonBackButton defaultHref={`/attractions/`} />
          </IonButtons>
          <IonTitle className="ion-text-center">景點簡介</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonCard className="container">
          <div className="header">
            <IonCardHeader>
              <IonCardSubtitle>為你推薦</IonCardSubtitle>

              <IonCardTitle className="log">{attractions.name}</IonCardTitle>
            </IonCardHeader>
            <IonButton
              className="ion-margin-start"
              onClick={async () => {
                if (user_name) {
                  const bookmarkObj = {
                    attractions_name: attractions!.name,
                    attractions_id: attractions!.id,
                    user_id: user_id,
                  };
                  post("/addbookmark", bookmarkObj);
                  if (msg === "加入書籤") {
                    setMsg("已加入書籤");
                  } else {
                    const attractions_id = attractions.id;
                    setMsg("加入書籤");

                    del(`/delbookmark/${user_id}/${attractions_id}`)
                  }
                } else {
                  alert("請登入使用書籤");
                }
              }}
            >
              {msg}
            </IonButton>
          </div>
          <IonCardContent>
            <img src={attractions.img_url} alt={attractions!.name}></img>
          </IonCardContent>
        </IonCard>
        <IonCard>
          {attractions.description ? (
            <IonCardContent>
              <IonIcon icon={informationCircleOutline} />{" "}
              {attractions!.description}
            </IonCardContent>
          ) : null}
        </IonCard>
        <IonCard>
          {attractions.address ? (
            <IonCardContent>
              {" "}
              <IonIcon icon={location} /> {attractions!.address}
            </IonCardContent>
          ) : null}
        </IonCard>
        {attractions.latitude ? (
          <IonCard className="container">
            {!renderMapSequence[0] ? (
              "loading map..."
            ) : (
              <MapContainer
                center={[attractions!.latitude, attractions!.longitude]}
                zoom={100}
                scrollWheelZoom={false}
              >
                <TileLayer
                  attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                  url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker
                  position={[attractions!.latitude, attractions!.longitude]}
                >
                  <Popup>
                    A pretty CSS3 popup. <br /> Easily customizable.
                  </Popup>
                </Marker>
              </MapContainer>
            )}
          </IonCard>
        ) : null}
      </IonContent>
    </IonPage>
  );
}
