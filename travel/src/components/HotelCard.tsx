import React from "react";
import { IonCard, IonCardContent, IonCardTitle } from "@ionic/react";
import './RestaurantCard.css'

type Props = {
  id: number;
  name: string;
  categories: string;
  address: string;
  latitude: number;
  longitude: number;
  sub_district_id: number;
};

export default function HotelCard(props: Props) {
  return (
    <>
      <IonCard className="RestaurantCard">
        <IonCardContent>
          <IonCardTitle className="restaurantTitle">{props.name}</IonCardTitle>
          <IonCardContent>
            <div>住宿種類: {props.categories}</div>
            <div>地址：{props.address}</div>
          </IonCardContent>
        </IonCardContent>
      </IonCard>
    </>
  );
}
