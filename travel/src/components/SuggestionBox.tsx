import {
  IonBackButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { IonSegment, IonSegmentButton } from "@ionic/react";
import { RootState } from "../redux/state";
import Favourite from "./Favourite";
import Popular from "./Popular";
import Preferences from "./Preferences";
import shuffle from "../helpers/shuffle";
import { getAPIServer } from "../helpers/api";

const SuggestionBox: React.FC = () => {
  const user_id = useSelector((state: RootState) => state.auth.user?.id);

  type preferenceItem = {
    id: number;
    name: string;
    categories: string;
    address: string;
    phone_number: number;
    website: string;
    latitude: number;
    longitude: number;
    img_url: string;
    sub_district_id: number;
  };

  type fav = {
    id: number;
    user_id: number;
    attractions_id: number;
    attractions_name: string;
    img_url: string;
  };

  const [tab, setTab] = useState<string | undefined>("favourite");
  let [preferences, setPreferences] = useState<string[]>();
  let [preferenceItems0, setPreferenceItems0] = useState<preferenceItem[]>();
  let [preferenceItems1, setPreferenceItems1] = useState<preferenceItem[]>();
  let [favs, setFavs] = useState<fav[]>();

  useEffect(() => {
    if (user_id) {
      let loadPreference = async () => {
        const res = await fetch(getAPIServer() + `/getPreference/${user_id}`);
        const json = await res.json();
        console.log("prefer", json);
        let getPreference = [json[0].preference1, json[0].preference2];

        setPreferences(getPreference);
      };

      let loadFav = async () => {
        const res = await fetch(getAPIServer() + `/loadbookmark/${user_id}`);
        const json = await res.json();

        setFavs(json);
      };
      loadFav();
      loadPreference();
    }
  }, [user_id]);

  useEffect(() => {
    if (preferences) {
      let loadPreferenceItems0 = async () => {
        // console.log(preferences![0])
        const res = await fetch(
          getAPIServer() + `/loadPreferenceItems0/${preferences![0]}`
        );
        const json = await res.json();

        setPreferenceItems0(shuffle(json));
      };
      loadPreferenceItems0();

      let loadPreferenceItems1 = async () => {
        const res = await fetch(
          getAPIServer() + `/loadPreferenceItems1/${preferences![1]}`
        );
        const json = await res.json();

        setPreferenceItems1(shuffle(json));
      };
      loadPreferenceItems1();
    } else {
      return;
    }
  }, [preferences]);

  return (
    <>
      <IonPage>
        <IonHeader>
          <IonToolbar color="primary">
            <IonButtons slot="start">
              <IonBackButton defaultHref="/tab/journey" />
            </IonButtons>
            <IonTitle className="ion-text-center">行程</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonContent>
          <IonSegment
            value={tab}
            onIonChange={(e) => setTab(e.detail.value)}
            className="tabText"
          >
            <IonSegmentButton value="favourite" className="segment-btn">
              我的最愛
            </IonSegmentButton>
            {preferences
              ? preferences.map((preference) => (
                  <IonSegmentButton
                    value={preference}
                    className="segment-btn"
                    key={preference}
                  >
                    {preference}
                  </IonSegmentButton>
                ))
              : null}
          </IonSegment>

          {favs ? tab === "favourite" && <Favourite favourites={favs} /> : null}
          {preferenceItems0
            ? tab === preferences![0] && (
                <Preferences preferenceItems={preferenceItems0} />
              )
            : null}
          {preferenceItems1
            ? tab === preferences![1] && (
                <Preferences preferenceItems={preferenceItems1} />
              )
            : null}
        </IonContent>
      </IonPage>
    </>
  );
};

export default SuggestionBox;
