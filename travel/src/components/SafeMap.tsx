import { useEffect, useState } from "react";
import { LatLngExpression, Map as LeafletMap } from "leaflet";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";

const SafeMap = (props: {
  position?: [number, number];
  markers?: Array<{
    id: string | number;
    position: [number, number];
    text?: string | JSX.Element;
  }>;
}) => {
  const { position, markers } = props;

  const center: [number, number] = [0, 0];
  if (!markers || markers.length === 0) {
    center[0] = 22.3174732;
    center[1] = 114.1709714;
  } else {
    markers.forEach((marker) => {
      center[0] += marker.position[0];
      center[1] += marker.position[1];
    });
    center[0] /= markers.length;
    center[1] /= markers.length;
  }

  const [map, setMap] = useState<LeafletMap>();

  const [renderMapSequence, setRenderMapSequence] = useState([
    true,
    false,
    true,
  ]);

  useEffect(() => {
    if (renderMapSequence.length > 1) {
      setTimeout(() => {
        setRenderMapSequence(renderMapSequence.slice(1));
      }, 555);
    }
  }, [renderMapSequence]);

  useEffect(() => {
    if (map) {
      map.setView(center);
    }
  }, [center[0], center[1], map]);

  if (!renderMapSequence[0]) {
    return <>loading map ...</>;
  }

  return (
    <>
      <MapContainer
        center={position || center}
        zoom={17}
        scrollWheelZoom={false}
        whenCreated={(map) => setMap(map)}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {markers?.map((marker) => (
          <Marker key={marker.id} position={marker.position}>
            {marker.text ? <Popup>{marker.text}</Popup> : null}
          </Marker>
        ))}
      </MapContainer>
    </>
  );
};

export default SafeMap;
