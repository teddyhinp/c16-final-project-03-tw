import { IonCol, IonGrid, IonRow } from "@ionic/react"
import { Link } from "react-router-dom"
import DistrictCard from "./DistrictCard"

type district = {
    name: string,
    id: number
}

interface IAttractionsProp {
    districts: district[]
}


const Attractions: React.FC<IAttractionsProp> = (props) => {
    return (
        <div>

            {props.districts.map(district =>
                <Link to={"/attractions/" + district.name} key={district.id}>
                    <div> <DistrictCard name={district.name} />
                    </div>
                </Link>
            )}

        </div>
    )
}

export default Attractions