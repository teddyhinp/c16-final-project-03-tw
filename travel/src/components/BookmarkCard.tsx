import React, { useEffect, useState } from 'react';
import { IonCard, IonCardContent, IonButton } from '@ionic/react';
import './AttractionsCard.css'
import { Link } from 'react-router-dom';
import { getAPIServer } from '../helpers/api';

type Props = {
    name: string
    id: number
    sub_district_id: number
    onRemove(): void
}



//地區+顯示圖 
export default function BookmarkCard(props: Props) {


    return (
        <>
            <IonCard className='BookmarkCard' >
                <IonCardContent>
                    <div className='title'>{props.name}</div>
                    <Link to={"/AttractionDetailPage/" + props.id}>
                        <IonButton >查看行程</IonButton>
                    </Link>
                    <IonButton onClick=
                        {props.onRemove}
                    >在我的書籤移除</IonButton>

                </IonCardContent>
            </IonCard>

        </>
    )
}