import React from 'react';
import { IonCard, IonCardTitle, IonIcon, IonLabel, IonButton } from '@ionic/react';
import { addCircleOutline } from "ionicons/icons";
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../redux/state';
import './SuggestionCard.css'
import { getAPIServer } from '../helpers/api';
type Props = {
    name: string
    id: number
    address?: string,
    img_url?: string

}


export default function SuggestionCard(props: Props) {
    const user_id = useSelector(
        (state: RootState) => state.auth.user?.id)
    // let img = './images/facilities/' + props.name + '.jpg'
    const [press, setPress] = useState(false)
    const [msg, setMsg] = useState('加入')

    let onadd = async () => {
        if (press === false) {
            setPress(true)
            setMsg('按此取消')

            let addObj = { name: props.name, id: props.id }
            await fetch(getAPIServer() + `/addPlan/${user_id}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(addObj)
            })

            console.log("Add to DB", { name: props.name, id: props.id })
        } else {
            setPress(false)
            setMsg('加入')
            console.log('Del from DB', { name: props.name, id: props.id })
            await fetch(getAPIServer() + `/delplanItem/${user_id}/${props.id}`, {
                method: 'DELETE',
            })
        }
    }

    return (
        <>
            <IonCard className='SuggestionCard'>
                <IonCardTitle className='sug_title'>{props.name}</IonCardTitle>
                {props.address ? <div>{props.address}</div> : null}
                {props.img_url ? <img src={props.img_url} alt={props.name} ></img> : null}

                <IonButton className='add-btn' onClick={onadd}>
                    <IonIcon icon={addCircleOutline} />
                    <IonLabel>{msg}</IonLabel>
                </IonButton>
            </IonCard>

        </>
    )
}