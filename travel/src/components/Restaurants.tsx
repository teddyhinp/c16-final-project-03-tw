import React, { useState, FormEvent, useEffect } from "react";
import {
  IonGrid,
  IonButton,
  IonSelect,
  IonLabel,
  IonItem,
  IonSelectOption,
  IonCol,
  IonContent,
  IonTitle,
  IonToast,
} from "@ionic/react";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { Form } from "react-bootstrap";
import RestaurantCard from "./RestaurantCard";

import { Map as LeafletMap } from "leaflet";
import { getAPIServer } from "../helpers/api";
import SafeMap from "./SafeMap";

type district = { id: number; name: string };
type subDistrict = { id: number; name: string };
type category = { categories: string };
type restaurantsList = {
  id: number;
  name: string;
  categories: string;
  address: string;
  latitude: number;
  longitude: number;
  sub_district_id: number;
};
export const Restaurants: React.FC = () => {
  const [districts, setDistricts] = useState<district[]>([]);
  const [selectedDistrict, setSelectedDistrict] = useState<district>({} as any);
  const [subDistricts, setSubDistricts] = useState<subDistrict[]>([]);
  const [selectedSubDistrict, setSelectedSubDistrict] = useState<string>("");

  const [categories, setCategories] = useState<category[]>([]);
  const [selectedCategories, setSelectedCategories] = useState<category>(
    {} as any
  );
  const [restaurantsLists, setRestaurantsLists] = useState<restaurantsList[]>(
    []
  );
  const [showToast1, setShowToast1] = useState(false);

  function getLocation() {
    const options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0,
    };

    function success(pos: any) {
      var crd = pos.coords;
      let { latitude, longitude } = pos.coords;
      // setCenter([latitude, longitude]);
      console.log("Your current position is:");
      console.log(`More or less ${crd.accuracy} meters.`);
    }

    function error(err: any) {
      console.warn(`ERROR(${err.code}): ${err.message}`);
    }
    navigator.geolocation.getCurrentPosition(success, error, options);
  }

  useEffect(() => {
    try {
      let districts = async () => {
        const res = await fetch(getAPIServer() + `/district`);
        const json = await res.json();

        setDistricts(json);
      };
      districts();
    } catch (error) {
      console.error(error);
    }
  }, []);

  useEffect(() => {
    if (selectedDistrict.id) {
      let subDistricts = async () => {
        const res = await fetch(
          getAPIServer() + `/district/${selectedDistrict.id}`
        );
        const json = await res.json();
        console.log(json, "subDistricts");
        setSubDistricts(json);
      };
      subDistricts();
    } else {
      return;
    }
  }, [selectedDistrict]);
  console.log(selectedDistrict);

  useEffect(() => {
    let categories = async () => {
      const res = await fetch(getAPIServer() + `/categories`);
      const json = await res.json();
      let categoryList = json.rows;

      setCategories(categoryList);
    };
    categories();
  }, []);

  function loadRestaurants() {
    let restaurantsLists = async () => {
      let selectedSubDistrict_id: number = subDistricts.filter(
        (subDistrict) => subDistrict.name === selectedSubDistrict
      )[0].id;

      const res = await fetch(
        getAPIServer() +
          `/categories/${selectedSubDistrict_id}/${selectedCategories}`
      );
      const json = await res.json();

      if (json.length === 0) {
        setShowToast1(true);
      }
      setRestaurantsLists(json);
    };
    restaurantsLists();
  }

  function submit(event: FormEvent) {
    event.preventDefault();
    console.log("submit form data:", {
      selectedCategories,
      selectedDistrict,
      selectedSubDistrict,
    });
  }

  console.log(!!restaurantsLists === true);

  return (
    <IonGrid>
      <SafeMap
        markers={restaurantsLists.map((item) => ({
          id: item.id,
          position: [item.latitude, item.longitude],
          text: item.name,
        }))}
      />

      <Form onSubmit={submit}>
        <IonItem>
          <IonLabel>選擇地區</IonLabel>
          <IonSelect
            className="district"
            value={selectedDistrict}
            okText="OK"
            cancelText="Cancel"
            onIonChange={(e) => setSelectedDistrict(e.detail.value!)}
          >
            {districts.map((district) => (
              <IonSelectOption key={district.id} value={district}>
                {district.name}
              </IonSelectOption>
            ))}
          </IonSelect>
        </IonItem>

        <IonItem>
          <IonLabel>選擇分區</IonLabel>
          <IonSelect
            className="subDistrict"
            value={selectedSubDistrict}
            okText="OK"
            cancelText="Cancel"
            onIonChange={(e) => setSelectedSubDistrict(e.detail.value!)}
          >
            {subDistricts.map((subDistrict) => (
              <IonSelectOption key={subDistrict.id} value={subDistrict.name}>
                {subDistrict.name}
              </IonSelectOption>
            ))}
          </IonSelect>
        </IonItem>

        <IonItem>
          <IonLabel>選擇食品/餐廳類型</IonLabel>
          <IonSelect
            className="category"
            value={selectedCategories}
            okText="OK"
            cancelText="Cancel"
            onIonChange={(e) => setSelectedCategories(e.detail.value!)}
          >
            {categories.map((category, i) => (
              <IonSelectOption key={i} value={category.categories}>
                {category.categories}
              </IonSelectOption>
            ))}
          </IonSelect>
        </IonItem>

        <IonButton
          type="submit"
          className="allowGetNavigation"
          color="warning"
          onClick={loadRestaurants}
        >
          搜尋
        </IonButton>

        {restaurantsLists.length > 0
          ? restaurantsLists.map((restaurantsList) => (
              <IonCol key={restaurantsList.id}>
                <RestaurantCard
                  id={restaurantsList.id}
                  name={restaurantsList.name}
                  categories={restaurantsList.categories}
                  address={restaurantsList.address}
                  latitude={restaurantsList.latitude}
                  longitude={restaurantsList.longitude}
                  sub_district_id={restaurantsList.sub_district_id}
                />
              </IonCol>
            ))
          : null}
        {/* : <div>你所選的地區沒有相關餐廳</div>} */}
      </Form>

      <IonToast
        isOpen={showToast1}
        onDidDismiss={() => setShowToast1(false)}
        message="沒有相關搜尋結果."
        duration={2000}
      />
    </IonGrid>
  );
};

export default Restaurants;
