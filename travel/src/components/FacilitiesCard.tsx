import React from 'react';
import { IonCard, IonCardContent, IonButton, IonIcon } from '@ionic/react';
import './AttractionsCard.css'
import { location } from "ionicons/icons";
type Props = {
    id: number,
    name: string,
    categories: string,
    address: string,
    phone_number: number,
    website: string,
    latitude: number,
    longitude: number,
    sub_district_id: number,
}


export default function FacilitiesCard(props: Props) {

    return (
        <>
            <IonCard className='facilitiesCard'>
                <IonCardContent className='facility'>
                    <div>{props.name}</div>
                    <div>設施種類: {props.categories}</div>
                    <div><IonIcon icon={location} /> {props.address}</div>
                    {props.website ? <IonButton href={props.website}>網址
                    </IonButton> : null}

                </IonCardContent>
            </IonCard>

        </>
    )
}