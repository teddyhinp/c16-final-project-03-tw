import { IonGrid, IonRow } from "@ionic/react"

import SuggestionCard from "./SuggestionCard"

type preferenceItem = {

    id: number,
    name: string,
    categories: string,
    address: string,
    phone_number: number,
    website: string,
    latitude: number,
    longitude: number,
    img_url: string,
    sub_district_id: number,
}


interface IPreferences {
    preferenceItems: preferenceItem[]
}


const Preferences: React.FC<IPreferences> = (props) => {
    return (
        <IonGrid>
            <IonRow>
                {props.preferenceItems.map(preferenceItem =>
                    <SuggestionCard name={preferenceItem.name} id={preferenceItem.id} img_url={preferenceItem.img_url} address={preferenceItem.address} key={preferenceItem.id} />

                )}
            </IonRow>
        </IonGrid>
    )
}

export default Preferences