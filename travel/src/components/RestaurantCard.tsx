import React from "react";
import { IonCard, IonCardContent, IonCardTitle, IonIcon } from "@ionic/react";
import { restaurant, location } from "ionicons/icons";
import "./RestaurantCard.css";

type Props = {
  id: number;
  name: string;
  categories: string;
  address: string;
  latitude: number;
  longitude: number;
  sub_district_id: number;
};

export default function RestaurantCard(props: Props) {
  return (
    <>
      <IonCard className="RestaurantCard">
        <IonCardContent>
          <IonCardTitle className="restaurantTitle">{props.name}</IonCardTitle>
          <IonCardContent>
            <div>
              <IonIcon icon={restaurant} />
              {props.categories}
            </div>
            <div>
              <IonIcon icon={location} />
              {props.address}
            </div>
          </IonCardContent>
        </IonCardContent>
      </IonCard>
    </>
  );
}
