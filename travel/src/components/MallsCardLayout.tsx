import "../App.css";
import React, { useEffect, useState } from "react";
import {
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCardContent,
  IonContent,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonButtons,
  IonBackButton,
} from "@ionic/react";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";

import { Map as LeafletMap } from "leaflet";
import { useParams } from "react-router";
import { get, getAPIServer } from "../helpers/api";
import { useGet } from "../hooks/use-api";

type Malls = {
  id: number;
  name: string;
  address: string;
  latitude: number;
  longitude: number;
  img_url: string;
  sub_district_id: number;
};

export default function MallsCardLayout() {
  const { district, location_id } =
    useParams<{ district: string; location_id: string }>();

  const [mallsList, setMalls] = useGet<Malls[]>(
    `/malls/${district}/${location_id}`,
    [{} as Malls]
  );
  const malls = (mallsList as Malls[])[0];
  const [map, setMap] = useState<LeafletMap>();
  const [position, setPosition] = useState<[number, number]>([
    malls!.latitude,
    malls!.longitude,
  ]);
  const [renderMapSequence, setRenderMapSequence] = useState([
    true,
    false,
    true,
  ]);

  useEffect(() => {
    if (renderMapSequence.length > 1) {
      setTimeout(() => {
        setRenderMapSequence(renderMapSequence.slice(1));
      }, 555);
    }
  }, [position, renderMapSequence]);

  useEffect(() => {
    if (map) {
      map.setView(position);
    }
  }, [position, map]);

  if (!renderMapSequence[0]) {
    return (
      <IonContent>
        <IonTitle className="Ion-text-center"> loading...</IonTitle>
      </IonContent>
    );
  }

  return (
    <>
      <IonHeader>
        <IonToolbar color="primary">
          <IonButtons slot="start">
            <IonBackButton defaultHref="/tab/attractions" />
          </IonButtons>
          <IonTitle className="ion-text-center">商場簡介</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonCard className="container">
          <div className="header">
            <IonCardHeader>
              <IonCardSubtitle>為你推薦</IonCardSubtitle>

              <IonCardTitle className="log">{malls.name}</IonCardTitle>
            </IonCardHeader>
          </div>
          <IonCardContent>
            <img src={malls.img_url} alt={malls!.name}></img>
          </IonCardContent>
        </IonCard>
        <IonCard></IonCard>
        {malls.latitude ? (
          <IonCard className="container">
            <MapContainer
              center={[malls!.latitude, malls!.longitude]}
              zoom={17}
              scrollWheelZoom={false}
            >
              <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
              <Marker position={[malls!.latitude, malls!.longitude]}>
                <Popup>{malls.name}</Popup>
              </Marker>
            </MapContainer>
          </IonCard>
        ) : null}
      </IonContent>
    </>
  );
}
