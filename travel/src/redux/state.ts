import { AuthState } from './auth/state'

import { RouterState } from 'connected-react-router'


export type RootState = {
    auth: AuthState
    router: RouterState
}