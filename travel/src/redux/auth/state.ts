import { JWTPayload } from '../../../../server/auth/types'

export type AuthState = {
    user?: JWTPayload
    isLoading:boolean
    error?:string
    token?: string
    isLoggedIn: boolean
}

export type User = JWTPayload


export const initialState : AuthState = {
    isLoggedIn: false,
    isLoading: false,

}