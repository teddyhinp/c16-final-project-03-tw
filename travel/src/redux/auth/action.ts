// import { User } from "./state"
import { JWTPayload } from "../../../../server/auth/types";

export function loginLoading() {
  return {
    type: "loginLoading" as const,
  };
}

export function loginSuccess(token: string, payload: JWTPayload) {
  return {
    type: "loginSuccess" as const,
    token,
    payload,
  };
}

export function loginFailed(reason: string) {
  return {
    type: "loginFailed" as const,
    reason,
  };
}

export function logout() {
  return {
    type: "logout" as const,
  };
}

export type AuthAction =
  | ReturnType<typeof loginLoading>
  | ReturnType<typeof loginSuccess>
  | ReturnType<typeof loginFailed>
  | ReturnType<typeof logout>;
