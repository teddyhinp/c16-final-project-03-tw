import { AuthAction } from "./action";
import { initialState, AuthState } from "./state";

export const authReducer = (
  state: AuthState = initialState,
  action: AuthAction
): AuthState => {
  switch (action.type) {
    case "loginLoading":
      return {
        ...state,
        isLoading: true,
      };
    case "loginSuccess":
      return {
        user: action.payload,
        isLoggedIn: true,
        isLoading: false,
        error: undefined,
      };
    case "loginFailed":
      return {
        isLoggedIn: false,
        isLoading: false,
        error: action.reason,
      };
    case "logout":
      return {
        isLoggedIn: false,
        isLoading: false,
      };
    default:
      return state;
  }
};
