import { RootDispatch } from "../dispatch";
import { loginFailed, loginLoading, loginSuccess, logout } from "./action";
import jwtDecode from "jwt-decode";
import { JWTPayload } from "../../../../server/auth/types";
import { getAPIServer } from "../../helpers/api";
import { errorToString } from "../../helpers/format";
import { authReducer } from "./reducer";

export function loginWithPasswordThunk(
  username: string,
  password: string,
  history: any
) {
  return async (dispatch: RootDispatch) => {
    let origin;
    try {
      origin = getAPIServer();
    } catch (error) {
      dispatch(loginFailed(errorToString(error)));
      return;
    }

    dispatch(loginLoading());

    let json: any;

    try {
      let res = await fetch(`${origin}/login/password`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ username, password }),
      });
      json = await res.json();
      console.log(json);
    } catch (error) {
      console.error("failed to call login with password API:", error);
      dispatch(loginFailed(errorToString(error)));
      return;
    }
    if (json.error) {
      console.error("login with password response error:", json.error);
      dispatch(loginFailed(json.error));
      return;
    }

    let token: string = json.token;
    dispatch(handleTokenThunk(token));
    history.push("/tab/settings");
  };
}

function handleTokenThunk(token: string) {
  return (dispatch: RootDispatch) => {
    localStorage.setItem("token", token);
    try {
      let payload = jwtDecode<JWTPayload>(token);
      dispatch(loginSuccess(token, payload));
      return;
    } catch (error) {
      console.error("failed to decode JWT token:", error);
      dispatch(loginFailed(errorToString(error)));
    }
  };
}

export function checkTokenThunk() {
  return (dispatch: RootDispatch) => {
    let token = localStorage.getItem("token");
    if (!token) {
      dispatch(logout());
      return;
    }
    dispatch(handleTokenThunk(token));
  };
}

export function loginWithFacebookThunk(accessToken: string) {
  return async (dispatch: RootDispatch) => {
    let origin;
    try {
      origin = getAPIServer();
    } catch (error) {
      dispatch(loginFailed(errorToString(error)));
      return;
    }
    let json: any;
    let history: any;
    try {
      let res = await fetch(`${origin}/login/facebook`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ accessToken }),
      });
      json = await res.json();
    } catch (error) {
      console.error("failed to call login with facebook API:", error);
      dispatch(loginFailed(errorToString(error)));
      return;
    }
    if (json.error) {
      console.error("login with facebook response error:", json.error);
      dispatch(loginFailed(json.error));
      return;
    }

    let token: string = json.token;
    dispatch(handleTokenThunk(token));
    history.push("/tab/settings");
  };
}
