import { authReducer } from './auth/reducer'
import { RootState } from './state'
import { RootAction } from './action'
import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import history from './history'


export const rootReducer: (
    state: RootState | undefined,
    action: RootAction,
    ) => RootState = combineReducers({
        auth: authReducer,
        router: connectRouter(history)
    })
    