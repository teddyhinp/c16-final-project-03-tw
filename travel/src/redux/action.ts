import { Dispatch } from 'redux'
import { AuthAction } from './auth/action'


export type RootAction = AuthAction 