import {createStore, applyMiddleware, compose} from 'redux'
import { rootReducer } from './reducer'
import  logger from 'redux-logger'
import { createBrowserHistory } from 'history'
import {
    RouterState,
    connectRouter,
    routerMiddleware,
    CallHistoryMethodAction
} from 'connected-react-router';
import history from './history'
import thunk, { ThunkDispatch } from 'redux-thunk'
import { RootState } from './state';
import { RootAction } from './action';
export type { RootState } from './state' 



declare global {
    /* tslint:disable:interface-name */
    interface Window {
    __REDUX_DEVTOOLS_EXTENSION__: any
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
    }

    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

    const rootEnhancer = composeEnhancers(
    applyMiddleware(logger),
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history)),
    )

    const store = createStore(rootReducer, rootEnhancer)
    
    export default store
