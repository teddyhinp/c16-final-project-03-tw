import type { ThunkDispatch } from "redux-thunk"
import type { RootAction } from "./action"
import type { RootState } from "./state"

type RootEnhancer = {}
export type RootDispatch = ThunkDispatch<RootState, RootEnhancer, RootAction>