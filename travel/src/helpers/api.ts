export function getAPIServer() {
  let origin = window.location.origin;
  if (origin === "http://localhost:3000") {
    return "http://localhost:8080";
  }
  return "https://api.travelplanet.katielok.xyz";
  // let { REACT_APP_API_SERVER } = process.env;
  // if (!REACT_APP_API_SERVER) {
  //   console.error("missing REACT_APP_API_SERVER in env");
  //   throw new Error("Missing REACT_APP_API_SERVER in env");
  // }
  // return REACT_APP_API_SERVER;
}

export async function get<T = unknown>(url: string): Promise<T> {
  // try {
  //   const res = await fetch(getAPIServer() + url, {
  //     headers: {
  //       Authorization: "Bearer " + localStorage.getItem("token"),
  //     },
  //   });
  //   const json = await res.json();
  //   return json as T;
  // } catch (error: any) {
  //   return { error: error.toString() };
  // }
  return fetch(getAPIServer() + url, {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  }).then((res) => res.json());
}

export async function del(url: string) {
  const res = await fetch(getAPIServer() + url, {
    method: "DELETE",
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
    },
  });
  const json = await res.json();
  return json;
}

export async function post(url: string, body: object = {}) {
  const res = await fetch(getAPIServer() + url, {
    method: "POST",
    headers: {
      Authorization: "Bearer " + localStorage.getItem("token"),
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  });
  const json = await res.json();
  return json;
}
