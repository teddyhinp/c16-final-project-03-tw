export function errorToString(error: unknown): string {
    return (error as Error).toString();
}