import { useEffect } from "react";
import { get } from "../helpers/api";
import { useStorageState } from "react-use-storage-state";

export function useGet<T>(url: string, initialValue: T) {
  const [json, setJson] = useStorageState<T | { error: string }>(
    url,
    initialValue
  );
  useEffect(() => {
    get<T>(url)
      .then(setJson)
      .catch((error) => {
        console.error("Failed to GET", url, "error:", error);
      });
  }, [url, setJson]);
  return [json, setJson] as const;
}

export type Attraction = {
  id: number;
  name: string;
  categories: string;
  address: string;
  phone_number: number;
  website: string;
  latitude: number;
  longitude: number;
  img_url: string;
  sub_district_id: number;
  my_category: string;
  description?: string;
};

type Mall = {
  id: number;
  name: string;
  address: string;
  latitude: number;
  longitude: number;
  img_url: string;
  sub_district_id: number;
};

export function useAttractions(district: string) {
  const [attractions2D] = useGet<Attraction[][]>(
    "/attractions/" + district,
    []
  );
  if ("error" in attractions2D) {
    return attractions2D;
  }
  const attractions: Attraction[] = [];
  attractions2D.forEach((array) => attractions.push(...array));
  return attractions;
}
export function useMalls(district: string) {
  const [malls2D] = useGet<Attraction[][]>("/malls/" + district, []);
  if ("error" in malls2D) {
    return malls2D;
  }
  const malls: Mall[] = [];
  malls2D.forEach((array) => malls.push(...array));
  return malls;
}
