import { Route, Switch } from "react-router-dom";

import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
} from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import {
  home,
  search,
  bookmark,
  compass,
  cog,
  bedOutline,
} from "ionicons/icons";
import { RegisterPage } from "./pages/RegisterPage";
import Tab1 from "./pages/Tab1";
import Tab2 from "./pages/Tab2";
import Tab3 from "./pages/Tab3";
import Tab4 from "./pages/Tab4";
import { SettingPage } from "./pages/SettingPage";
import { LoginPage } from "./pages/LoginPage";
import { TermsPage } from "./pages/TermsPage";
import { PrivacyPage } from "./pages/PrivacyPage";
import { NewJourney } from "./pages/NewJourney";
import { AboutPage } from "./pages/AboutPage";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";
import "./App.css";
import AttractionsPage from "./pages/AttractionsPage";
import { Redirect } from "react-router-dom";
import ResumeJourney from "./pages/ResumeJourney";
import { EditUserProfile } from "./pages/EditUserProfile";
import AttractionDetailPage from "./components/AttractionDetailPage";
import FacilitiesPage from "./pages/FacilitiesPage";
import SuggestionBox from "./components/SuggestionBox";
import MallsPage from "./pages/MallsPage";
import MallsCardLayout from "./components/MallsCardLayout";
import HotelsPage from "./pages/HotelsPage";
import CardByCategory from "./components/CardByCategory";
import CategoryPage from "./pages/CategoryPage";
import { MapContainer, TileLayer } from "react-leaflet";
import SafeMap from "./components/SafeMap";
import { useEffect } from "react";
import { checkTokenThunk } from "./redux/auth/thunk";
import { useDispatch } from "react-redux";

function Map() {
  return (
    <div hidden>
      <SafeMap position={[22.3174732, 114.1709714]} />
    </div>
  );
}

const App: React.FC = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(checkTokenThunk());
  }, []);
  return (
    <IonApp>
      <Map />
      <IonReactRouter>
        <Route exact path="/">
          <Redirect to="/tab/home" />
        </Route>
        <Route path="/tab">
          <IonTabs>
            <IonRouterOutlet>
              <Route exact path="/tab/home">
                <Tab1 />
              </Route>
              <Route exact path="/tab/attractions">
                <Tab2 />
              </Route>
              <Route exact path="/tab/bookmarks">
                <Tab3 />
              </Route>
              <Route exact path="/tab/hotels">
                <HotelsPage />
              </Route>
              <Route exact path="/tab/journey">
                <Tab4 />
              </Route>
              <Route exact path="/tab/settings">
                <SettingPage />
              </Route>
            </IonRouterOutlet>

            <IonTabBar slot="bottom">
              <IonTabButton tab="tab-home" href="/tab/home">
                <IonIcon icon={home} />
                <IonLabel>主頁</IonLabel>
              </IonTabButton>
              <IonTabButton tab="tab-attractions" href="/tab/attractions">
                <IonIcon icon={search} />
                <IonLabel>景點</IonLabel>
              </IonTabButton>
              <IonTabButton tab="tab-hotels" href="/tab/hotels">
                <IonIcon icon={bedOutline} />
                <IonLabel>住宿</IonLabel>
              </IonTabButton>
              <IonTabButton tab="tab-bookmarks" href="/tab/bookmarks">
                <IonIcon icon={bookmark} />
                <IonLabel>書籤</IonLabel>
              </IonTabButton>
              <IonTabButton tab="tab-journey" href="/tab/journey">
                <IonIcon icon={compass} />
                <IonLabel>行程</IonLabel>
              </IonTabButton>
              <IonTabButton tab="tab-settings" href="/tab/settings">
                <IonIcon icon={cog} />
                <IonLabel>設定</IonLabel>
              </IonTabButton>
            </IonTabBar>
          </IonTabs>
        </Route>
        <Switch>
          <Route exact path="/register">
            <RegisterPage />
          </Route>
          <Route exact path="/login">
            <LoginPage />
          </Route>
          <Route exact path="/terms">
            <TermsPage />
          </Route>
          <Route exact path="/privacy">
            <PrivacyPage />
          </Route>
          <Route exact path="/about">
            <AboutPage />
          </Route>
          <Route exact path="/AttractionDetailPage/:location_id">
            <AttractionDetailPage />
          </Route>
          <Route exact path="/malls/:district/:location_id">
            <MallsCardLayout />
          </Route>
          <Route exact path="/newJourney">
            <NewJourney />
          </Route>
          <Route exact path="/suggestions">
            <SuggestionBox />
          </Route>
          <Route exact path="/resumeJourney">
            <ResumeJourney />
          </Route>
          <Route exact path="/tab/editProfile">
            <EditUserProfile />
          </Route>
          <Route exact path="/attractions/:district">
            <AttractionsPage />
          </Route>
          <Route exact path="/facilities/:facility">
            <FacilitiesPage />
          </Route>
          <Route exact path="/malls/:district">
            <MallsPage />
          </Route>
          <Route exact path="/hotels/:district_id">
            <FacilitiesPage />
          </Route>
          <Route exact path="/attractions/:category/:location_id">
            <CardByCategory />
          </Route>
          <Route exact path="/category/:category">
            <CategoryPage />
          </Route>
        </Switch>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
