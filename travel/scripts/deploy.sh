#!/usr/bin/bash
set -e
set -o pipefail
set -x


npm run build 
aws s3 sync build s3://travelplanet
aws cloudfront create-invalidation --distribution-id E1M69MTQGZSWQ1 --paths '/*'