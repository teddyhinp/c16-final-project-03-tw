#!/bin/bash
set -e
set -o pipefail
set -x

# npx cap add ios

# npm run build
npx cap sync ios
npx cap open ios
npx cap run  ios
