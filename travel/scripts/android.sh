#!/usr/bin/bash
set -e
set -o pipefail
set -x


npm run build
# npx cap add android
npx cap sync android
npx cap open android
npx cap run  android