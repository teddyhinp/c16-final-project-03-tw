# c16-final-project-03-tw-旅行星球 (TripAdvisor)

| Title            | Type        | Link                                                   |
| ---------------- | ----------- | ------------------------------------------------------ |
| ionic Framework  | Frontend    | https://ionicframework.com/                            |
| Openrouteservice | Map         | https://openrouteservice.org/                          |
| leaflet.js       | Map         | https://leafletjs.com/                                 |
| openstreetmap    | Map         | https://www.openstreetmap.org/#map=11/22.3567/114.1363 |
| HK GEODATA       | API/DATASET | https://geodata.gov.hk/gs/                             |
| FOURSQUARE       | API/MAPPING | https://foursquare.com/                                |

### 上面的API 有每分鐘call 的limit 可以考慮 google apps script 既方式 拎點對點距離,時間

| Title            | Type        | Link                                                   |
| ---------------- | ----------- | ------------------------------------------------------ |
|Google Map-Data|Map Tutorial|http://www.chicagocomputerclasses.com/google-sheets-google-maps-function-distance-time/|
|Google Script Doc|Doc|https://developers.google.com/apps-script/reference/maps/direction-finder|
|Google data studio|Dashboard|https://datastudio.google.com/reporting/18e545a9-cc0d-4b64-9875-73cd48f0aa5c

`<iframe width="600" height="1068" src="https://datastudio.google.com/embed/reporting/18e545a9-cc0d-4b64-9875-73cd48f0aa5c/page/EV3dC" frameborder="0" style="border:0" allowfullscreen></iframe>`

SQL (做好就tick)

- [ ] 地區list(e.g['黃大仙','觀塘','沙田'...])
- [ ] 景點list sorted by 地區(e.g {"黃大仙":["景點A","景點B","景點C",...]})
- [ ] 獨立景點 info (景點名,圖片,景點description)



```
/**
* Get Distance between 2 different addresses.
* @param start_address Address as string Ex. "300 N LaSalles St, Chicago, IL"
* @param end_address Address as string Ex. "900 N LaSalles St, Chicago, IL"
* @param return_type Return type as string Ex. "miles" or "kilometers" or "minutes" or "hours"
* @customfunction
*/

function GOOGLEMAPS(start_address,end_address,return_type) {

  // https://www.chicagocomputerclasses.com/
  // Nov 2017
  // improvements needed
  
  var mapObj = Maps.newDirectionFinder();
  mapObj.setOrigin(start_address);
  mapObj.setDestination(end_address);
  var directions = mapObj.getDirections();
  
  var getTheLeg = directions["routes"][0]["legs"][0];
  
  var meters = getTheLeg["distance"]["value"];
  
  switch(return_type){
    case "miles":
      return meters * 0.000621371;
      break;
    case "minutes":
        // get duration in seconds
        var duration = getTheLeg["duration"]["value"];
        //convert to minutes and return
        return duration / 60;
      break;
    case "hours":
        // get duration in seconds
        var duration = getTheLeg["duration"]["value"];
        //convert to hours and return
        return duration / 60 / 60;
      break;      
    case "kilometers":
      return meters / 1000;
      break;
    default:
      return "Error: Wrong Unit Type";
   }
  
}
```

         
            
         
              
    
           
   
             
   
          
            
         
            
          
        
     
 

文化藝術:"Museum",'Performing Arts Venue','Arts & Entertainment','Public Art','Art Gallery','Art Museum','Museum',    
演藝娛樂:'Movie Theater', 'Opera House', 'Exhibit','Indie Theater','Indie Movie Theater', 'Theater','Concert Hall','History Museum','Music Venue', 'Science Museum', Amphitheater',
戶外探索:'Memorial Site','Outdoor Sculpture','Zoo Exhibit','Mini Golf','Planetarium',   
朋友相聚:'VR Cafe','Rugby Stadium','Theme Park','Go Kart Track',
其他:'Racecourse', 'Laser Tag',   'Country Dance Club',  'Comedy Club',   'Dance Studio',   'Multiplex',  'Rock Club',

