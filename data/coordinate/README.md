1. Run getAttractionCoordinates.ts
    - Create coordinates.json
2. Run getRouteFromGoogleAPI.ts with API key
    - Record founded routes in route.json 
    - Record unfounded routes in error.txt
3. Run updateUnfoundedRoute.ts
    - Update unfounded routes in route2.json