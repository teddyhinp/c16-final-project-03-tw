import axios from 'axios';
import jsonfile from 'jsonfile';
import dotenv from 'dotenv';
import fs from 'fs';

dotenv.config();

async function main() {
    const places = jsonfile.readFileSync('./resources/coordinates.json');

    const obj = [];
    for (let i = 0; i < places.length; i++) {
        for (let y = i + 1; y < places.length; y++) {
            try {
                const res = await axios.get('https://maps.googleapis.com/maps/api/directions/json', {
                    params: {
                        key: process.env.googleKey,
                        origin: places[i].coordinate.join(','),
                        destination: places[y].coordinate.join(','),
                        mode: "transit",
                    }
                });
                obj.push({
                    origin: places[i].name,
                    destination: places[y].name,
                    distance: await res.data.routes[0].legs[0].distance.value,
                    time: Math.floor(await res.data.routes[0].legs[0].duration.value / 60)
                });

                obj.push({
                    origin: places[y].name,
                    destination: places[i].name,
                    distance: await res.data.routes[0].legs[0].distance.value,
                    time: Math.floor(await res.data.routes[0].legs[0].duration.value / 60)
                });
            }catch(e){
                fs.appendFileSync('./resources/error.txt', `Error exists in origin: ${places[i].name}(${i}) ,destination: ${places[y].name}(${y})\n`)
                console.log(obj.length);
            }      
        }
    }
    console.log(obj.length);
    jsonfile.writeFileSync('./resources/route.json', obj, { spaces: 4 });   
}

main();