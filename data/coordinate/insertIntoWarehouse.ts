import { Client } from 'pg';
import dotenv from 'dotenv';
import jsonfile from "jsonfile";

dotenv.config();

const client = new Client({
    host: process.env.RDS_HOST,
    port: 5432,
    database: process.env.RDS_NAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD
});

async function main() {
    await client.connect();
    const routes1 = jsonfile.readFileSync('./resources/route.json');
    const routes2 = jsonfile.readFileSync('./resources/route2.json');

    // Inserts seed entries
    for (const route1 of routes1) {
        const origin_id = await client.query('SELECT id FROM place WHERE name = $1', [route1.origin]);
        const destination_id = await client.query('SELECT id FROM place WHERE name = $1', [route1.destination]);

        await client.query('INSERT INTO routes (distance, duration, origin_id, destination_id) values ($1, $2, $3, $4) RETURNING id', [route1.distance, route1.time, origin_id.rows[0].id, destination_id.rows[0].id]);
    }

    for (const route2 of routes2) {
        const origin_id = await client.query('SELECT id FROM place WHERE name = $1', [route2.origin]);
        const destination_id = await client.query('SELECT id FROM place WHERE name = $1', [route2.destination]);

        await client.query('INSERT INTO routes (distance, duration, origin_id, destination_id) values ($1, $2, $3, $4) RETURNING id', [route2.distance, route2.time, origin_id.rows[0].id, destination_id.rows[0].id]);
    }

    await client.end();
}

main();