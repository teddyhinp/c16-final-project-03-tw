import fs from 'fs';
import jsonfile from 'jsonfile';

function main() {
    const errorDatas = fs.readFileSync('./resources/error.txt', 'utf8').split('\n');
    console.log('Route unfounded: ' + (errorDatas.length - 1)*2);

    const routeDatas = jsonfile.readFileSync('./resources/route.json');
    console.log('Route founded: ' + routeDatas.length);

    const coordinatesDatas = jsonfile.readFileSync('./resources/coordinates.json');
    const route2Obj = [];

    for (const errorData of errorDatas) {
        if (errorData) {
            let originArray = errorData.split('origin: ')[1].split(' ,destination: ')[0].split('(');
            let destinationArray = errorData.split('origin: ')[1].split(' ,destination: ')[1].split('(');
            let originIndex = originArray.pop();
            let destinationIndex = destinationArray.pop();

            if (originArray && destinationArray && originIndex && destinationIndex) {
                originIndex = originIndex.split(')')[0];
                destinationIndex = destinationIndex.split(')')[0]
                const { distance, time } = getDistanceAndTime(parseInt(originIndex), parseInt(destinationIndex), coordinatesDatas, routeDatas);
                route2Obj.push({
                    "origin": originArray.join('('),
                    "destination": destinationArray.join('('),
                    "distance": distance,
                    "time": time
                });

                route2Obj.push({
                    "origin": destinationArray.join('('),
                    "destination": originArray.join('('),
                    "distance": distance,
                    "time": time
                });
            }
        }
    }
    console.log('Unfounded route updated: ' + route2Obj.length);
    jsonfile.writeFileSync('./resources/route2.json', route2Obj, { spaces: 4 });
}

function getDistanceAndTime(originIndex: number, destinationIndex: number, coordinatesDatas: Object[], routeDatas: Object[]) {
    const originCoordinates = coordinatesDatas[originIndex]['coordinate'];
    const destinationCoordinates = coordinatesDatas[destinationIndex]['coordinate'];

    let routeOriginCoordinates;
    let routeDestinationCoordinates;
    
    //Base case
    for (const coordinatesData of coordinatesDatas) {
        if (coordinatesData['name'] == routeDatas[0]['origin']){
            routeOriginCoordinates = coordinatesData['coordinate'];
        }

        if (coordinatesData['name'] == routeDatas[0]['destination']){
            routeDestinationCoordinates = coordinatesData['coordinate'];
        }

        if (routeOriginCoordinates && routeDestinationCoordinates){
            break;
        }
    }
    let coordinatesDifference = getDistance(originCoordinates, routeOriginCoordinates) + getDistance(destinationCoordinates, routeDestinationCoordinates);
    let distance = routeDatas[0]['distance'];
    let time = routeDatas[0]['time'];

    //Find all the case
    for(const routeData of routeDatas){
        routeOriginCoordinates = null;
        routeDestinationCoordinates = null;
        for (const coordinatesData of coordinatesDatas) {
            if (coordinatesData['name'] == routeData['origin']){
                routeOriginCoordinates = coordinatesData['coordinate'];
            }
    
            if (coordinatesData['name'] == routeData['destination']){
                routeDestinationCoordinates = coordinatesData['coordinate'];
            }
    
            if (routeOriginCoordinates && routeDestinationCoordinates){
                break;
            }
        }

        const tempCoordinatesDifference = getDistance(originCoordinates, routeOriginCoordinates) + getDistance(destinationCoordinates, routeDestinationCoordinates);
        if(tempCoordinatesDifference < coordinatesDifference){
            coordinatesDifference = tempCoordinatesDifference;
            distance = routeData['distance'];
            time = routeData['time'];
        }
    }

    return {
        distance: distance,
        time: time
    };
}

function getDistance(origin: number[], destination: number[]){
    return Math.sqrt((origin[0] - destination[0])**2 + (origin[1] - destination[1])**2 );
}

main();