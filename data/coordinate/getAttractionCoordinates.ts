import dotenv from 'dotenv';
import pg from 'pg';
import jsonfile from 'jsonfile';

dotenv.config();

const client = new pg.Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
})

async function main() {
    await getCoordinates();
}

async function getCoordinates() {
    await client.connect();
    const coordinateObj = []
    const coordinates = await client.query('SELECT * FROM attractions')
    const rows = coordinates.rows
    console.log(rows.length)

    for (let i = 0; i < rows.length; i++) {
        const coordinate = {
            name: rows[i].name,
            coordinate: [rows[i].latitude, rows[i].longitude],
        }
        coordinateObj .push(coordinate)        
    }
    jsonfile.writeFileSync('./resources/coordinates.json', coordinateObj, {spaces: 4});
    console.log("done")

    await client.end();
}

main();