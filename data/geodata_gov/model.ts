export const titles = [
    "羽毛球場",
    "燒烤區（漁農署）",
    "燒烤區（康文署）",
    "籃球場",
    "泳灘",
    "草地滾球場",
    "露營地點",
    "兒童遊戲室",
    "郊野公園",
    "單車場",
    "健身室",
    "高爾夫球設施",
    "草地球場",
    "賓館(持牌)",
    "硬地球場",
    "度假營",
    "度假營(持牌)",
    "度假屋(持牌)",
    "酒店(持牌)",
    "海岸公園",
    "多用途室內體育館",
    "其他康體設施",
    "公園及動植物公園",
    "表演場地",
    "公眾休憩空間",
    "康樂場地",
    "體育館",
    "運動攀登設施",
    "運動場",
    "壁球場",
    "大球場",
    "泳池",
    "乒乓球檯",
    "網球場",
    "郊野公園訪客中心",
    "水上活動中心"
];

export interface Data {
    title_cn: string;
    title_en: string | null;
    update_date: Date | null;
}

export interface DataModel {
    type: string,
    features: Feature[]
}

export interface Feature {
    type: string,
    geometry: {
        type: string,
        coordinates: number[] //[longitude, latitude]
    }
    properties: {
        GMID: string | undefined,
        Northing: number | undefined,
        Easting: number | undefined,
        Dataset: string | undefined,
        'Facility Name': string | undefined,
        Address: string | undefined,
        數據集: string | undefined,
        設施名稱: string | undefined, //name
        地址: string | undefined, //address
        District: string | undefined,
        地區: string | undefined, //sub_district
        'Facility Type': string | undefined,
        設施種類: string | undefined,
        'Opening Hours': string | undefined,
        開放時間: string | undefined,
        Telephone: string | undefined, //phone_number
        聯絡電話: string | undefined,
        'Fax Number': string | undefined,
        傳真號碼: string | undefined,
        'Email Address': string | undefined,
        電郵地址: string | undefined,
        Website: string | undefined, //website
        網頁: string | undefined,
        'Last Update': string | undefined,
        'Facility Details' : string | undefined,
        設施詳情: string | undefined,
        ownershipEN: string | undefined,
        locationZH: string | undefined, //address
        relevantDistrictCouncilZH: string | undefined, //sub_district
        nameZH: string | undefined, //name
        ownershipZH: string | undefined,
        locationEN: string | undefined,
        nameEN: string | undefined,
        relevantDistrictCouncilEN: string | undefined,
        items : Item[] | undefined
    }
}

export interface Item {
    requirementsUnderLeaseEN: string | undefined,
    briefDescriptionZH: string | undefined
    remarksZH: string | undefined,
    openingHoursEN: string | undefined,
    requirementsUnderLeaseZH: string | undefined,
    remarksEN: string | undefined,
    openingHoursZH: string | undefined,
    briefDescriptionEN: string | undefined,
}