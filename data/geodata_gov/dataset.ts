import playwright from 'playwright';
import jsonfile from 'jsonfile';
import fs from 'fs';
import { titles , Data } from './model';

async function main() {
    console.log('GOTO https://geodata.gov.hk/gs/datasets#search');
    const browser = await playwright.chromium.launch();
    const context = await browser.newContext({ acceptDownloads: true });
    const page = await context.newPage();
    await page.goto('https://geodata.gov.hk/gs/datasets#search');

    //select Dataset
    console.log('SELECT Dataset:' + titles);
    const dataset = await getDataset(page);
    const selectedDataset = []
    for (const title of titles) {
        for (const data of dataset) {
            if (title == data.title_cn) {
                selectedDataset.push({
                    title_cn: data.title_cn,
                    title_en: data.title_en,
                    update_date: data.update_date
                })
            }
        }
    }

    const date = Date.now();
        
    if (!fs.existsSync('./log.json')) {
        //Create log file
        console.log(`CREATING log file: ./log.json`);
        fs.writeFileSync(`./log.json`, "");
        const version = {};
        for (const data of selectedDataset) {
            version[data.title_cn] = data.update_date
        }
        const log = {
            date: (new Date(date)).toString(),
            version: version
        }
        await jsonfile.writeFile('./log.json', log, { spaces: 4 });

        await downloadDataset(page, selectedDataset);
    }
    else {
        //Update log file
        console.log(`UPDATING log file: ./log.json`);
        const oldLog = await jsonfile.readFile('./log.json');
        const datasetToUpdate: Data[] = [];
        const version = {};

        for (const data of selectedDataset) {
            version[data.title_cn] = data.update_date;
            if(new Date(oldLog['version'][data.title_cn]).toString() !== data.update_date?.toString()){
                datasetToUpdate.push({
                    title_cn: data.title_cn,
                    title_en: data.title_en,
                    update_date: data.update_date
                });
            }            
        }

        if(datasetToUpdate.length > 0){
            await downloadDataset(page, datasetToUpdate);
        }
        else{
            console.log('Already up to date');
        }
        
        const log = {
            date: (new Date(date)).toString(),
            version: version
        }
        await jsonfile.writeFile('./log.json', log, { spaces: 4 });
    }    

    await browser.close();
}

async function getDataset(page: playwright.Page) {
    const dataset = await page.evaluate(() => {
        interface Data {
            title_cn: string | null,
            title_en: string | null,
            update_date: Date | null
        };

        enum Month {
            Jan = 0,
            Feb,
            Mar,
            Apr,
            May,
            Jun,
            Jul,
            Aug,
            Sep,
            Oct,
            Nov,
            Dec
        };

        const dataset: Data[] = [];
        const datasetLayers = document.querySelectorAll('#category_search_result .cat_button2');
        for (let i = 0; i < datasetLayers.length; i++) {
            const title = datasetLayers[i].querySelector('.cat_button_title')?.innerHTML.split('<br>');
            const update_date_array = datasetLayers[i].querySelector('.upd_date')?.innerHTML.split(' ');
            let update_date = null;
            if (update_date_array) {
                update_date = new Date(parseInt(update_date_array[2]), Month[update_date_array[1]], parseInt(update_date_array[0]));
                if (isNaN(update_date.getTime())) {
                    update_date = null;
                }
            }
            if (title) {
                dataset.push({
                    title_cn: title[1],
                    title_en: title[0].replace('&amp;', '&'),
                    update_date: update_date
                });
            }
        }
        return dataset;
    });
    return dataset;
}

function downloadJsonFile(page: playwright.Page) {
    try {
        page.click('#download-fullset-geojson-button');
    }
    catch (e) {
        console.log('ERROR: Unable to download this file');
    }
}

async function downloadDataset(page: playwright.Page, dataset: Data[]){   
    for (const data of dataset) {
        await page.goto('https://geodata.gov.hk/gs/datasets#search');
        await page.waitForLoadState('networkidle');
        try {
            await page.click(`#category_search_result .cat_button2 .cat_button_title:has-text("${data.title_en}")`);
            await page.waitForLoadState('networkidle');
            await page.click('#hkgeohub_download_dataset_button');
            console.log(`DOWNLOADING ./download/${data.title_cn}.geoJson`);
            const [download] = await Promise.all([
                page.waitForEvent('download'),
                downloadJsonFile(page)
            ]);
            const PATH = `./download/${data.title_cn}.geoJson`
            if (fs.existsSync(PATH)){
                fs.unlinkSync(PATH)
            }
            await download.saveAs(PATH);
            await download.delete();
        } catch(e) {
            console.log(`Error: File was not downloaded`);
        }
    }
    console.log('DONE');
}

main();