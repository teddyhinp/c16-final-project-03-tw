import { Client } from 'pg';
import dotenv from 'dotenv';
import jsonfile from 'jsonfile';
import { titles, DataModel } from './model';
import { getSubDistrictFromCoordinates, getSubDistrictFromAddress } from '../../server/resources/getSubDistrict';

dotenv.config();

const client = new Client({
    host: process.env.RDS_HOST,
    port: 5432,
    database: process.env.RDS_NAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD
});

async function main() {
    let categories1: string;
    let categories2: string;
    let name = null;
    let sub_district = null;
    let address = null;
    let phone_number = null;
    let website = null;
    let description = null;
    let latitude = null;
    let longitude = null;
    let image_url = null;
    let source: string;
    let update_date: string;

    await client.connect();
    const log = await jsonfile.readFile('./log.json');

    for (const title of titles) {
        const dataset: DataModel = await jsonfile.readFile(`./download/${title}.geoJson`);
        for (const feature of dataset.features) {
            try {
                if (title == "度假營(持牌)" || title == "度假營" || title == "賓館(持牌)" || title == "度假屋(持牌)" || title == "酒店(持牌)") {
                    categories1 = 'hotels';
                }
                else {
                    categories1 = 'facilities';
                }

                categories2 = title.split('（')[0].split('(')[0];

                latitude = feature.geometry.coordinates[1];
                longitude = feature.geometry.coordinates[0];
                source = `geodata_${title}`;
                update_date = log.version[`${title}`].split('T').join(' ').substring(0, 19);

                if (feature.properties.hasOwnProperty('設施名稱') && feature.properties.設施名稱) {
                    name = feature.properties.設施名稱;
                }
                else if (feature.properties.hasOwnProperty('nameZH') && feature.properties.nameZH) {
                    name = feature.properties.nameZH;
                }
                
                if (feature.properties.hasOwnProperty('地址') && feature.properties.地址) {
                    address = feature.properties.地址;
                }
                else if (feature.properties.hasOwnProperty('locationZH') && feature.properties.locationZH) {
                    address = feature.properties.locationZH;
                }
                
                if(address){
                    sub_district = getSubDistrictFromAddress(address);
                }                
                if(!sub_district){
                    sub_district = getSubDistrictFromCoordinates([latitude, longitude])
                }                

                if (feature.properties.hasOwnProperty('Telephone') && feature.properties.Telephone) {
                    phone_number = parseInt(feature.properties.Telephone.split(' ').join(''));
                    if (isNaN(phone_number)) {
                        phone_number = null;
                    }
                }

                if (feature.properties.hasOwnProperty('Website') && feature.properties.Website) {
                    website = feature.properties.Website;
                    if (website == 'N.A.') {
                        website = null;
                    }
                }
            } catch (e) {
                console.log(`Invalid data in ${title}.geoJson, data insertion delined`);
                console.log(e)
                break;
            }

            await client.query('INSERT INTO place (categories1, categories2, name, sub_district, address, phone_number, website, description, latitude, longitude, image_url, source, update_date) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) returning id', [
                categories1,
                categories2,
                name,
                sub_district,
                address,
                phone_number,
                website,
                description,
                latitude,
                longitude,
                image_url,
                source,
                update_date
            ]);
        }
    }

    await client.end();
}
main();