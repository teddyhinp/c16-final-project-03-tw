-- psql -U postgres -W -h finalproject.cqjdvf0zfvrl.ap-southeast-1.rds.amazonaws.com finalproject
CREATE TABLE place (
    id SERIAL PRIMARY KEY,
    categories1 VARCHAR(255) NOT NULL,
    categories2 VARCHAR(255),
    name VARCHAR(255) NOT NULL,
    sub_district VARCHAR(255),
    address VARCHAR(255),
    phone_number INTEGER,
    website VARCHAR(255),
    description VARCHAR(255),
    latitude FLOAT NOT NULL,
    longitude FLOAT NOT NULL,
    image_url VARCHAR(255),
    source VARCHAR(255) NOT NULL,
    update_date TIMESTAMP NOT NULL
);

CREATE TABLE routes (
    id SERIAL PRIMARY KEY,
    distance INTEGER NOT NULL,
    duration INTEGER NOT NULL,
    origin_id INTEGER NOT NULL,
    destination_id INTEGER NOT NULL,
    FOREIGN KEY (destination_id) REFERENCES place(id),
    FOREIGN KEY (destination_id) REFERENCES place(id)
);

SELECT * FROM places
SELECT * FROM routes;

DROP TABLE routes;
DROP TABLE place;