-- psql create database googledata;         --> for google dashboard

CREATE TABLE dim_districts(
    id SERIAL PRIMARY KEY,
    name text,
    latitude DECIMAL,
    longtitude DECIMAL
);


CREATE UNIQUE INDEX dim_districts_unique_idx on dim_districts (name);


CREATE TABLE dim_categories(
    id SERIAL PRIMARY KEY,
    name text
);

CREATE UNIQUE INDEX dim_categories_unique_idx on dim_categories(name);


CREATE TABLE dim_attractions(
    id SERIAL PRIMARY KEY,
    name text,
    address text,
    latitude DECIMAL,
    longtitude DECIMAL
);

CREATE UNIQUE INDEX dim_attractions_unique_idx on dim_attractions(name,address);

CREATE TABLE dim_insert_dates(
    id SERIAL PRIMARY KEY,
    insert_date timestamp DEFAULT NOW(),
    year integer,
    month integer,
    day integer,
    hour integer
);

CREATE UNIQUE INDEX dim_insert_dates_unique_idx on 
dim_insert_dates(insert_date);


-- //fact
CREATE TABLE fact_district_attractions(
    id SERIAL PRIMARY KEY,
    dim_insert_date_id  integer ,
    foreign key (dim_insert_date_id) references dim_insert_dates(id),
    dim_district_id  integer ,
    foreign key (dim_district_id) references dim_districts(id),
    dim_category_id  integer ,
    foreign key (dim_category_id) references dim_categories(id),
    dim_attraction_id  integer ,
    foreign key (dim_attraction_id) references dim_attractions(id)
);



-- psql \dt              <- show list of relations