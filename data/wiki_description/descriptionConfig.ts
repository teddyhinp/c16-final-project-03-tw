import jsonfile from 'jsonfile';

function main(){
    const descriptionObjs = jsonfile.readFileSync('./resources/filteredDescription.json');

    let temp_descriptions = [];

    for(const descriptionObj of descriptionObjs){
        if(descriptionObj.description.length >= 255){
            let descriptionArray = descriptionObj.description.split("。");
            let count = 0;
            let temp_descriptionArray = [];
            for(const description of descriptionArray){
                if(count + description.length + 10 >= 255){
                    break;
                }

                temp_descriptionArray.push(description);
            }


            temp_descriptions.push({
                "name": descriptionObj.name,
                "description": temp_descriptionArray.join("。").replace('\n', "")
            });
        }
        else{
            temp_descriptions.push({
                "name": descriptionObj.name,
                "description": descriptionObj.description.replace('\n', "")
            });
        }  
    }

    jsonfile.writeFileSync('./resources/filteredDescription2.json', temp_descriptions, {spaces: 4});
}

main();