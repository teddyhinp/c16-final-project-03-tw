import playwright from 'playwright';
import jsonfile from 'jsonfile';
import { Client } from 'pg';
import dotenv from 'dotenv';

dotenv.config();

export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
});

async function main() {
    await client.connect();

    const browser = await playwright.chromium.launch();
    const context = await browser.newContext();
    const page = await context.newPage();

    const result = await client.query('SELECT name FROM attractions');
    const nameObjs = result.rows;
    const dataset = [];

    for (const nameObj of nameObjs) {
        await page.goto(`https://zh.wikipedia.org/w/index.php?search=${nameObj.name.split('(')[0]}`);
        await page.waitForLoadState('networkidle');

        if (await page.$(`.mw-search-result-heading a`) == null) {
            continue;
        }

        await page.click(`.mw-search-result-heading a`);
        await page.waitForLoadState('networkidle');

        const data = await getDescription(page);

        if(data.description?.includes('香港')){
            dataset.push({
                name: nameObj.name,
                title: data.title,
                description: data.description
            });
        }        
    }

    jsonfile.writeFile('./resources/description.json', dataset, { spaces: 4 });

    await browser.close();

    await client.end();
}

async function getDescription(page: playwright.Page) {
    const description = await page.evaluate(() => {
        let title = document.getElementById('firstHeading')?.innerHTML.split('<')[0];
        let description = document.querySelector('.mw-parser-output p')?.innerHTML;
        if (description) {
            let descriptionArray = description.split('<');
            for (let elem in descriptionArray) {
                descriptionArray[elem] = descriptionArray[elem].split('>')[1];
            }
            description = descriptionArray.join('').replace('\n', '').replace('&nbsp;', '');

            for(let i = 1; i < 10; i++){
                description = description.replace(`[${i}]`, '');
            }
        }

        return {
            title: title,
            description: description
        };
    });
    return description;
}

main();