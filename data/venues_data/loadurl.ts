import axios from 'axios';
import fs from 'fs';
import { ImageFile } from './model';
import jsonfile from 'jsonfile';

const instance = axios.create({
    baseURL: 'https://api.foursquare.com/v2/venues',
    headers: { 'X-Custom-Header': 'foobar' },
    params: {
        client_id: 'T2A33GMGNWPJMGWGONWMH4YPJWZLYXTTHZXIAWAYPB5LNKVK',
        client_secret: '4XHEQ5G3Q3RAB0R4J0P5AH3YP5JDNSQGV5RGY51ATCECUW1G',
        v: '20210101',
    }
});

let count: number = 0;

async function getPhotoURL(venueId:string) {
    let imageURL: string ;
    try {
      const response = await instance.get(`/${venueId}/photos`);
      const data: any = response.data
      const photoResponse = data.response.photos.items[0]
      const prefix = photoResponse.prefix 
      const suffix = photoResponse.suffix 
      const height = 400;
      const width = 600;
      if (prefix != undefined){
        imageURL = `${prefix}${width}x${height}${suffix}`;
    }else{
        imageURL = "empty"
    }
    } catch (error) {
      console.error(error);
      return ""
    }
    return imageURL
  }

async function loadFile(filepath: string, category1:string, filename:string) {
    const rawdata = fs.readFileSync(filepath
        , 'utf8');
    const datas = JSON.parse(rawdata);
    let venueIdArr: any = [];
    for (const data of datas) {
        const imageurl = await getPhotoURL(data.id)
        const imagefile: ImageFile = {
            foursquare_id : data.id,
            imageurl: imageurl,
            category1: category1,
            category2: data.categories_name,
            name: data.name
        }
        venueIdArr.push(imagefile)
        count = count + 1
        console.log(count)
    }
    await jsonfile.writeFile(`./${filename}.json`, venueIdArr, { spaces: 4 });
    console.log("done")
}

loadFile('./venues/arts_entertainment.json','attraction','./url_arts_entertainment')