//URL: https://tecky-finalproject.s3.ap-southeast-1.amazonaws.com/<PATH>
import * as AWS from 'aws-sdk'; //yarn add aws-sdk
import dotenv from 'dotenv'; //yarn add dotenv @types/dotenv
import fs from 'fs';

dotenv.config();

AWS.config.update({
    accessKeyId: process.env.accessKeyId,
    secretAccessKey: process.env.secretAccessKey,
    region: "ap-southeast-1"
});

let s3 = new AWS.S3();
const bucket = 'tecky-finalproject';

function main() {
    const imgpaths = imgPaths()
    for (const imgpath of imgpaths) {
        uploadFile(imgpath)
    }
    ;
}

const filepath = "./image/mall/"

function imgPaths() {
    let imgPaths: any = [];
    const filenames = fs.readdirSync(filepath)
    filenames.forEach(file => {
        const imgpath = (`${filepath}${file}`);
        imgPaths.push(imgpath)
    }
    );
    return imgPaths
}


function uploadFile(file: Buffer) {
    const params = {
        Bucket: bucket,
        Key: `${file}`, //s3 path
        Body: file
    };
    s3.upload(params, (err: any, data: any) => {
        if (err) {
            console.log('There was an error uploading your file: ', err);
            return false;
        }
        console.log('Successfully uploaded file.', data);
        return true;
    });
}

main();