
export interface Venue {
    id: number,
    name: string,
    lat: number,
    lng: number,
    address: string,
    city: string,
    state: string | null,
    categories_id: string,
    categories_name: string,
}

export const categories_id = {
    "shopping_mall": "4bf58dd8d48988d1fd941735",
    "arts_entertainment": "4d4b7104d754a06370d81259",
    "theme_park": "4bf58dd8d48988d182941735",
    "American_Restaurant": "4bf58dd8d48988d14e941735",
    "Chinese Restaurant": "4bf58dd8d48988d145941735",
    "Hotpot Restaurant": "52af0bd33cf9994f4e043bdd",
    "Japanese Restaurant": "4bf58dd8d48988d111941735",
    "Korean Restaurant": "4bf58dd8d48988d113941735",
    "Noodle House": "4bf58dd8d48988d1d1941735",
    "Thai Restaurant": "4bf58dd8d48988d149941735",
    "Vietnamese Restaurant": "4bf58dd8d48988d14a941735",
    "Buffet": "52e81612bcbc57f1066b79f4",
    "Coffee Shop": "4bf58dd8d48988d1e0931735",
    "Dessert Shop": "4bf58dd8d48988d1d0941735",
    "French Restaurant": "4bf58dd8d48988d10c941735",
    "Indian Restaurant": "4bf58dd8d48988d10f941735",
    "Italian Restaurant": "4bf58dd8d48988d110941735",
    "Vegan Restaurant": "4bf58dd8d48988d1d3941735"
}

export const HK = [
    "港島",
    "九龍",
    "新界"
];

export const StateParams = [
    "中西區",
    "灣仔",
    "東區",
    "南區",
    "油尖旺",
    "深水埗",
    "九龍城",
    "黃大仙",
    "觀塘",
    "葵青",
    "荃灣",
    "屯門",
    "元朗",
    "北區",
    "大埔",
    "沙田",
    "西貢",
    "離島",
];

export const Sub_StateParams = [
    '22.2822235,114.1292619',
    '22.2863914,114.1338924',
    '22.2875634,114.1417059',
    '22.2863943,114.1491375',
    '22.2799907,114.1587983',
    '22.2796357,114.1654865',
    '22.2816223,114.147865',
    '22.2776827,114.1591909',
    '22.276022,114.1751471',
    '22.2859787,114.1914919',
    '22.268352,114.1865135',
    '22.2751881,114.1937244',
    '22.2731007,114.1892659',
    '22.2666667,114.2',
    '22.2823972,114.1922382',
    '22.2847861,114.1998006',
    '22.2884683,114.192803',
    '22.2827206,114.2123009',
    '22.2824738,114.2227035',
    '22.278746,114.2286476',
    '22.2678103,114.2360778',
    '22.2615148,114.2494516',
    '22.2679201,114.1290719',
    '22.2483068,114.1524398',
    '22.2418581,114.1528532',
    '22.2459198,114.1664957',
    '22.2471845,114.1810919',
    '22.2345043,114.1952039',
    '22.2183333,114.2048704',
    '22.220422,114.2126685',
    '22.2567774,114.2106427',
    '22.2334016,114.2420218',
    '22.2988123,114.1721746',
    '22.3068537,114.1714423',
    '22.3034757,114.1602846',
    '22.3095048,114.1751471',
    '22.3203648,114.169773',
    '22.3198474,114.1620857',
    '22.3366356,114.1402195',
    '22.3377817,114.1359272',
    '22.3358055,114.1494892',
    '22.3285899,114.1602846',
    '22.3337786,114.1674019',
    '22.3312462,114.1744039',
    '22.341024,114.1654865',
    '22.3228031,114.1350173',
    '22.3055658,114.1887233',
    '22.3173373,114.1879691',
    '22.3206419,114.1929813',
    '22.3223949,114.1862935',
    '22.3246084,114.1990482',
    '22.3306602,114.1920172',
    '22.318507,114.1755339',
    '22.3369319,114.1763367',
    '22.3495063,114.1692021',
    '22.3364426,114.1989259',
    '22.3429615,114.1929813',
    '22.3330898,114.1933481',
    '22.3405302,114.1862935',
    '22.3377402,114.1862935',
    '22.35,114.2',
    '22.3515518,114.200412',
    '22.3356369,114.2084317',
    '22.3080749,114.2018982',
    '22.322466,114.2167592',
    '22.3244408,114.2175022',
    '22.3103686,114.2227035',
    '22.3186108,114.2348969',
    '22.3080199,114.2375638',
    '22.2940736,114.2375638',
    '22.2848099,114.2377354',
    '22.3668502,114.1302591',
    '22.3474872,114.1023164',
    '22.3699122,114.1144306',
    '22.3792004,114.1357605',
    '22.3768203,114.0755593',
    '22.367102,114.058809',
    '22.3631111,114.0473141',
    '22.3500753,114.059207',
    '22.331731,114.0289607',
    '22.3641206,114.0227101',
    '22.3754639,113.9987128',
    '22.3908295,113.9725126',
    '22.4216464,113.981979',
    '22.4346478,113.9927005',
    '22.4464878,113.9860245',
    '22.468172,113.984489',
    '22.4606419,114.0041996',
    '22.4445376,114.0222076',
    '22.4950121,114.0696131',
    '22.5098835,114.0815055',
    '22.440058,114.0650077',
    '22.4249178,114.0753753',
    '22.4465916,114.0933976',
    '22.4916829,114.1414685',
    '22.5006876,114.1398274',
    '22.5112008,114.1260991',
    '22.502011,114.130402',
    '22.531577,114.20324',
    '22.520629,114.2138563',
    '22.5058269,114.2441439',
    '22.4445559,114.1704893',
    '22.4423218,114.1655064',
    '22.4370545,114.1828429',
    '22.4728367,114.232961',
    '22.5081084,114.2509376',
    '22.428573,114.248978',
    '22.4107,114.27302',
    '22.375941,114.178626',
    '22.3771304,114.1974398'
];

export const sub_district_model = {
    '香港': null,
    '中環': '中環',
    '金鐘': '金鐘',
    '山顶': '山頂',
    'Shek Tong Tsui': '石塘咀',
    '坚尼地城': '堅尼地城',
    'Sheung Wan': '上環',
    '薄扶林': '薄扶林',
    Aberdeen: '香港仔',
    '灣仔': null,
    '铜锣湾': '銅鑼灣',
    '尖沙咀': '尖沙咀',
    '北角': '北角',
    '炮臺山': null,
    '九龍': null,
    '鰂魚涌': '鰂魚涌',
    '太古城': '鰂魚涌',
    'Repulse Bay': '淺水灣',
    'Sai Wan Ho': '西灣河',
    'Tsim Sha Tsui': '尖沙咀',
    '红磡': '紅磡',
    '筲箕灣': '筲箕灣',
    'Cha Kwo Ling': null,
    '油塘': '油塘',
    'Lam Tin': '藍田',
    '柴灣': '柴灣',
    'Kwun Tong': null,
    '观塘': null,
    '小西湾': '小西灣',
    'Sau Mau Ping': '秀茂坪',
    'Tseung Kwan O': '將軍澳',
    '观塘区': null,
    'Chek Chue': '赤柱',
    'Kowloon Bay': '九龍灣',
    '牛头角': '牛頭角',
    'West Kowloon': '西九龍填海區',
    'Ap Lei Chau': '鴨脷洲',
    '旺角': '旺角',
    'Yau Tsim Mong': null,
    '油麻地': '油麻地',
    '九龙塘': '九龍塘',
    '深水埗區': null,
    'Ho Man Tin': '何文田',
    '大角咀': '大角咀',
    'Mong Kok': '旺角',
    'Cheung Sha Wan': '長沙灣',
    '荔枝角': '荔枝角',
    'Shum Shui Po': null,
    'Lai Chi Kok': '荔枝角',
    'Kwai Chung': '葵涌',
    'Tsing Yi': '青衣',
    '青衣': '青衣',
    'Tsuen Wan': null,
    'Wang Tau Hom': '橫頭磡',
    '九龙城': null,
    '土瓜湾': '土瓜灣',
    'Wong Tai Sin': null,
    'Kowloon City': null,
    '钻石山': '鑽石山',
    'San Po Kong': '新蒲崗',
    '牛池湾': '牛池灣',
    'Tsz Wan Shan': '慈雲山',
    Shatin: null,
    'Tai Wai': '大圍',
    'Hang Hau Town': '坑口',
    'tsuen wan': null,
    'Sham Tseng': '深井',
    'Yuen Long': null,
    'So Kwun Wat': '掃管笏',
    'Discovery Bay': '大嶼山',
    '屯門': null,
    'Tin Shui Wai': '天水圍',
    'Tung Chung': '東涌',
    'Tuen Mun': null,
    'Sheung Shui': '上水',
    Fanling: '粉嶺',
    'Tai Po': null,
    Taipo: null,
    'Ma On Shan': '馬鞍山',
    'Ma On Shan Tsuen': '馬鞍山',
    '乌溪沙': '烏溪沙',
    'Fo Tan': '火炭',
    'Sha Tin': null,
    'Kennedy Town': '堅尼地城',
    'Sai Ying Pun': '西營盤',
    Soho: '中環',
    'Hong Kong': null,
    'Mid-Levels': '半山區',
    'Mid-levels': '半山區',
    '中西区': null,
    '跑马地': '跑馬地',
    Wanchai: null,
    Kornhill: '鰂魚涌',
    '數碼港': '薄扶林',
    'Wong Chuk Hang': '黃竹坑',
    'Huáng zhú kēng': '黃竹坑',
    Mongkok: '旺角',
    '九龍灣': '九龍灣',
    'Ting Kau': '汀九',
    Hongkong: null,
    "Penny's Bay": '大嶼山',
    'Lantau Island': '大嶼山',
    'Peng Chau': '坪洲',
    'Ma Liu Shui': '馬料水',
    'Shuen Wan': '船灣',
    'Ting Kok': '船灣',
    'Hong Kong S.A.R': null,
    '大埔': null,
    '西貢': null,
    'Wong Chuk Hang, Hong Kong': '黃竹坑',
    Jordan: '尖沙咀',
    '馬灣市': '馬灣',
    'Kau To': '火炭'
}

export const district = [
    "中西區",
    "灣仔",
    "東區",
    "南區",
    "油尖旺",
    "深水埗",
    "九龍城",
    "黃大仙",
    "觀塘",
    "葵青",
    "荃灣",
    "屯門",
    "元朗",
    "北區",
    "大埔",
    "沙田",
    "西貢",
    "離島",
];
export interface ImageURL {
    prefix: string,
    suffix: string,
    width: number,
    height: number,
}

export interface ImageFile {
    foursquare_id: string,
    imageurl: string | NodeJS.Timer,
    category1: string,
    category2: string,
    name: string,
}