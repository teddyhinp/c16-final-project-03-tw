import axios from 'axios';
import { Venue, Sub_StateParams, categories_id } from './model';
import jsonfile from 'jsonfile';

async function main() {
    for (const cat_id in categories_id) {
        const ids: number[] = [];
        const venueArr: Venue[] = [];
        for (const state of Sub_StateParams) try {
            const res = await axios.get('https://api.foursquare.com/v2/venues/explore', {
                params: {
                    client_id: 'T2A33GMGNWPJMGWGONWMH4YPJWZLYXTTHZXIAWAYPB5LNKVK',
                    client_secret: '4XHEQ5G3Q3RAB0R4J0P5AH3YP5JDNSQGV5RGY51ATCECUW1G',
                    ll: state,
                    categoryId: categories_id[cat_id],
                    v: '20210101',
                    limit: "50",
                }
            })
            console.log("getting required venue data...")
            const data: any = res.data
            const items = data.response.groups[0].items
            for (const item of items) {
                const venueData = item.venue
                if (!ids.includes(venueData.id)) {
                    const venue: Venue = {
                        id: venueData.id,
                        name: venueData.name,
                        lat: venueData.location.lat,
                        lng: venueData.location.lng,
                        address: venueData.location.formattedAddress,
                        city: venueData.location.city,
                        state: venueData.location.state || null,
                        categories_id: venueData.categories[0].id,
                        categories_name: venueData.categories[0].name,

                    }
                    venueArr.push(venue)
                    ids.push(venueData.id)
                }
                
            }
            console.log(`${state}${cat_id} results: ${data.response.totalResults} \n Writing into files...`)
        } catch (e) {
            console.log(`Error: not data on ${state}${cat_id}`);
        }
        await jsonfile.writeFile(`./venues/${cat_id}.json`, venueArr, { spaces: 4 });
    }
    console.log(`DONE!`)
}

main();