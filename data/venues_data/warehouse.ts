import { Client } from 'pg';
import dotenv from 'dotenv';
import jsonfile from 'jsonfile';
import { categories_id, sub_district_model } from './model';
import { getSubDistrictFromCoordinates, getSubDistrictFromAddress } from '../../server/resources/getSubDistrict';

dotenv.config();

const client = new Client({
    host: process.env.RDS_HOST,
    port: 5432,
    database: process.env.RDS_NAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD
});

async function main() {
    let categories1: string;
    let categories2: string;
    let name = null;
    let sub_district = null;
    let address = null;
    let phone_number = null;
    let website = null;
    let description = null;
    let latitude = null;
    let longitude = null;
    let image_url = null;
    let source: string;
    let update_date: string;

    await client.connect();

    const url_arts_entertainment = jsonfile.readFileSync('./img_url/url_arts_entertainment.json');
    const url_shopping_mall = jsonfile.readFileSync('./img_url/url_shopping_mall.json');
    const url_theme_park = jsonfile.readFileSync('./img_url/url_theme_park.json');

    for (const categories in categories_id) {
        const dataset = await jsonfile.readFile(`./venues/${categories}.json`);
        for (const data of dataset) {
            try {
                if (categories == 'arts_entertainment') {
                    categories1 = 'attractions';
                    for (const url of url_arts_entertainment) {
                        if (url.foursquare_id == data.id && url.imageurl != "") {
                            image_url = `https://tecky-finalproject.s3.ap-southeast-1.amazonaws.com/image/attraction/${data.id}.jpg`
                        }
                    }
                }
                else if (categories == 'theme_park') {
                    categories1 = 'attractions';
                    for (const url of url_theme_park) {
                        if (url.foursquare_id == data.id && url.imageurl != "") {
                            image_url = `https://tecky-finalproject.s3.ap-southeast-1.amazonaws.com/image/attraction/${data.id}.jpg`
                        }
                    }
                }
                else if (categories == 'shopping_mall') {
                    categories1 = 'malls';
                    for (const url of url_shopping_mall) {
                        if (url.foursquare_id == data.id && url.imageurl != "") {
                            image_url = `https://tecky-finalproject.s3.ap-southeast-1.amazonaws.com/image/mall/${data.id}.jpg`
                        }
                    }
                }
                else {
                    categories1 = 'restaurants';
                }

                categories2 = data.categories_name;
                address = data.address.join(', ');

                if (data.city == '深圳市' || address.includes('中国')) {
                    continue;
                }
                else if (categories1 == 'restaurants' && !Object.keys(categories_id).includes(categories2)) {
                    continue;
                }

                latitude = data.lat;
                longitude = data.lng;
                source = `foursquare_${categories}`;
                update_date = '2021-01-01 16:00:00';
                sub_district = sub_district_model[data.city];

                if (address == '香港' || address == '香港, 香港' || address == 'Hong Kong, 香港') {
                    address = null;
                }

                if (address) {
                    sub_district = getSubDistrictFromAddress(address);
                }

                if (!sub_district) {
                    sub_district = getSubDistrictFromCoordinates([data.lat, data.lng])
                }

                if (data.name == 'Broadway Cinema (百老匯戲院)' || data.name == 'Emperor Cinemas (英皇戲院)' || data.name == 'Para Site Art Space' || data.name == 'Studiodanz' || data.name == 'Hong Kong Funky Dance Centre') {
                    name = `${data.name}(${sub_district})`;
                }
                else {
                    name = data.name;
                }
            }
            catch (e) {
                console.log(`Invalid data in ${categories}.json, data insertion delined`);
                console.log(e);
                break;
            }

            await client.query('INSERT INTO place (categories1, categories2, name, sub_district, address, phone_number, website, description, latitude, longitude, image_url, source, update_date) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) returning id', [
                categories1,
                categories2,
                name,
                sub_district,
                address,
                phone_number,
                website,
                description,
                latitude,
                longitude,
                image_url,
                source,
                update_date
            ]);
        }
    }

    await client.end();
}

main();