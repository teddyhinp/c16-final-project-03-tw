import fs from 'fs';
const download = require('image-downloader')

//download image from url
async function loadImage(filepath: string,) {
    const rawdata = fs.readFileSync(filepath, 'utf8');
    const datas = JSON.parse(rawdata);
    for (const data of datas) {
        if (data.imageurl != "") {
            let filename: string = `${data.foursquare_id}`;
            // filename = filename.replace(/\s/g, "_");
            downloadImage(`${data.imageurl}`, `${data.category1}`, `${filename}`)
        } else {
            console.log(`${data.name} :no image url`)
        }
    }
    console.log("done")
}

//run whole json file download image
loadImage('./img_url/url_shopping_mall.json');
loadImage('./img_url/url_arts_entertainment.json');
loadImage('./img_url/url_theme_park.json');

//image file rename logic
function downloadImage(url: string, folder: string, filename: string) {
    download.image({
        url,
        dest: `./image/${folder}/${filename}.jpg`
    });
}

