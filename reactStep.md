# React Step

##  `Tab2.tsx(Page)`
如何從database 拎 data?<br>
用useEffect,dependency array[ ]<br>
就會做到`componentdidMount` 的效果(只會在mount的時候行一次)<br>
而不受因其他state 造成的重新render 影響 
```
  useEffect(() => {
    let loadDistrict = async () => {
      const res = await fetch('http://localhost:8080/district')
      const json = await res.json()
      console.log('json', json)

      setDistricts(json)
    }
    loadDistrict()
  }, [])
```
然後下面 為child Component 寫 所需的props<br>
用.map 的形式 
```
{districts.map(district =>
              <IonCol> <CardCover
                key={district.id}
                district={district.name}
              /></IonCol>)}
```
##  `cardCover.tsx (tab2 的child)`
而家係用地方名 對番檔名 src e.g(中西區.png)<br>
let img = './images/districts/' + props.district + '.png'<br>
填番相應既props.district 同img
```
<IonCardTitle>{props.district}</IonCardTitle>
<img src={img} alt={props.district} ></img>
```
## Tab1.tsx(page)(未有data 暫時hardcode)
預計會在 tab 1 的位置 fetch 一個array of obj
```
let locations = [{
  location_name: "嘉頓山", img: '/ssp.jpg', text: "..."
}, {
  location_name: "銅鑼灣", img: '/cwb.jpg', text: "...."
}]

<Card location_name={location.location_name}
      img={location.img}
      text={location.text} />
```
### Card.tsx(component)
利用由 parent component 傳俾佢的props<br>
寫`props.location_name`,`props.img`,`props.text`
```
type Props = {
    location_name: string
    img: string
    text: string
}
```

# Register Page
其實可以用React hook form <br>
但katie 用了 比較麻煩的方法<br>
每個input 都用useState<br>
```
const [username, setUsername] = useState<string>();
const [password, setPassword] = useState<string>();
const [gender, setGender] = useState<string>();
const [preference1, setPreference1] = useState<string>();
const [preference2, setPreference2] = useState<string>();
const [checked, setChecked] = useState(false);
```
然後寫了submit function
```
function submit(event: FormEvent) {
      event.preventDefault()
      console.log('submit form data:', { username, password, gender, preference1, preference2, checked })
  }
```

而且用了ionic<br>
就要寫onIonChange

```
onIonChange={e => setUsername(e.detail.value!)}>
```