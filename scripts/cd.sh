#!/usr/bin/bash
set -e
set -o pipefail
set -x

scripts/build-image.sh
ubuntu-server/02-upload-image.sh
ubuntu-server/03-deploy-image.sh
ubuntu-server/06-clean-image.sh