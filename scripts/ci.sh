#!/usr/bin/bash
set -e
set -o pipefail
set -x

echo TRAVELPLANET write tests

cd server
npm install
npm test