#!/usr/bin/bash
set -e
set -o pipefail

cd shared
npm install
npm run build

cd ../server
npm install

cd ..

docker build -t travel-server .