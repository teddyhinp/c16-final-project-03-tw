#!/usr/bin/bash
set -e
set -o pipefail

docker run -it travel-server

docker-compose up -d
