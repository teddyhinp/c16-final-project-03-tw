#!/usr/bin/bash
set -e
set -o pipefail
set -x

ssh travel-server "docker images | grep none | awk '{print \$3}' | xargs -I {} docker rmi {}"