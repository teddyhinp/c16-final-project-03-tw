#!/usr/bin/bash
set -e
set -o pipefail
set -x

scp ubuntu-server/etc/nginx/sites-available/default travel-server:~/default 
ssh travel-server "sudo chown root:root ~/default" 
ssh travel-server "sudo mv ~/default /etc/nginx/sites-available/default"
ssh travel-server "sudo nginx -t"
ssh travel-server "sudo service nginx restart"
