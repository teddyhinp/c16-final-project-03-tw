#!/usr/bin/bash
set -e
set -o pipefail
set -x

scp ubuntu-server/install.sh travelplanet.katielok.xyz:~/install.sh
ssh travelplanet.katielok.xyz "~/install.sh"