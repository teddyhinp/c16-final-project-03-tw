#!/usr/bin/bash
set -e
set -o pipefail
set -x

docker images | grep travel-server

docker save travel-server | zstd | ssh travel-server "unzstd | docker load"

