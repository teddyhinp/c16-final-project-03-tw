#!/usr/bin/bash
set -e
set -o pipefail
set -x


ssh travel-server "sudo certbot --nginx"
ssh travel-server "sudo nginx -t"
ssh travel-server "sudo service nginx restart"

echo "please setup auto renew cert on the server:
> sudo crontab -e 
0 0 */1 * * certbot renew


(you can change the editoring by running 'select-editor')
"