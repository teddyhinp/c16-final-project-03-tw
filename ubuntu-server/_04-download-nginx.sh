#!/usr/bin/bash
set -e
set -o pipefail
set -x


scp travel-server:/etc/nginx/sites-available/default ubuntu-server/etc/nginx/sites-available/default
