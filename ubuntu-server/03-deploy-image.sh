#!/usr/bin/bash
set -e
set -o pipefail
set -x



scp docker-compose.yml travel-server:~/

ssh travel-server "docker-compose up -d"