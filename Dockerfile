FROM node:slim

WORKDIR /app

COPY . .

WORKDIR /app/shared
RUN npm install --force
RUN npm rebuild --force

WORKDIR /app/server
RUN rm -rf node_modules/bcrypt
RUN npm install --force
RUN npm rebuild --force


# CMD bash ./scripts/migrate.sh && npx knex migrate:latest && npx knex seed:run && npm run start:prod
CMD npx knex migrate:latest && npx knex seed:run && npm run start:prod
