export type LoginInput = {
  username: string;
  password: string;
};

export type JWTPayload = {
  id: number;
  username?: string;
  name?: string;
  picture?: string;
};
